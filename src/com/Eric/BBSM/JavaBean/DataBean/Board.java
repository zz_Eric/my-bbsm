package com.Eric.BBSM.JavaBean.DataBean;

import java.util.List;

/**
 * @author Eric
 * @Create 2020-09-02  15:04
 * @Version Demo
 **/
public class Board {
    private Integer boardId;
    private Boolean boardIdMother;
    private Integer boardBid;

    private String boardName;
    private String boardInfo;

    private Integer boardMasterId;
    private String boardImg;

    private Integer boardPostNum;
    private Integer boardTopicNum;
    private Integer boardTodayNum;

    private String boardLastReply;

    //体现实体间关系的属性
    private User user;
    private List<Topic> topics;
    private List<Post> posts;

    public Board() {
    }

    public Board(Boolean boardIdMother, Integer boardBid, String boardName, String boardInfo,
                 Integer boardMasterId) {
            String mBoardImg = "/static/assets/img/message_board.png"; //默认的主板块图片
            String sBoardImg = "/static/assets/img/message.png";    //默认的子版块图片
        this.boardIdMother = boardIdMother;
        this.boardBid =this.boardIdMother?null:boardBid;
        this.boardName = boardName;
        this.boardInfo = boardInfo;
        this.boardMasterId = boardMasterId;
        this.boardImg = this.boardIdMother?mBoardImg:sBoardImg;
        this.boardPostNum = 0;
        this.boardTopicNum = 0;
        this.boardTodayNum = 0;
    }


    public Board(Integer boardId, Boolean boardIdMother, Integer boardBid, String boardName,
                 String boardInfo, Integer boardMasterId, String boardImg, Integer boardPostNum,
                 Integer boardTopicNum, Integer boardTodayNum, String boardLastReply) {
        this.boardId = boardId;
        this.boardIdMother = boardIdMother;
        this.boardBid = boardBid;
        this.boardName = boardName;
        this.boardInfo = boardInfo;
        this.boardMasterId = boardMasterId;
        this.boardImg = boardImg;
        this.boardPostNum = boardPostNum;
        this.boardTopicNum = boardTopicNum;
        this.boardTodayNum = boardTodayNum;
        this.boardLastReply = boardLastReply;
    }

    @Override
    public String toString() {
        return "Board{" +
                "boardId=" + boardId +
                ", boardIdMother=" + boardIdMother +
                ", boardBid=" + boardBid +
                ", boardName='" + boardName + '\'' +
                ", boardInfo='" + boardInfo + '\'' +
                ", boardMasterId=" + boardMasterId +
                ", boardImg='" + boardImg + '\'' +
                ", boardPostNum=" + boardPostNum +
                ", boardTopicNum=" + boardTopicNum +
                ", boardTodayNum=" + boardTodayNum +
                ", boardLstReply='" + boardLastReply + '\'' +
                '}';
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public String getBoardLastReply() {
        return boardLastReply;
    }

    public void setBoardLastReply(String boardLastReply) {
        this.boardLastReply = boardLastReply;
    }

    public Integer getBoardId() {
        return boardId;
    }

    public void setBoardId(Integer boardId) {
        this.boardId = boardId;
    }

    public Boolean getBoardIdMother() {
        return boardIdMother;
    }

    public void setBoardIdMother(Boolean boardIdMother) {
        this.boardIdMother = boardIdMother;
    }

    public Integer getBoardBid() {
        return boardBid;
    }

    public void setBoardBid(Integer boardBid) {
        this.boardBid = boardBid;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public String getBoardInfo() {
        return boardInfo;
    }

    public void setBoardInfo(String boardInfo) {
        this.boardInfo = boardInfo;
    }

    public Integer getBoardMasterId() {
        return boardMasterId;
    }

    public void setBoardMasterId(Integer boardMasterId) {
        this.boardMasterId = boardMasterId;
    }

    public String getBoardImg() {
        return boardImg;
    }

    public void setBoardImg(String boardImg) {
        this.boardImg = boardImg;
    }

    public Integer getBoardPostNum() {
        return boardPostNum;
    }

    public void setBoardPostNum(Integer boardPostNum) {
        this.boardPostNum = boardPostNum;
    }

    public Integer getBoardTopicNum() {
        return boardTopicNum;
    }

    public void setBoardTopicNum(Integer boardTopicNum) {
        this.boardTopicNum = boardTopicNum;
    }

    public Integer getBoardTodayNum() {
        return boardTodayNum;
    }

    public void setBoardTodayNum(Integer boardTodayNum) {
        this.boardTodayNum = boardTodayNum;
    }
}
