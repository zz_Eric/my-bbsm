package com.Eric.BBSM.JavaBean.DataBean;

import java.util.Date;

public class Admin {
    private Integer adminId;
    private String adminUsername;
    private String adminPassword;
    private Integer adminUserId;
    private Date registTime;
    private User user;

    public Admin() {
    }

    //管理员的所有信息的构造器
    public Admin(Integer adminId, String adminUsername, String adminPassword, Integer adminUserId, Date registTime) {
        this.adminId = adminId;
        this.adminUsername = adminUsername;
        this.adminPassword = adminPassword;
        this.adminUserId = adminUserId;
        this.registTime = registTime;
    }

    //注册管理员信息的构造器
    public Admin(String adminUsername, String adminPassword) {
        this.adminId=null;
        this.adminUsername = adminUsername;
        this.adminPassword = adminPassword;
        this.registTime = new Date();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getAdminUsername() {
        return adminUsername;
    }

    public void setAdminUsername(String adminUsername) {
        this.adminUsername = adminUsername;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public Integer getAdminUserId() {
        return adminUserId;
    }

    public void setAdminUserId(Integer adminUserId) {
        this.adminUserId = adminUserId;
    }

    public Date getRegistTime() {
        return registTime;
    }

    public void setRegistTime(Date registTime) {
        this.registTime = registTime;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "adminId=" + adminId +
                ", adminUsername='" + adminUsername + '\'' +
                ", adminPassword='" + adminPassword + '\'' +
                ", adminUserId=" + adminUserId +
                ", registTime=" + registTime +
                '}';
    }
}
