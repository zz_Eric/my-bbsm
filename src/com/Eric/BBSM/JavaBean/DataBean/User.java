package com.Eric.BBSM.JavaBean.DataBean;

import java.util.Date;
import java.util.List;

public class User {

    //1.用户注册信息 5
    private Integer userId;
    private String userUsername;
    private String userPassword;
    private String userSex;
    private Date userRegistTime;

    //2.用户基本信息 7
    private Integer age;
    private Date userBirthday;
    private String userQq;
    private String userEmail;
    private String userFace;
    private String userTel;
    private String userSign;

    //3.系统所需用户信息 12
    private String userMark;
    private String userGrade;
    private Integer userTopic;
    private Integer userWealth;
    private Integer userPost;
    private Integer userDeleteNum;
    private String userGroup;
    private String userLastIp;
    private Date userLastTime;
    private Boolean userLocked;
    private Boolean userLogin;
    private Boolean userAdmin;

    //4.用户找回密码的验证信息 2
    private String userPasswordA;
    private String userPasswordQ;

    //5.用户详细信息 7
    private String userSecondName;
    private String userTrueName;
    private String userBlood;
    private String userZodiac;
    private String userNation;
    private String userProvince;
    private String userCity;

    //6.体现实体间关系的属性
    private List<Board> boards;
    private List<Topic> topics;
    private List<Post> posts;


    public User() {
    }

    public User(String userUsername, String userPassword, String userSex, String userEmail, String userTel) {
        this.userUsername = userUsername;
        this.userPassword = userPassword;
        this.userSex = userSex;
        this.userRegistTime = new Date();   //获取当前时间
        this.userEmail = userEmail;
        this.userTel = userTel;
        //初始化用户信息
        this.userFace = "/static/assets/img/photo/photo_03.png";
        this.userMark = "0";
        this.userGrade = "1";
        this.userTopic = 0;
        this.userWealth = 0;
        this.userPost = 0;
        this.userDeleteNum = 0;
        this.userLocked = false;
        this.userLogin = false;
        this.userAdmin = false;
        this.userSecondName="游客"+System.currentTimeMillis()+"";
    }

    public User(Integer userId, String userUsername, String userPassword, String userSex, Date userRegistTime,
                Integer age, Date userBirthday, String userQq, String userEmail, String userFace, String userTel,
                String userSign, String userMark, String userGrade, Integer userTopic, Integer userWealth, Integer userPost,
                String userGroup, Integer userDeleteNum, String userLastIp, Date userLastTime, Boolean userLocked, Boolean userLogin,
                Boolean userAdmin, String userPasswordA, String userPasswordQ, String userSecondName, String userTrueName,
                String userBlood, String userZodiac, String userNation, String userProvince, String userCity) {

        this.userId = userId;
        this.userUsername = userUsername;
        this.userPassword = userPassword;
        this.userSex = userSex;
        this.userRegistTime = userRegistTime;

        this.age = age;
        this.userBirthday = userBirthday;
        this.userQq = userQq;
        this.userEmail = userEmail;
        this.userFace = userFace;
        this.userTel = userTel;
        this.userSign = userSign;

        this.userMark = userMark;
        this.userGrade = userGrade;
        this.userTopic = userTopic;
        this.userWealth = userWealth;
        this.userPost = userPost;
        this.userGroup = userGroup;
        this.userDeleteNum = userDeleteNum;
        this.userLastIp = userLastIp;
        this.userLastTime = userLastTime;
        this.userLocked = userLocked;
        this.userLogin = userLogin;
        this.userAdmin = userAdmin;

        this.userPasswordA = userPasswordA;
        this.userPasswordQ = userPasswordQ;

        this.userSecondName = userSecondName;
        this.userTrueName = userTrueName;
        this.userBlood = userBlood;
        this.userZodiac = userZodiac;
        this.userNation = userNation;
        this.userProvince = userProvince;
        this.userCity = userCity;

    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userUsername='" + userUsername + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userSex='" + userSex + '\'' +
                ", userRegistTime=" + userRegistTime +
                ", age=" + age +
                ", userBirthday=" + userBirthday +
                ", userQq='" + userQq + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userFace='" + userFace + '\'' +
                ", userTel='" + userTel + '\'' +
                ", userSign='" + userSign + '\'' +
                ", userMark='" + userMark + '\'' +
                ", userGrade='" + userGrade + '\'' +
                ", userTopic=" + userTopic +
                ", userWealth=" + userWealth +
                ", userPost=" + userPost +
                ", userGroup='" + userGroup + '\'' +
                ", userDeleteNum=" + userDeleteNum +
                ", userLastIp='" + userLastIp + '\'' +
                ", userLastTime=" + userLastTime +
                ", userLocked=" + userLocked +
                ", userLogin=" + userLogin +
                ", userAdmin=" + userAdmin +
                ", userPasswordA='" + userPasswordA + '\'' +
                ", userPasswordQ='" + userPasswordQ + '\'' +
                ", userSecondName='" + userSecondName + '\'' +
                ", userTrueName='" + userTrueName + '\'' +
                ", userBlood='" + userBlood + '\'' +
                ", userZodiac='" + userZodiac + '\'' +
                ", userNation='" + userNation + '\'' +
                ", userProvince='" + userProvince + '\'' +
                ", userCity='" + userCity + '\'' +
                '}';
    }

    public List<Board> getBoards() {
        return boards;
    }

    public void setBoards(List<Board> boards) {
        this.boards = boards;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }


    public Integer getUserId() {
        return userId;
    }

    public String getUserUsername() {
        return userUsername;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getUserSex() {
        return userSex;
    }

    public Date getUserRegistTime() {
        return userRegistTime;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(Date userBirthday) {
        this.userBirthday = userBirthday;
    }

    public String getUserQq() {
        return userQq;
    }

    public void setUserQq(String userQq) {
        this.userQq = userQq;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserFace() {
        return userFace;
    }

    public String getUserTel() {
        return userTel;
    }

    public String getUserSign() {
        return userSign;
    }

    public void setUserSign(String userSign) {
        this.userSign = userSign;
    }

    public String getUserMark() {
        return userMark;
    }

    public String getUserGrade() {
        return userGrade;
    }

    public Integer getUserTopic() {
        return userTopic;
    }

    public Integer getUserWealth() {
        return userWealth;
    }

    public Integer getUserPost() {
        return userPost;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public Integer getUserDeleteNum() {
        return userDeleteNum;
    }

    public String getUserLastIp() {
        return userLastIp;
    }

    public void setUserLastIp(String userLastIp) {
        this.userLastIp = userLastIp;
    }

    public Date getUserLastTime() {
        return userLastTime;
    }

    public void setUserLastTime(Date userLastTime) {
        this.userLastTime = userLastTime;
    }

    public Boolean getUserLocked() {
        return userLocked;
    }

    public Boolean getUserLogin() {
        return userLogin;
    }

    public Boolean getUserAdmin() {
        return userAdmin;
    }

    public String getUserPasswordA() {
        return userPasswordA;
    }

    public void setUserPasswordA(String userPasswordA) {
        this.userPasswordA = userPasswordA;
    }

    public String getUserPasswordQ() {
        return userPasswordQ;
    }

    public void setUserPasswordQ(String userPasswordQ) {
        this.userPasswordQ = userPasswordQ;
    }

    public String getUserSecondName() {
        return userSecondName;
    }

    public void setUserSecondName(String userSecondName) {
        this.userSecondName = userSecondName;
    }

    public String getUserTrueName() {
        return userTrueName;
    }

    public void setUserTrueName(String userTrueName) {
        this.userTrueName = userTrueName;
    }

    public String getUserBlood() {
        return userBlood;
    }

    public void setUserBlood(String userBlood) {
        this.userBlood = userBlood;
    }

    public String getUserZodiac() {
        return userZodiac;
    }

    public void setUserZodiac(String userZodiac) {
        this.userZodiac = userZodiac;
    }

    public String getUserNation() {
        return userNation;
    }

    public void setUserNation(String userNation) {
        this.userNation = userNation;
    }

    public String getUserProvince() {
        return userProvince;
    }

    public void setUserProvince(String userProvince) {
        this.userProvince = userProvince;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setUserUsername(String userUsername) {
        this.userUsername = userUsername;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public void setUserRegistTime(Date userRegistTime) {
        this.userRegistTime = userRegistTime;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setUserFace(String userFace) {
        this.userFace = userFace;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    public void setUserMark(String userMark) {
        this.userMark = userMark;
    }

    public void setUserGrade(String userGrade) {
        this.userGrade = userGrade;
    }

    public void setUserTopic(Integer userTopic) {
        this.userTopic = userTopic;
    }

    public void setUserWealth(Integer userWealth) {
        this.userWealth = userWealth;
    }

    public void setUserPost(Integer userPost) {
        this.userPost = userPost;
    }

    public void setUserDeleteNum(Integer userDeleteNum) {
        this.userDeleteNum = userDeleteNum;
    }

    public void setUserLocked(Boolean userLocked) {
        this.userLocked = userLocked;
    }

    public void setUserLogin(Boolean userLogin) {
        this.userLogin = userLogin;
    }

    public void setUserAdmin(Boolean userAdmin) {
        this.userAdmin = userAdmin;
    }
}
