package com.Eric.BBSM.JavaBean.DataBean;

import java.util.Date;
import java.util.List;

/**
 * @author Eric
 * @Create 2020-09-02  19:13
 * @Version Demo
 **/
public class Topic {
    private Integer topicId;
    private Integer topicBoardId;
    private Integer topicUserId;

    private String topicName;
    private Date topicCreateTime;

    private Integer topicHits;
    private Integer topicReplyNum;
    private Integer topicLastReplyId;
    private String topicText;

    //状态信息
    private Boolean topicTop;
    private Boolean topicBest;
    private Boolean topicDel;
    private Boolean topicHot;

    //体现关系的属性
    private User user;
    private Board board;
    private List<Post> posts;
    private User lastReplayer;

    public User getLastReplayer() {
        return lastReplayer;
    }

    public void setLastReplayer(User lastReplayer) {
        this.lastReplayer = lastReplayer;
    }

    public String getTopicText() {
        return topicText;
    }

    public void setTopicText(String topicText) {
        this.topicText = topicText;
    }

    public Topic() {
    }

    public Topic(Integer topicBoardId, Integer topicUserId, String topicName, String topicText,Date time) {
        this.topicBoardId = topicBoardId;
        this.topicUserId = topicUserId;
        this.topicName = topicName;
        this.topicText = topicText;
        this.topicCreateTime = time;
        this.topicReplyNum = 0;
        this.topicLastReplyId = null;
        this.topicTop = false;
        this.topicBest = false;
        this.topicDel = false;
        this.topicHot = false;
   }

    @Override
    public String toString() {
        return "Topic{" +
                "topicId=" + topicId +
                ", topicBoardId=" + topicBoardId +
                ", topicUserId=" + topicUserId +
                ", topicName='" + topicName + '\'' +
                ", topicCreateTime=" + topicCreateTime +
                ", topicHits=" + topicHits +
                ", topicReplyNum=" + topicReplyNum +
                ", topicLastReplyId=" + topicLastReplyId +
                ", topicTop=" + topicTop +
                ", topicBest=" + topicBest +
                ", topicDel=" + topicDel +
                ", topicHot=" + topicHot +
                '}';
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public Integer getTopicBoardId() {
        return topicBoardId;
    }

    public void setTopicBoardId(Integer topicBoardId) {
        this.topicBoardId = topicBoardId;
    }

    public Integer getTopicUserId() {
        return topicUserId;
    }

    public void setTopicUserId(Integer topicUserId) {
        this.topicUserId = topicUserId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public Date getTopicCreateTime() {
        return topicCreateTime;
    }

    public void setTopicCreateTime(Date topicCreateTime) {
        this.topicCreateTime = topicCreateTime;
    }

    public Integer getTopicHits() {
        return topicHits;
    }

    public void setTopicHits(Integer topicHits) {
        this.topicHits = topicHits;
    }

    public Integer getTopicReplyNum() {
        return topicReplyNum;
    }

    public void setTopicReplyNum(Integer topicReplyNum) {
        this.topicReplyNum = topicReplyNum;
    }

    public Integer getTopicLastReplyId() {
        return topicLastReplyId;
    }

    public void setTopicLastReplyId(Integer topicLastReplyId) {
        this.topicLastReplyId = topicLastReplyId;
    }

    public Boolean getTopicTop() {
        return topicTop;
    }

    public void setTopicTop(Boolean topicTop) {
        this.topicTop = topicTop;
    }

    public Boolean getTopicBest() {
        return topicBest;
    }

    public void setTopicBest(Boolean topicBest) {
        this.topicBest = topicBest;
    }

    public Boolean getTopicDel() {
        return topicDel;
    }

    public void setTopicDel(Boolean topicDel) {
        this.topicDel = topicDel;
    }

    public Boolean getTopicHot() {
        return topicHot;
    }

    public void setTopicHot(Boolean topicHot) {
        this.topicHot = topicHot;
    }
}
