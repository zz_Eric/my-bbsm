package com.Eric.BBSM.JavaBean.DataBean;

import java.util.Date;

/**
 * @author Eric
 * @Create 2020-09-02  19:21
 * @Version Demo
 **/
public class Post {
    private Integer postId;
    private Integer postBoardId;
    private Integer postUserId;
    private Integer postTopicId;
    private Integer postReplyId;

    private String postContent;
    private Date postCreateTime;
    private Date postEditTime;

    private String postIpAddress;

    //体现实体间关系的属性
    private User user;
    private Board board;
    private Topic topic;

    public Post() {
    }

    public Post(Integer postId, Integer postBoardId, Integer postUserId, Integer postTopicId,
                Integer postReplyId, String postContent, Date postCreateTime, Date postEditTime,
                String postIpAddress) {
        this.postId = postId;
        this.postBoardId = postBoardId;
        this.postUserId = postUserId;
        this.postTopicId = postTopicId;
        this.postReplyId = postReplyId;
        this.postContent = postContent;
        this.postCreateTime = postCreateTime;
        this.postEditTime = postEditTime;
        this.postIpAddress = postIpAddress;
    }

    @Override
    public String toString() {
        return "Post{" +
                "postId=" + postId +
                ", postBoardId=" + postBoardId +
                ", postUserId=" + postUserId +
                ", postTopicId=" + postTopicId +
                ", postReplyId=" + postReplyId +
                ", postContent='" + postContent + '\'' +
                ", postCreateTime=" + postCreateTime +
                ", postEditTime=" + postEditTime +
                ", postIpAddress='" + postIpAddress + '\'' +
                '}';
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Integer getPostBoardId() {
        return postBoardId;
    }

    public void setPostBoardId(Integer postBoardId) {
        this.postBoardId = postBoardId;
    }

    public Integer getPostUserId() {
        return postUserId;
    }

    public void setPostUserId(Integer postUserId) {
        this.postUserId = postUserId;
    }

    public Integer getPostTopicId() {
        return postTopicId;
    }

    public void setPostTopicId(Integer postTopicId) {
        this.postTopicId = postTopicId;
    }

    public Integer getPostReplyId() {
        return postReplyId;
    }

    public void setPostReplyId(Integer postReplyId) {
        this.postReplyId = postReplyId;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Date getPostCreateTime() {
        return postCreateTime;
    }

    public void setPostCreateTime(Date postCreateTime) {
        this.postCreateTime = postCreateTime;
    }

    public Date getPostEditTime() {
        return postEditTime;
    }

    public void setPostEditTime(Date postEditTime) {
        this.postEditTime = postEditTime;
    }

    public String getPostIpAddress() {
        return postIpAddress;
    }

    public void setPostIpAddress(String postIpAddress) {
        this.postIpAddress = postIpAddress;
    }
}
