package com.Eric.BBSM.Dao.Implement;

import com.Eric.BBSM.Dao.BaseDao;
import com.Eric.BBSM.Dao.UserDao;
import com.Eric.BBSM.JavaBean.DataBean.Board;
import com.Eric.BBSM.JavaBean.DataBean.User;

import java.util.List;

/**
 * @author Eric
 * @Create 2020-09-01  18:35
 * @Version Demo
 **/
public class UserDaoImplement extends BaseDao<User> implements UserDao {

    /**
     * 插入一条新用户信息(注册)
     *
     * @param user
     */
    @Override
    public Integer insertUser(User user) {
        String sql = "insert into users(user_username,user_password,user_sex,user_regist_time," +
                "user_email,user_face,user_tel,user_mark,user_grade,user_topic,user_wealth," +
                "user_post,user_delete_num,user_locked,user_login,user_admin,user_second_name) " +
                "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        int row = this.update(sql, user.getUserUsername(), user.getUserPassword(), user.getUserSex(), user.getUserRegistTime(),
                user.getUserEmail(), user.getUserFace(), user.getUserTel(), user.getUserMark(), user.getUserGrade(),
                user.getUserTopic(), user.getUserWealth(), user.getUserPost(), user.getUserDeleteNum(),
                user.getUserLocked(), user.getUserLogin(), user.getUserAdmin(),user.getUserSecondName());
        return row;
    }

    /**
     * 根据用户名和密码获得对应的用户
     * @param username
     * @param password
     * @return
     */
    @Override
    public User queryUserByUsernameAndPassword(String username, String password) {
        String sql = "select user_id userId,user_name userUsername,user_password userPassword,user_sex userSex,user_regist_time userRegistTime," +
                "user_age age,user_qq userQq,user_email userEmail,user_face userFace,user_tel userTel,user_sign userSign,user_mark userMark,user_grade userGrade," +
                "user_topic userTopic,user_wealth userWealth,user_post userPost,user_group userGroup,user_delete_num userDeleteNum,user_last_ip userLastIp," +
                "user_last_time userLastTime,user_locked userLocked,user_login userLogin,user_admin userAdmin,user_second_name userSecondName" +
                " from users where user_name = ? and user_password=?";
        return this.getBean(sql,username,password);
    }



    @Override
    public List<User> queryAllUsers() {
        return null;
    }

    @Override
    public Integer updateUser(User user) {
        return null;
    }

    @Override
    public User queryUserById(Integer id) {
        String sql ="select user_id userId,user_name userUsername,user_password userPassword,user_sex userSex,user_regist_time userRegistTime," +
                "user_age age,user_qq userQq,user_email userEmail,user_face userFace,user_tel userTel,user_sign userSign,user_mark userMark,user_grade userGrade," +
                "user_topic userTopic,user_wealth userWealth,user_post userPost,user_group userGroup,user_delete_num userDeleteNum,user_last_ip userLastIp," +
                "user_last_time userLastTime,user_locked userLocked,user_login userLogin,user_admin userAdmin,user_second_name userSecondName" +
                " from users where user_id = ?";
        return this.getBean(sql,id);
    }

    @Override
    public User queryUserByBoardId(Integer bid) {
        Board board = new BoardDaoImplement().QueryBoardById(bid);
        return this.queryUserById(board.getBoardMasterId());
    }


}
