package com.Eric.BBSM.Dao.Implement;

import com.Eric.BBSM.Dao.BaseDao;
import com.Eric.BBSM.Dao.PostDao;
import com.Eric.BBSM.JavaBean.DataBean.Post;

import java.util.List;

/**
 * @author Eric
 * @ProjectName my-bbsm
 * @description post的Dao层的实现类
 * @CreateTime 2020-11-17 10:50:33
 */
public class PostDaoImplement extends BaseDao<Post> implements PostDao {
    /**
     * 根据话题Id 查询其下所有的Post信息
     * @param topicId
     * @return
     */
    @Override
    public List<Post> queryAllPostByBorderId(String topicId) {
        String sql = "select " +
                "post_id ,post_board_id,post_user_id,post_topic_id,post_reply_id,post_content,post_time,post_editime,post_ip" +
                " from post " +
                " where post_topic_id = ?";
        return this.getBeanList(sql,topicId);
    }
}
