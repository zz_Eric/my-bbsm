package com.Eric.BBSM.Dao.Implement;

import com.Eric.BBSM.Dao.BaseDao;
import com.Eric.BBSM.Dao.BoardDao;
import com.Eric.BBSM.JavaBean.DataBean.Board;

import java.util.List;

/**
 * @author Eric
 * @Create 2020-09-02  19:07
 * @Version Demo
 **/
public class BoardDaoImplement extends BaseDao<Board> implements BoardDao {

    /**
     * 添加板块
     * @return
     */
    @Override
    public Integer addBoard(Board board) {
        String sql = "insert into board values(null,?,?,?,?,?,?,?,?,?,?)";
        return this.update(sql,board.getBoardIdMother(),board.getBoardId(),board.getBoardName(),
                board.getBoardInfo(),board.getBoardMasterId(),board.getBoardImg(),board.getBoardPostNum(),
                board.getBoardTopicNum(),board.getBoardTodayNum(),board.getBoardLastReply());
    }

    /**
     * 删除板块
     * @param id
     * @return
     */
    @Override
    public Integer delBoardById(Integer id) {
        String sql = "delete from board where board_id = ?";
        return this.update(sql,id);
    }

    /**
     * 更新板块 基本信息  板块名称，板块简介，板块图片
     * @param board
     * @return
     */
    @Override
    public Integer updateBoardBase(Board board) {
        String sql = "update board set board_name=? , board_info=? ,board_img = ? where board_id=?";
        return this.update(sql,board.getBoardName(),board.getBoardInfo(),board.getBoardImg(),board.getBoardId());
    }

    /**
     * 更新版块信息  实时信息  帖子总数，话题总数，当日帖子数
     */
    @Override
    public Integer updateBoardTime(Board board) {
        String sql = "update board set board_post_num=?,board_topic_num=?,board_today_num=? where board_id=?";
        return this.update(sql,board.getBoardPostNum(),board.getBoardTopicNum(),board.getBoardTodayNum(),board.getBoardId());
    }

    /**
     * 更新板块信息  更换版主 版主id
     */
    @Override
    public Integer updateBoardMaster(Board board) {
        String sql = "update board set board_master_id=? where board_id=?";
        return this.update(sql,board.getBoardMasterId(),board.getBoardId());
    }

    /**
     * 更新版块信息 更新  板块最新回复信息
     */
    @Override
    public Integer updateBoardLastReplayInfo(Board board) {
        String sql = "update board set board_last_reply=? where board_id=?";
        return this.update(sql,board.getBoardLastReply(),board.getBoardId());
    }

    /**
     * 根据id查询板块
     * @param id
     * @return
     */
    @Override
    public Board QueryBoardById(Integer id) {
        String sql = "select board_id boardId,board_idMother boardIdMother,board_bid boardBid,board_name boardName," +
                "board_info boardInfo,board_master_id boardMasterId, board_img boardImg, board_post_num boardPostNum," +
                "board_topic_num boardTopicNum, board_today_num boardTodayNum, board_last_reply boardLastReply " +
                "from board where board_id=?";
        return this.getBean(sql,id);
    }

    /**
     * 根据主版块id查询子版块集合
     * @param mid
     * @return
     */
    @Override
    public List<Board> QueryBoardByMotherId(Integer mid) {
        String sql = "select board_id boardId,board_idMother boardIdMother,board_bid boardBid,board_name boardName," +
                "board_info boardInfo,board_master_id boardMasterId, board_img boardImg, board_post_num boardPostNum," +
                "board_topic_num boardTopicNum, board_today_num boardTodayNum, board_last_reply boardLastReply " +
                "from board where board_idMother=?";
        return this.getBeanList(sql,mid);
    }

    /**
     * 查询所有的板块
     * @return
     */
    @Override
    public List<Board> QueryAllBoards() {
        String sql = "select board_id boardId,board_idMother boardIdMother,board_bid boardBid,board_name boardName," +
                "board_info boardInfo,board_master_id boardMasterId, board_img boardImg, board_post_num boardPostNum," +
                "board_topic_num boardTopicNum, board_today_num boardTodayNum, board_last_reply boardLastReply " +
                "from board";
        return this.getBeanList(sql);
    }

}
