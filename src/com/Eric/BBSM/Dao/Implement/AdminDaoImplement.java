package com.Eric.BBSM.Dao.Implement;

import com.Eric.BBSM.Dao.AdminDao;
import com.Eric.BBSM.Dao.BaseDao;
import com.Eric.BBSM.JavaBean.DataBean.Admin;

public class AdminDaoImplement extends BaseDao<Admin> implements AdminDao {
    /**
     *
     * @param admin 根据 admin.adminUsername,admin.adminPassword 查找
     * @return
     * select admin_id,admin_username,admin_password,admin_user_id,regist_time from admins where admin_username = ? and admin_password = ?
     */
    @Override
    public Admin getAdmin(Admin admin) {
        String sql = "select admin_id adminId,admin_username adminUsername,admin_password adminPassword,admin_user_id adminUserId,regist_time registTime from admins where admin_username = ? and admin_password = ?";
        return this.getBean(sql, admin.getAdminUsername(), admin.getAdminPassword());
    }

    /**
     * 根据用户名寻找是否存在该管理员
     * @param admin
     * @return
     * select * from admins where admin_username = ?
     */
    @Override
    public boolean checkAdmin(Admin admin) {
        String sql = "select * from admins where admin_username = ?";
        return this.getBean(sql,admin.getAdminUsername()) != null;
    }

    /**
     * 保存该管理员的注册信息
     * @param admin
     * insert into admins(admin_id,admin_username,admin_password,admin_user_id,regist_time) values(?,?,?,?,?)
     */
    @Override
    public void saveAdmin(Admin admin) {
        String sql = "insert into admins(admin_id,admin_username,admin_password,admin_user_id,regist_time) " +
                " values(?,?,?,?,?)" ;
        update(sql,admin.getAdminId(),admin.getAdminUsername(),admin.getAdminPassword(),admin.getAdminUserId(),admin.getRegistTime());
    }
}
