package com.Eric.BBSM.Dao.Implement;

import com.Eric.BBSM.Dao.BaseDao;
import com.Eric.BBSM.Dao.TopicDao;
import com.Eric.BBSM.JavaBean.DataBean.Topic;

import java.util.List;

/**
 * @author Eric
 * @ProjectName my-bbsm
 * @description 话题Dao层的实现类
 * @CreateTime 2020-10-25 20:13:21
 */
public class TopicDaoImplement extends BaseDao<Topic> implements TopicDao {

    /**
     * 添加话题信息
     * @param topic
     * @return
     */
    @Override
    public Integer addTopic(Topic topic) {
        String sql = "insert into topic (topic_board_id ,topic_user_id ,topic_name ," +
                "topic_create_time ,topic_hits ,topic_replynum ,topic_lastreply_id ," +
                "topic_top ,topic_best ,topic_del ,topic_hot) " +
                "values(?,?,?,?,?,?,?,?,?,?,?)";
        return this.update(sql,topic.getTopicBoardId(),topic.getTopicUserId(),topic.getTopicName(),
                topic.getTopicCreateTime(),topic.getTopicHits(),topic.getTopicReplyNum(),
                topic.getTopicLastReplyId(),topic.getTopicTop(),topic.getTopicBest(),
                topic.getTopicDel(),topic.getTopicHot());
    }

    /**
     * 根据Id删除话题
     * @param id
     * @return
     */
    @Override
    public Integer delTopicById(Integer id) {
        String sql = "delete topic where topic_id=?";
        return update(sql,id);
    }

    /**
     * 更新话题信息
     * @param topic
     * @return
     */
    @Override
    public Integer updateTopic(Topic topic) {
        String sql = "insert into topic (topic_board_id ,topic_user_id ,topic_name ," +
                "topic_create_time ,topic_hits ,topic_replynum ,topic_lastreply_id ," +
                "topic_top ,topic_best ,topic_del ,topic_hot) " +
                "values(?,?,?,?,?,?,?,?,?,?,?)";
        return this.update(sql,topic.getTopicBoardId(),topic.getTopicUserId(),topic.getTopicName(),
                topic.getTopicCreateTime(),topic.getTopicHits(),topic.getTopicReplyNum(),
                topic.getTopicLastReplyId(),topic.getTopicTop(),topic.getTopicBest(),
                topic.getTopicDel(),topic.getTopicHot());
    }

    /**
     * 根据id查询话题信息
     * @param id
     * @return
     */
    @Override
    public Topic QueryTopicById(Integer id) {
        String sql = " select topic_board_id topicBoardId,topic_user_id topicUserId ,topic_name topicName," +
                "topic_create_time topicCreateTime,topic_hits topicHits,topic_replynum topicReplyNum," +
                "topic_lastreply_id topicLastReplyId,topic_top topicTop,topic_best topicBest," +
                "topic_del topicDel,topic_hot topicHot ,topic_text topicText from topic " +
                "where topic_id=?";
        return this.getBean(sql,id);
    }

    /**
     * 根据板块id查询所有话题信息
     * @param boardId
     * @return
     */
    @Override
    public List<Topic> queryAllTopicByBoardId(int boardId) {
        String sql = " select topic_id topicId, topic_board_id topicBoardId,topic_user_id topicUserId ,topic_name topicName," +
                "topic_create_time topicCreateTime,topic_hits topicHits,topic_replynum topicReplyNum," +
                "topic_lastreply_id topicLastReplyId,topic_top topicTop,topic_best topicBest," +
                "topic_del topicDel,topic_hot topicHot ,topic_text topicText from topic " +
                "where topic_board_id = ? ";
        return this.getBeanList(sql,boardId);
    }

    /**
     * 查询所有的话题信息
     * @return
     */
    @Override
    public List<Topic> queryAllTopic() {
     String sql =" select topic_board_id topicBoardId,topic_user_id topicUserId ,topic_name topicName," +
        "topic_create_time topicCreateTime,topic_hits topicHits,topic_replynum topicReplyNum," +
                "topic_lastreply_id topicLastReplyId,topic_top topicTop,topic_best topicBest," +
                "topic_del topicDel,topic_hot topicHot ,topic_text topicText from topic";
        return this.getBeanList(sql);
    }
}











