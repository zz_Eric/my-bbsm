package com.Eric.BBSM.Dao;

import com.Eric.BBSM.JavaBean.DataBean.Post;

import java.util.List;

/**
 * @Discription 帖子的Dao接口
 * @author Eric
 * @Create 2020-09-02  15:02
 * @Version Demo
 **/
public interface PostDao {
    List<Post> queryAllPostByBorderId(String topicId);
}
