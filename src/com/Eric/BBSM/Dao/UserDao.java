package com.Eric.BBSM.Dao;

import com.Eric.BBSM.JavaBean.DataBean.User;

import java.util.List;

/**
 * @author Eric
 * @Create 2020-09-01  18:34
 * @Version Demo
 **/
public interface UserDao {

    /**
     * 插入一条新用户信息
     */
    public Integer insertUser(User user);

    /**
     * 根据用户名密码查询用户信息
     */
    public User queryUserByUsernameAndPassword(String username,String password);

    /**
     * 查询所有用户信息
     */
    public List<User> queryAllUsers();

    /**
     * 修改用户信息
     */
    public Integer updateUser(User user);

    /**
     * 根據id查詢該用戶信息
     */
    public User queryUserById(Integer id);

    /**
     * 根據 版塊Id查詢版主
     */
    public User queryUserByBoardId(Integer bid);
}
