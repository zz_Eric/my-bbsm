package com.Eric.BBSM.Dao;

import com.Eric.BBSM.JavaBean.DataBean.Board;

import java.util.List;

/**
 * @author Eric
 * @Create 2020-09-02  15:01
 * @Version Demo
 **/
public interface BoardDao {

    /**
     * 添加板块信息
     */
    public Integer addBoard(Board board);

    /**
     * 根据id删除板块信息
     */
    public Integer delBoardById(Integer id);


    /**
     * 更新版块信息  基本信息  板块名称，板块简介，板块图片
     */
    public Integer updateBoardBase(Board board);
    /**
     * 更新版块信息  实时信息  帖子总数，话题总数，当日帖子数
     */
    public Integer  updateBoardTime(Board board);
    /**
     * 更新板块信息  更换版主 版主id
     */
    public Integer  updateBoardMaster(Board board);
    /**
     * 更新版块信息 更新  板块最新回复信息
     */
    public Integer  updateBoardLastReplayInfo(Board board);



    /**
     * 根据id查询版块信息
     */
    public Board QueryBoardById(Integer id);

    /**
     * 根据MotherId查询版块信息 所属板块id
     */
    public List<Board> QueryBoardByMotherId(Integer mid);

    /**
     * 查询所有的板块信息
     */
    public List<Board> QueryAllBoards();

}
