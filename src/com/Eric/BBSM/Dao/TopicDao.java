package com.Eric.BBSM.Dao;

import com.Eric.BBSM.JavaBean.DataBean.Board;
import com.Eric.BBSM.JavaBean.DataBean.Topic;

import java.util.List;

/**
 * @author Eric
 * @Create 2020-09-02  15:01
 * @Version Demo
 **/
public interface TopicDao {

    /**
     * 添加话题信息
     * @return
     */
    public Integer addTopic(Topic topic);

    /**
     * 根据Id删除话题
     * @return
     */
    public Integer delTopicById(Integer id);

    /**
     * 更新话题信息
     * @param topic
     * @return
     */
    public Integer updateTopic(Topic topic);

    /**
     * 根据id查询话题信息
     */
    public Topic QueryTopicById(Integer id);

    /**
     * 根据板块id查询所有话题信息
     */
    public List<Topic> queryAllTopicByBoardId(int boardId);

    /**
     * 查询所有的话题信息
     */
    public List<Topic> queryAllTopic();

}
