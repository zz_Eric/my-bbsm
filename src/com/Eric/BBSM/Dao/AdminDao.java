package com.Eric.BBSM.Dao;

import com.Eric.BBSM.JavaBean.DataBean.Admin;

public interface AdminDao {

    /**
     * 根据管理员和密码去寻找数据库中是否存在Admin对象
     * @param admin 根据 admin.adminUsername,admin.adminPassword 查找
     * @return
     * "select admin_id adminId,admin_username adminUsername,admin_password adminPassword,admin_user_id adminUserId, "
     * " regist_time registTime from admins where admin_username = ? and admin_password = ?";
     */
    public Admin getAdmin(Admin admin);

    /**
     * 根据用户名寻找是否存在该管理员
     * @param admin
     * @return
     */
    public boolean checkAdmin(Admin admin);

    /**
     * 保存该管理员的注册信息
     * @param admin
     */
    public void saveAdmin(Admin admin);

}
