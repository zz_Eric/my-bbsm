package com.Eric.BBSM.Service;

import com.Eric.BBSM.JavaBean.DataBean.Admin;

public interface AdminService {
    /**
     * 根据管理员和密码去寻找数据库中是否存在Admin对象
     * @param admin 根据 admin.adminUsername,admin.adminPassword 查找
     * @return
     */
    public Admin login(Admin admin);

    /**
     * 根据用户名寻找是否存在该管理员 用于注册判断
     * @param admin
     * @return
     */
    public boolean regist(Admin admin);

    /**
     * 保存该管理员的注册信息
     * @param admin
     */
    public void saveAdmin(Admin admin);
}
