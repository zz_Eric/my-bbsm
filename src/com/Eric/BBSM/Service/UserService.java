package com.Eric.BBSM.Service;

import com.Eric.BBSM.JavaBean.DataBean.User;

/**
 * @author Eric
 * @Create 2020-09-01  20:21
 * @Version Demo
 **/
public interface UserService {

    /**
     * 实现注册业务
     */
    public Integer regist(User user);

    /**
     * 根据用户名密码查询用户信息
     */
    public Boolean checkRegist(String username, String password);

    /**
     * 根据用户名密码查询用户信息
     * @param username
     * @param password
     * @return
     */
    public User getUser(String username, String password);

    /**
     * 根據id查詢該用戶信息
     */
    public User queryUserById(String id);
}
