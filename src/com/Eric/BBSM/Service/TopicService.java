package com.Eric.BBSM.Service;

import com.Eric.BBSM.JavaBean.DataBean.Topic;

import java.text.ParseException;
import java.util.List;

/**
 * @author Eric
 * @ProjectName my-bbsm
 * @description 话题的业务层接口:增删改查
 * @CreateTime 2020-10-25 21:42:47
 */
public interface TopicService {

    /**
     *   添加话题信息
     * @param boardId
     * @param userId
     * @param topicName
     * @param topicText
     * @param createTime
     * @return
     * @throws ParseException
     */
    public Integer addTopic(String boardId,String userId,String topicName,String topicText,String createTime) throws ParseException;

    /**
     * 根据Id删除话题
     * @return
     */
    public Integer delTopicById(String id);

    /**
     * 更新话题信息
     * @param topic
     * @return
     */
    public Integer updateTopic(Topic topic);

    /**
     * 根据id查询话题信息
     */
    public Topic QueryTopicById(String id);

    /**
     * 根据板块id查询所有话题信息
     */
    public List<Topic> queryAllTopicByBoardId(String boardId);

    /**
     * 查询所有的话题信息
     */
    public List<Topic> queryAllTopic();


}
