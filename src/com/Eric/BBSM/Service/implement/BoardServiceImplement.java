package com.Eric.BBSM.Service.implement;

import com.Eric.BBSM.Dao.BoardDao;
import com.Eric.BBSM.Dao.Implement.BoardDaoImplement;
import com.Eric.BBSM.Dao.Implement.UserDaoImplement;
import com.Eric.BBSM.Dao.UserDao;
import com.Eric.BBSM.JavaBean.DataBean.Board;
import com.Eric.BBSM.JavaBean.DataBean.User;
import com.Eric.BBSM.Service.BoardService;

import java.util.List;

/**
 * @author Eric
 * @Create 2020-09-02  21:13
 * @Version Demo
 **/
public class BoardServiceImplement implements BoardService {

    private UserDao userDao = new UserDaoImplement();
    private BoardDao boardDao = new BoardDaoImplement();
    /**
     *添加版块信息
     * @param board
     * @return
     */
    @Override
    public Integer addBoard(Board board) {

        return boardDao.addBoard(board);
    }

    /**
     *查询所有的版块信息
     * @return
     */
    @Override
    public List<Board> queryAllBoard() {

        return boardDao.QueryAllBoards();
    }

    @Override
    public Integer delBoardById(String id) {
        return boardDao.delBoardById(Integer.valueOf(id));
    }

    @Override
    public Integer updateBoardBase(Board board) {
        return null;
    }

    @Override
    public Integer updateBoardTime(Board board) {
        return null;
    }

    @Override
    public Integer updateBoardMaster(Board board) {
        return null;
    }

    @Override
    public Integer updateBoardLastReplayInfo(Board board) {
        return null;
    }

    @Override
    public Board QueryBoardById(String id) {
        return boardDao.QueryBoardById(Integer.valueOf(id));
    }

    @Override
    public List<Board> QueryBoardByMotherId(String mid) {
        return boardDao.QueryBoardByMotherId(Integer.valueOf(mid));
    }
    @Override
    public User queryUserByBoardId(String boardId) {
        return userDao.queryUserByBoardId(Integer.valueOf(boardId));
    }


}
