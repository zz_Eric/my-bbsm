package com.Eric.BBSM.Service.implement;

import com.Eric.BBSM.Dao.Implement.UserDaoImplement;
import com.Eric.BBSM.Dao.UserDao;
import com.Eric.BBSM.JavaBean.DataBean.User;
import com.Eric.BBSM.Service.UserService;

/**
 * @author Eric
 * @Create 2020-09-01  20:23
 * @Version Demo
 **/
public class UserServiceImplement implements UserService {

    private UserDao userDao = new UserDaoImplement();

    //实现注册业务！
    @Override
    public Integer regist(User user) {
        return  userDao.insertUser(user);
    }

    //用户已存在为 true
    @Override
    public Boolean checkRegist(String username, String password) {
        return userDao.queryUserByUsernameAndPassword(username,password)!=null;
    }

    @Override
    //根据用户名密码，查询用户
    public User getUser(String username, String password) {
        return userDao.queryUserByUsernameAndPassword(username,password);
    }

    /**
     * 根據id查詢該用戶信息
     */
    @Override
    public User queryUserById(String id) {
        return userDao.queryUserById(Integer.valueOf(id));
    }

}
