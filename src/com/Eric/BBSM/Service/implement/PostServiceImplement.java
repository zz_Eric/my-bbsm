package com.Eric.BBSM.Service.implement;

import com.Eric.BBSM.Dao.BoardDao;
import com.Eric.BBSM.Dao.Implement.BoardDaoImplement;
import com.Eric.BBSM.Dao.Implement.PostDaoImplement;
import com.Eric.BBSM.Dao.Implement.TopicDaoImplement;
import com.Eric.BBSM.Dao.Implement.UserDaoImplement;
import com.Eric.BBSM.Dao.PostDao;
import com.Eric.BBSM.Dao.TopicDao;
import com.Eric.BBSM.Dao.UserDao;
import com.Eric.BBSM.JavaBean.DataBean.Board;
import com.Eric.BBSM.JavaBean.DataBean.Post;
import com.Eric.BBSM.JavaBean.DataBean.Topic;
import com.Eric.BBSM.JavaBean.DataBean.User;
import com.Eric.BBSM.Service.BoardService;
import com.Eric.BBSM.Service.PostService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Eric
 * @ProjectName my-bbsm
 * @description Post的service层实现类
 * @CreateTime 2020-11-17 10:48:30
 */
public class PostServiceImplement implements PostService {

    private UserDao userDao = new UserDaoImplement();
    private BoardService boardService = new BoardServiceImplement();
    private PostDao postDao = new PostDaoImplement();
    private TopicDao topicDao = new TopicDaoImplement();
    /**
     * 根据话题id查询 该话题所有的post，以及话题对应的作者信息，板块信息
     * @param topicId 话题Id
     * @return
     */
    @Override
    public Map<String,Object> queryAllPostByBorderId(String topicId) {
        HashMap<String, Object> retMap = new HashMap<>();
        if(!"".equals(topicId)){
            Topic topic = topicDao.QueryTopicById(Integer.valueOf(topicId));
            Board board =  boardService.QueryBoardById(String.valueOf(topic.getTopicBoardId()));
            List<Post> postList = postDao.queryAllPostByBorderId(topicId);
            User user = userDao.queryUserByBoardId(board.getBoardId());

            retMap.put("postList",postList);
            retMap.put("board",board);
            retMap.put("user",user);
            retMap.put("topic",topic);
        }
        return retMap;
    }
}
