package com.Eric.BBSM.Service.implement;

import com.Eric.BBSM.Dao.AdminDao;
import com.Eric.BBSM.Dao.Implement.AdminDaoImplement;
import com.Eric.BBSM.JavaBean.DataBean.Admin;
import com.Eric.BBSM.Service.AdminService;

public class AdminServiceImplement implements AdminService {

        private AdminDao adminDao = new AdminDaoImplement();
    /**
     * @param admin 根据 admin.adminUsername,admin.adminPassword 查找
     * @return
     */
    @Override
    public Admin login(Admin admin) {
        return adminDao.getAdmin(admin);
    }

    /**
     * 根据用户名寻找是否存在该管理员用于注册判断
     * @param admin
     * @return
     */
    @Override
    public boolean regist(Admin admin) {
        return adminDao.checkAdmin(admin);
    }

    /**
     * 保存该管理员的注册信息
     * @param admin
     */
    @Override
    public void saveAdmin(Admin admin) {
        adminDao.saveAdmin(admin);
    }
}
