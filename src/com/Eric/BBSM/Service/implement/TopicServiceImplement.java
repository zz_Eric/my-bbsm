package com.Eric.BBSM.Service.implement;

import com.Eric.BBSM.Dao.Implement.TopicDaoImplement;
import com.Eric.BBSM.Dao.TopicDao;
import com.Eric.BBSM.JavaBean.DataBean.Topic;
import com.Eric.BBSM.Service.TopicService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Eric
 * @ProjectName my-bbsm
 * @description 话题业务层的实现类:增删改、查询所有或某个
 * @CreateTime 2020-10-25 21:44:23
 */
public class TopicServiceImplement implements TopicService {

    private TopicDao topicDao = new TopicDaoImplement();

    @Override
    public Integer addTopic(String boardId, String userId, String topicName, String topicText, String createTime) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date time =  simpleDateFormat.parse(createTime);
        Topic topic = new Topic(Integer.valueOf(boardId), Integer.valueOf(userId), topicName, topicText, time);
        return topicDao.addTopic(topic);
    }

    @Override
    public Integer delTopicById(String id) {
        return topicDao.delTopicById(Integer.valueOf(id));
    }

    @Override
    public Integer updateTopic(Topic topic) {
        return topicDao.updateTopic(topic);
    }

    @Override
    public Topic QueryTopicById(String id) {
        return topicDao.QueryTopicById(Integer.valueOf(id));
    }

    @Override
    public List<Topic> queryAllTopicByBoardId(String boardId) {
        return topicDao.queryAllTopicByBoardId(Integer.parseInt(boardId));
    }
    @Override
    public List<Topic> queryAllTopic() {
        return topicDao.queryAllTopic();
    }

}
