package com.Eric.BBSM.Service;

import com.Eric.BBSM.JavaBean.DataBean.Board;
import com.Eric.BBSM.JavaBean.DataBean.User;

import java.util.List;

/**
 * @author Eric
 * @Create 2020-09-02  21:10
 * @Version Demo
 **/
public interface BoardService {

    /**
     * 添加版块信息
     */
    public Integer addBoard(Board board);

    /**
     * 查询所有的版块信息
     */
    public List<Board> queryAllBoard();

    /**
     * 根据id删除板块信息
     */
    public Integer delBoardById(String id);

    /**
     * 更新版块信息  基本信息  板块名称，板块简介，板块图片
     */
    public Integer updateBoardBase(Board board);
    /**
     * 更新版块信息  实时信息  帖子总数，话题总数，当日帖子数
     */
    public Integer  updateBoardTime(Board board);
    /**
     * 更新板块信息  更换版主 版主id
     */
    public Integer  updateBoardMaster(Board board);
    /**
     * 更新版块信息 更新  板块最新回复信息
     */
    public Integer  updateBoardLastReplayInfo(Board board);

    /**
     * 根据id查询版块信息
     */
    public Board QueryBoardById(String id);

    /**
     * 根据MotherId查询版块信息 所属板块id
     */
    public List<Board> QueryBoardByMotherId(String mid);

    /**
     * 根據板塊Id查詢 版主信息
     */
    public User queryUserByBoardId(String boardId);

    /**
     * 根據板塊Id查詢 最后回复者信息
     */
    //public User queryReplayerByBoardId(String boardId);
}
