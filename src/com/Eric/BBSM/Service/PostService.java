package com.Eric.BBSM.Service;

import com.Eric.BBSM.JavaBean.DataBean.Post;

import java.util.List;
import java.util.Map;

/**
 * @author Eric
 * @ProjectName my-bbsm
 * @description 帖子的service层接口
 * @CreateTime 2020-11-17 10:47:52
 */
public interface PostService {
    Map<String,Object> queryAllPostByBorderId(String topicId);
}
