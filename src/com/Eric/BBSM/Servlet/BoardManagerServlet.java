package com.Eric.BBSM.Servlet;

import com.Eric.BBSM.JavaBean.DataBean.Board;
import com.Eric.BBSM.Service.BoardService;
import com.Eric.BBSM.Service.implement.BoardServiceImplement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Eric
 * @Create 2020-09-04  15:08
 * @Version Demo
 **/
@WebServlet("/BoardManagerServlet")
public class BoardManagerServlet extends BaseServlet {

    private BoardService boardService = new BoardServiceImplement();

    protected void queryAllBoards(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Board> allBoards = boardService.queryAllBoard();
        System.out.println("allBoards = " + allBoards);
        request.setAttribute("allBoards",allBoards);
        request.getRequestDispatcher("/after-pages/boardManager/boardContent.jsp").forward(request,response);
    }
}
