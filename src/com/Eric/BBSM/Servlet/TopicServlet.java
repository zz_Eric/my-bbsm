package com.Eric.BBSM.Servlet;

import com.Eric.BBSM.Dao.BoardDao;
import com.Eric.BBSM.JavaBean.DataBean.Board;
import com.Eric.BBSM.JavaBean.DataBean.Topic;
import com.Eric.BBSM.JavaBean.DataBean.User;
import com.Eric.BBSM.Service.BoardService;
import com.Eric.BBSM.Service.TopicService;
import com.Eric.BBSM.Service.UserService;
import com.Eric.BBSM.Service.implement.BoardServiceImplement;
import com.Eric.BBSM.Service.implement.TopicServiceImplement;
import com.Eric.BBSM.Service.implement.UserServiceImplement;
import sun.text.resources.FormatData;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Eric
 * @ProjectName my-bbsm
 * @description 话题的业务请求
 * @CreateTime 2020-10-25 21:47:50
 */
@WebServlet("/TopicServlet")
public class TopicServlet extends BaseServlet {
    private UserService userService = new UserServiceImplement();
    private BoardService boardService = new BoardServiceImplement();
    private TopicService topicService = new TopicServiceImplement();

   //准备好  添加话题数据  前所需要的基本数据  楼主名，所属板块，当前时间
    //  before-pages/topic/add_topic.jsp
   protected void preAddTopic(HttpServletRequest request, HttpServletResponse response) throws Exception {
      //1.获取请求参数
       String boardMaster = request.getParameter("boardMaster");
       String boardId = request.getParameter("boardId");
       System.out.println(boardMaster);
       System.out.println(boardId);
       String boardName = boardService.QueryBoardById(boardId).getBoardName();
       System.out.println(boardName);
       //3.共享数据
       SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       String currentTime = simpleDateFormat.format(new Date());

       request.setAttribute("boardMaster",boardMaster);
       request.setAttribute("boardId",boardId);
       request.setAttribute("boardName",boardName);
       request.setAttribute("currentTime",currentTime);
      //4.跳转页面
       request.getRequestDispatcher("/before-pages/topic/add_topic.jsp").forward(request,response);
   }

    /**
     * 根据板块Id查询所有的话题
     * @param request
     * @param response
     * @throws Exception
     */
    protected void QueryAllTopicByBoardId(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //1.获取板块Id
        String boardId = request.getParameter("boardId");
        //2.调用Service层方法
        User boardMaster = boardService.queryUserByBoardId(boardId);
        Board board = boardService.QueryBoardById(boardId);
        List<Topic> topics = topicService.queryAllTopicByBoardId(boardId);
        for (Topic topic : topics) {
            topic.setUser(userService.queryUserById(String.valueOf(topic.getTopicUserId())));
        }
        //3.共享数据
        request.setAttribute("boardMaster",boardMaster);
        request.setAttribute("board",board);
        request.setAttribute("topics",topics);
        //4.跳转页面
        request.getRequestDispatcher("/before-pages/topic/topicContent.jsp").forward(request,response);
    }

    //添加话题的请求处理
    protected void addTopic(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //获取请求参数
        String boardId = request.getParameter("boardId");
        String userId = request.getParameter("userId");
        String topicName = request.getParameter("topicName");
        String createTime = request.getParameter("createTime");
        String topicText = request.getParameter("topicText");
        //调用service层添加话题
        System.out.println(boardId);
        System.out.println(userId);
        System.out.println(topicName);
        System.out.println(createTime);
        System.out.println(topicText);
        topicService.addTopic(boardId,String.valueOf(userId),topicName,topicText,createTime);
        //跳转页面
        request.getRequestDispatcher("/TopicServlet?method=QueryAllTopicByBoardId&boardId="+boardId).forward(request,response);
    }
}
