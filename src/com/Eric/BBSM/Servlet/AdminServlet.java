package com.Eric.BBSM.Servlet;

import com.Eric.BBSM.JavaBean.DataBean.Admin;
import com.Eric.BBSM.Service.AdminService;
import com.Eric.BBSM.Service.implement.AdminServiceImplement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "AdminServlet",urlPatterns = "/AdminServlet")
public class AdminServlet extends BaseServlet {

        private AdminService adminService = new AdminServiceImplement();

    protected void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        //1.获取请求参数
        String username = request.getParameter("username");//null. "".
        String password = request.getParameter("password");
        Admin admin1 = new Admin(username, password);
        //2.调用Service方法
        Admin admin = adminService.login(admin1);
        System.out.println("admin = " + admin);
        if(admin != null)
        {
            //登陆成功！
            //3.将数据共享到域中
            session.setAttribute("admin",admin);
            //4.跳转页面
            response.sendRedirect(request.getContextPath()+"/after-pages/afterMain.jsp");
        }else{
            //登陆失败！
            //4.跳转页面
            response.sendRedirect(request.getContextPath()+"/after-pages/admin/login.jsp");
        }
    }

    protected void regist(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        session.removeAttribute("admin");
        response.sendRedirect(request.getContextPath()+"/after-pages/afterMain.jsp");
    }

}
