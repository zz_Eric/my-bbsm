package com.Eric.BBSM.Servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author Eric
 */
@WebServlet(name = "BaseServlet")
public class BaseServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //处理字符集问题
        //处理post请求的解码问题
        request.setCharacterEncoding("UTF-8");
        //处理响应的编码问题
        response.setContentType("text/html;encoding=UTF_8");

        //获取方法名的请求参数
        String method = request.getParameter("method");
        System.out.println("method = " + method);
        try {
            Method methodObj = this.getClass().getDeclaredMethod(method, HttpServletRequest.class, HttpServletResponse.class);
            //调用方法
            methodObj.invoke(this,request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
