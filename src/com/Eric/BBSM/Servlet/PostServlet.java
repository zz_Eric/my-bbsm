package com.Eric.BBSM.Servlet;

import com.Eric.BBSM.JavaBean.DataBean.Board;
import com.Eric.BBSM.JavaBean.DataBean.Post;
import com.Eric.BBSM.JavaBean.DataBean.Topic;
import com.Eric.BBSM.JavaBean.DataBean.User;
import com.Eric.BBSM.Service.PostService;
import com.Eric.BBSM.Service.implement.PostServiceImplement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Eric
 * @ProjectName my-bbsm
 * @description 创建Post功能的前后端交互层(视图层)
 * @CreateTime 2020-11-17 10:00:07
 */
@WebServlet("/PostServlet")
public class PostServlet extends BaseServlet {

    private PostService postService = new PostServiceImplement();

    protected void queryAllPostByTopicId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String topicId = request.getParameter("topicId");

        System.out.println("topicId= "+topicId+"=====End");

        Map<String,Object> retMap = postService.queryAllPostByBorderId(topicId);
        List<Post> postList = (List<Post>) retMap.get("postList");
        Board postBoard = (Board)retMap.get("board");
        User postUser = (User)retMap.get("user");
        Topic postTopic =  (Topic)retMap.get("topic");

        System.out.println("postList = " + postList);
        request.setAttribute("postList",postList);
        request.setAttribute("postBoard",postBoard);
        request.setAttribute("postUser",postUser);
        request.setAttribute("postTopic",postTopic);
        request.getRequestDispatcher("/before-pages/post/replays.jsp").forward(request,response);
    }
}
