package com.Eric.test;

/**
 * 类描述 获取当前源文件名称以及代码行号
 * 项目名称 my-bbsm
 * 创建时间： 2021-09-15 08:51:25
 * 版本号：v1.0
 *
 * @author Eric
 */
public class GetCurrSourceNameAndLine extends Thread {

    public static void main(String[] args) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        //获取文件名
        String fileName = stackTrace[1].getFileName();
        //获取代码行号
        int lineNumber = stackTrace[1].getLineNumber();
        //获取方法名
        String methodName = stackTrace[1].getMethodName();
        //获取类名
        String className = stackTrace[1].getClassName();

        System.out.println("文件名："+fileName);
        System.out.println("行号"+lineNumber);
        System.out.println("方法名"+methodName);
        System.out.println("类名"+className);

        System.out.println("栈中0位置====>"+stackTrace[0].getFileName());
        System.out.println("栈中0位置====>"+stackTrace[0].toString());

        System.out.println("栈中1位置====>"+stackTrace[1].getFileName());
        System.out.println("栈中1位置====>"+stackTrace[1].toString());

        test();
    }

    public static void test(){
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        //获取文件名
        String fileName = stackTrace[1].getFileName();
        //获取代码行号
        int lineNumber = stackTrace[1].getLineNumber();
        //获取方法名
        String methodName = stackTrace[1].getMethodName();
        //获取类名
        String className = stackTrace[1].getClassName();

        System.out.println("文件名："+fileName);
        System.out.println("行号"+lineNumber);
        System.out.println("方法名"+methodName);
        System.out.println("类名"+className);

        System.out.println("栈中0位置====>"+stackTrace[0].getFileName());
        System.out.println("栈中0位置====>"+stackTrace[0].toString());

        System.out.println("栈中1位置====>"+stackTrace[1].getFileName());
        System.out.println("栈中1位置====>"+stackTrace[1].toString());

        System.out.println("栈中2位置====>"+stackTrace[2].getFileName());
        System.out.println("栈中2位置====>"+stackTrace[2].toString());


    }

}
