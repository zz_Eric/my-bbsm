package com.Eric.test;

import sun.text.resources.FormatData;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Eric
 * @ProjectName my-bbsm
 * @description 测试
 * @CreateTime 2020-10-26 18:53:10
 */
public class Test {

    public static void main(String[] args) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(new Date());
        System.out.println(format);
        System.out.println(new Date());
    }
}
