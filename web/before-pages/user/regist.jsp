<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>用户注册界面</title>
	<%@include file="/WEB-INF/include/base.jsp"%>
    <!--注册的css文件引用-->
    <link rel="stylesheet" href="static/assets/css/reg.css"/>

	<script>
		$(function(){

			$(".form button").click(function(){

				var regexUserName = /^[\w_]{6,12}$/;
				var regexEmail =  /^[a-zA-Z0-9_-]+@([a-zA-Z0-9]+\.)+(com|cn|net|org)$/;
				var regexTel = /^[0-9]{11}$/;

				var username = $(".username").val();
				var password = $(".password").val();
				var repeat = $(".repeat").val();
				var email = $(".email").val();
				var tel = $(".tel").val();
				var radio = $(".radio").val();

				if(!regexUserName.test(username) || username==null)
				{
					alert("用户名不能为空，或输入有误!请重新输入！【6，12】位的字母或数字");
					return false;
				}
				if(password==null || password != repeat)
				{
					alert("密码不能为空，或两次密码输入不一致！请重新输入！");
					return false;
				}
				if(!regexEmail.test(email) || email == null)
				{
					alert("邮箱不能为空，或输入有误!请重新输入！【6，12】位的字母或数字");
					return false;
				}
				if(!regexTel.test(tel)|| tel == null)
				{
					alert("电话不能为空，或输入有误!请重新输入！【6，12】位的字母或数字");
					return false;
				}
			});

		});

	</script>

</head>

<body >

	<div id="regist-box">
		<h2><a href="before-pages/user/login.jsp">Login</a>/Sign Up</h2>
		<form action="UserServlet?method=regist" autocomplete="off" method="post">
			<%--<input type="hidden" name="method" value="regist">--%>
		<div class="form">
			<div class="item">
				<i class="fa fa-user-circle-o" aria-hidden="true"></i>
				<input class="username" type="text" placeholder="Username" name="username" value="">
			</div>
		    <div class="item">
		    	<i class="fa fa-key" aria-hidden="true"></i>
				<input class="password" type="password" placeholder="Password" name="password" value="">
		    </div>
		    <div class="item">
		    	<i class="fa fa-unlock" aria-hidden="true"></i>
				<input class="repeat"  type="password" placeholder="确认密码" name="repeat" value="">
		    </div>
		    <div class="item">
				<i class="fa fa-envelope-o" aria-hidden="true"></i>
				<input class="email" type="email" placeholder="Email" name="email" value="">
			</div>
			 <div class="item">
				<i class="fa fa-phone" aria-hidden="true"></i>
				<input class="tel" type="tel" placeholder="电话" name="tel" value="">
			</div>
			<div class="radio" >
				<i class="fa fa-mars" aria-hidden="true">男</i><input type="radio"  name="sex" value="男" checked>
				<i class="fa fa-venus" aria-hidden="true">女</i><input type="radio"  name="sex" value="女">
			</div>
			<button type="submit">Sign</button>
		</div>
		</form>
	</div>
     


</body>
</html>
