<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>前台内容</title>
	<%@include file="/WEB-INF/include/base.jsp"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/assets/css/main_content.css"/>

</head>
<body>

<div class="javaex-block has-border">
	<div class="javaex-banner">
		<span class="javaex-block-fixed"></span><B>Our Home </B>&nbsp; We are family!
		<%--动态时间效果--%>
		<%--<div align="right"><%=new Date()%></div>--%>
	</div>
	<div class="javaex-main-10" align="center">
		<p class="javaex-toast">
			<h2><b>浣溪沙</b></h2><br/>
		&nbsp;&nbsp; >>纳兰性德< <br/>
		<hr class="javaex-divider"/>
			<font style="font-family: 'Adobe 仿宋 Std R' " size="4">残雪凝辉冷画屏，落梅横笛已三更。更无人处月胧明。</font><br/>
			<font style="font-family: 'Adobe 仿宋 Std R' " size="4">我是人间惆怅客，知君何事泪纵横。断肠声里忆平生。</font><br/>
		</p>
		<p class="javaex-toast"></p>
	</div>
</div>
 <!--主体内容-->
<div class="javaex-block">
	<!--块元素-->
	<%--显示所有的主版块，然后分别显示每个主版块下的子版块--%>
	 <c:forEach items="${requestScope.allBoards}" var="mBoard">
	<div class="javaex-block">
		<!--页面有多个表格时，可以用于标识表格-->
			<c:if test="${mBoard.boardIdMother}">
				<div class="board_box" align="left">
				<img alt="${mBoard.boardName}" src="${pageContext.request.contextPath}${mBoard.boardImg}" width="60px" height="60px"/>
				<h3><font color="#337AB7"><B>${mBoard.boardName}</B></font></h3>
				</div>
				<div class="board_data">
					<ul>
						<li><a><b>主题:  <span>${mBoard.boardTopicNum}</span></b> </a></li>
						<li><a><b>帖子:  <span>${mBoard.boardPostNum}</span></b> </a></li>
					</ul>
				</div>
		<!--正文内容-->
		<div class="javaex-main">
			<div class="javaex-block">
				<!--等分系统-->
				<!--下列li将会分成4等分-->
				<ul class="javaex-equal-4 clear">
						<%--循环打印此版块下的 所有 子版块--%>
					<c:forEach items="${requestScope.allBoards}" var="board">
					<c:if test="${board.boardBid == mBoard.boardId}">
					<li>
						<a href="TopicServlet?method=QueryAllTopicByBoardId&boardId=${board.boardId}">
							<img alt="${board.boardName}" src="${pageContext.request.contextPath}${board.boardImg}" width="60px" height="30px" style="margin-top:10px"/>
							<b><font face="KaiTi_GB2312" color="#777777" size="2">${board.boardName}</font></b>
						</a>
						</c:if>
						</c:forEach>
				</ul>
			</div>
			<hr class="javaex-divider"/>
			</c:if>
		</div>
		</c:forEach>
	</div>
</div>


</body>
</html>
