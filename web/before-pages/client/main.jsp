<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <title>论坛首页</title>
  <%@include file="/WEB-INF/include/base.jsp"%>
</head>

<body>
<div class="javaex-navbar">
  <div class="javaex-container-fluid clear">
    <!--logo名称-->
    <div class="javaex-logo">论坛</div>

    <!--左侧导航-->
    <ul class="javaex-nav fl">
      <li class="active"><a href="javascript:;">首页</a></li>
      <li><a href="javascript:;">概览</a></li>
      <li>
        <a href="javascript:;">文学</a>
        <ul class="javaex-nav-dropdown-menu">
          <li><a href="#">小说</a></li>
          <li><a href="#">散文</a></li>
        </ul>
      </li>
    </ul>

    <!--右侧-->
    <ul class="javaex-nav fr">
      <c:if test="${not empty sessionScope.user}">
      <li>
        <a href="javascript:;">欢迎您，${user.userSecondName == "" ? user.userSecondName:user.userUsername}</a>
        <ul class="javaex-nav-dropdown-menu" style="right: 10px;">
          <li><a href="UserServlet?method=logout">退出当前账号</a></li>
        </ul>
      </li>
      </c:if>
      <c:if test="${empty sessionScope.user}">
      <li><a href="before-pages/user/login.jsp"  style="color: white;">登录</a></li>
      <li>/</li>
      <li><a href="before-pages/user/regist.jsp"  style="color: white;">注册</a></li>
      </c:if>
    </ul>
  </div>
</div>

<div class="javaex-main-container">
  <!--左侧菜单-->
  <div class="javaex-aside javaex-aside-fixed">
    <h1><span class="javaex-nav-name">总览</span></h1>
    <div id="javaex-toc" class="javaex-toc">
      <div class="javaex-menu-container">
        <div id="menu" class="javaex-menu">
          <ul>
              <c:if test="${not empty sessionScope.user}">
            <li class="javaex-menu-item hover">
              <a href="after-pages/admin/login.jsp"  target="_blank"> &gt login后台系统</a>
            </li>
              </c:if>
            <li class="javaex-menu-item">
              <a href="javascript:;">个人中心<i class="icon-keyboard_arrow_left"></i></a>
              <ul>
                <li><a href="javascript:page('Front-end/user/user_info_check.jsp');">个人主页</a></li>
                <li><a href="javascript:page('Front-end/user/change_password.jsp');">修改密码</a></li>
                <li><a href="javascript:page('Front-end/user/user_edit.jsp');">修改信息</a></li>
              </ul>
            </li>
            <li class="javaex-menu-item">
              <a href="javascript:;">我的板块<i class="icon-keyboard_arrow_left"></i></a>
              <ul>
                <li><a href="javascript:page('Front-end/user/user_info_check.jsp');">申请版主</a></li>
                <li><a href="javascript:page('Front-end/user/change_password.jsp');">板块管理</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

    <!--iframe载入内容-->
    <div class="javaex-markdown">
      <iframe id="page" src="BoardServlet?method=queryAllBoard"></iframe>
    </div>

</div>

</body>

<script>
  var heightUrl = "/before-pages/client/main_content.jsp";
  javaex.menu({
    id : "menu",
    isAutoSelected : true,
    key : "",
    url : heightUrl
  });

  $(function() {
    // 设置左侧菜单高度
    setMenuHeight();
  });

  /**
   * 设置左侧菜单高度
   */
  function setMenuHeight() {
    var height = document.documentElement.clientHeight - $("#javaex-toc").offset().top;
    height = height - 10;
    $("#javaex-toc").css("height", height+"px");
  }

  // 控制页面载入
  function page(url) {
    $("#page").attr("src", url);
  }
</script>

</html>
