<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>话题</title>
    <%@include file="/WEB-INF/include/base.jsp"%>
    <!--话题页面css文件的引用-->
    <link rel="stylesheet" href="static/assets/css/before-topicContent.css"/>

</head>

<body class="javaex-main">

    <div class="topic_nav" id="topic_nav">
            <span class="border_name">${requestScope.board.boardName}</span>
            <span class="border">版主:${requestScope.boardMaster.userUsername}</span>
            <span class="border_time">最新回复：${requestScope.board.boardLastReply}</span>
        <div class="function" id="function">
            <ul>
                <li>主题:<span class="topic_number">${requestScope.board.boardTopicNum}</span></li>
                <li>帖子:<span class="post_number">${requestScope.board.boardPostNum}</span></li>
            </ul>
                <a href="TopicServlet?method=preAddTopic&boardMaster=${requestScope.boardMaster.userUsername}&boardId=${requestScope.board.boardId}"><font size="2">发布主题</font></a>
        </div>
    </div>


<ul class="topic_nav_content">
    <!--foreach循环打印输出-->
    <c:forEach items="${requestScope.topics}" var="topic" >
	<li>
      <div class="topic" id="topic">
            <hr>
            <div class="photo" id="photo">
                <img alt=" " width="99" height="99" src="${topic.user.userFace}" style="border-radius: 50%;overflow:hidden;"/>
            </div>

            <div class="user_name" id="user_name">
              <span>${topic.user.userUsername}</span>
            </div>

            <div class="content_topic" id="content_topic">
                    <span class="topic_name">
                        <a href="PostServlet?method=queryAllPostByTopicId&topicId=${topic.topicId}">${topic.topicName}</a>
                        <a href="<%--topic_edit.jsp?topic_id=<%=tp.topic_id%>--%>#"><font size="2">管理</font></a>
                    </span>

                    <span class="other">In@${requestScope.board.boardName}&nbsp;${topic.topicCreateTime}</span>
                    <span class="best">赞: 0</span>
                    <span class="bad">踩: 0</span>
            </div>

            <div class="last_replay" id="last_replay">
                <span class="blank"> 最新回复者:</span>
                <c:if test="${topic.topicLastReplyId == null}">
                <span class="user">NULL</span>
                <%--<span class="time"></span>--%>
                </c:if>
                <c:if test="${topic.topicLastReplyId != null}">
                    <span class="user">${topic.lastReplayer.userUsername}</span>
                    <%--<span class="time">${}</span>--%>
                </c:if>
            </div>
      </div>
  </li>
    </c:forEach>
</ul>
</body>
</html>

