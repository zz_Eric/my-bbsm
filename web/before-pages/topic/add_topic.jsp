<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>发布主题页面</title>
	<%@include file="/WEB-INF/include/base.jsp"%>
</head>
<body>

	<!--主体内容-->
	<div class="javaex-list-content">
		<!--块元素-->
		<div class="javaex-block">
			<!--页面有多个表格时，可以用于标识表格-->
			<h2>发布主题</h2>

			<!--正文内容-->
			<div class="javaex-main">
	    <form id="form" action="TopicServlet?method=addTopic" method="post">

			<!-- 隐藏域 -->
			<input type="hidden" name="boardId" value="${requestScope.boardId}">
			<input type="hidden" name="userId" value="${sessionScope.user.userId}">

			<!--文本框-->
			<div class="javaex-unit clear">
				<div class="javaex-unit-left"><p class="subtitle required">楼主</p></div>
				<div class="javaex-unit-right">
					<input type="text" class="javaex-text readonly" name="master" style="width: 250px;" value="@${requestScope.boardMaster}"  readonly autocomplete="off"/>
				</div>
			</div>

			<!--单选框-->
			<div class="javaex-unit clear">
				<div class="javaex-unit-left"><p class="subtitle required" >所属板块</p></div>
				<div class="javaex-unit-right">
					<input type="text" class="javaex-text readonly" style="width: 250px;" value="@${requestScope.boardName}"  readonly autocomplete="off"/>
				</div>
			</div>

			<!--复选框-->
			<div class="javaex-unit clear">
				<div class="javaex-unit-left"><p class="subtitle required">主题名称</p></div>
				<div class="javaex-unit-right">
					<input type="text"  name="topicName" class="javaex-text" style="width: 220px;" placeholder="请输入主题名称" />
				</div>
			</div>


			<!--日期选择框-->
			<div class="javaex-unit clear">
				<div class="javaex-unit-left"><p class="subtitle">发布时间</p></div>
				<div class="javaex-unit-right">
					<input type="text" name="createTime" class="javaex-text readonly" style="width: 250px;" value="${requestScope.currentTime}"  readonly autocomplete="off"/>
				</div>
			</div>

			<!--文本域-->
			<div class="javaex-unit clear">
				<div class="javaex-unit-left"><p class="subtitle">话题内容</p></div>
				<div class="javaex-unit-right">
					<textarea class="javaex-desc" name="topicText" placeholder="请填写内容"></textarea>
					<!--提示说明-->
					<p class="javaex-hint">话题内容中不得包含令人反感的信息，且长度应在10到255个字符之间。</p>
				</div>
			</div>

			<!--提交按钮-->
			<div class="javaex-unit clear tc">
				<input type="button" id="return" class="javaex-btn no" value="返回" onclick="javascript:history.back();"/>
				<input type="button" id="save" class="javaex-btn yes" value="保存" />
			</div>
				</form>
			</div>
		</div>
	</div>

	<script>
	javaex.select({
		id : "select",
		isSearch : false
	});
	
	javaex.tag({
		id : "tag"
	});
	
	// 监听点击保存按钮事件
	$("#save").click(function() {
		// 表单验证函数
		if (javaexVerify()) {
			// 返回错误信息时，可以添加自定义异常提示。参数为元素id和提示
			// addErrorMsg("username", "用户名不存在");
			// 提交
			 $("#form").submit();
			alert("验证通过");
		}
	});

	// 监听点击返回按钮事件
	$("#return").click(function() {
		alert("返回");
		// window.location.href = document.referrer;
	});
</script>



</body>
</html>