<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>帖子页面</title>
    <%@include file="/WEB-INF/include/base.jsp" %>
    <!--帖子页面css文件的引用-->
    <link rel="stylesheet" href="static/assets/css/before-replays.css"/>
</head>

<body class="boddyy">

<div id="main" class="main">
    <div id="photo">
        <img width="48" height="48" src="" style="border-radius: 50%;overflow:hidden;" alt=""/>
    </div>

    <div id="topic_name">
        楼主@${requestScope.postUser.userUsername}&nbsp;${requestScope.postTopic.topicCreateTime}
        <a href="add_replay_post.jsp?user_id=${requestScope.postUser.userId}&&Current_user=&${sessionScope.user.userId}&&topic_id=&{requestScope.postTopic.topicId}">评论</a>
    </div>

    <div class="content" id="content">
        <b>
            ${requestScope.postTopic.topicName}
        </b>
        <br>
        <br>
        ${requestScope.postTopic.topicText}
    </div>
</div>


<div id="nav">
    <ul>
        <li><span class="box">板块</span> <a
                href="#">${requestScope.postBoard.boardName}
        </a></li>
        <li><span class="box">楼主</span><a href="#"> ${requestScope.postUser.userUsername}
        </a></li>
        <li><span class="box">发布时间</span><span class="time"> ${requestScope.postTopic.topicCreateTime}</span></li>
        <li><a href="#<%--//user.puser_id--%>">发表帖子</a></li>
    </ul>
</div>

<ul>
   <%--循环迭代--%>
    <li>
        <div id="replays" class="replays">
            <div id="photo">
                <img width="48" height="48" src="static/assets/img/photo/def-photo.jpg <%--<%=post_user.puser_face %>--%>" style="border-radius: 50%;overflow:hidden;"/>
            </div>

            <div id="topic_name">
                @张三 2020-11-23 12:03:11 <%--<%=post_user.puser_name %> <%=pos.post_time%>--%> <a href="add_replay_post.jsp">回复</a>
            </div>

            <div class="content" id="content">
                <b>11233123 <%--<%=post_topic.topic_name%>--%>
                </b>
                <br>
                <br>
                123441412<%--<%=pos.post_content %>--%>
            </div>
        </div>
    </li>

    <li>
        <%--错误提示：重复引用的id--%>
        <div id="replays" class="replays">

            <div id="photo">
                <img width="48" height="48" src="assets/img/photo/photo_11.png"
                     style="border-radius: 50%;overflow:hidden;"/>
            </div>

            <div id="topic_name">
                @root 2020-04-27 13:34 <a href="add_replay_post.jsp">回复</a>
            </div>

            <div class="content" id="content">
                <b>测试主题1</b>
                <br>
                <br>
                @root 我回复我自己。
            </div>
        </div>
    </li>
</ul>

<div id="main" class="main">

    <div id="photo">
        <img width="48" height="48" src="assets/img/photo/photo_11.png<%--<%=user.puser_face %>--%>" style="border-radius: 50%;overflow:hidden;"/>
    </div>

    <div id="topic_name">
        楼主@root 2020-04-27 13:34 <%--<%=user.puser_name%> <%=tp.topic_time%>--%> <a href="#<%--Front-end/user/user_info_check.jsp--%>">评论</a>
    </div>

    <div class="content" id="content">
        <b>
            咦？这是嘛呀?
            <%--<%=tp.topic_name%>--%>
        </b>
        <br>
        <br>
        这里什么都没有。。。
        <%--<%=tp.topic_info%>--%>
    </div>
</div>


</body>
</html>
