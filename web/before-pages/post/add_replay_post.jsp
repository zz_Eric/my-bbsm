<%@page import="FunCtion.Topic"%>
<%@page import="jdbc.DBOTopic"%>
<%@page import="jdbc.DBOUser"%>
<%@page import="HumanM.PUser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>发表帖子</title>

<%@ include file="javaex_folder/javaex_EX1.jsp" %>

</head>
<body>
<%
	//得到被回复用户的id
	int rePlayForUserId =Integer.parseInt(request.getParameter("user_id"));
	//得到当前用户的id
    int Current_userid =Integer.parseInt(request.getParameter("Current_user"));
	//得到当前话题的id
	int current_topicid = Integer.parseInt(request.getParameter("topic_id"));
	
	
	//创建对用户PUser数据库操作的DBOUser类
	DBOUser dbuser = new DBOUser();
	
	//创建PUser对象forRePlayer 接收被回复者的信息
	PUser forRePlayer = dbuser.Q_user_info(dbuser.Q_user_info_ofid(rePlayForUserId));
	
	//创建PUser对象forRePlayer 接收当前用户的信息
	PUser RePlayer = dbuser.Q_user_info(dbuser.Q_user_info_ofid(Current_userid));
	
	//创建对Topic数据库操作的DBOTopic类
	DBOTopic dbotopic = new DBOTopic();
	
	//创建Topic类来接收当前所要回复的主体
	//Topic topic = 
    
    

    
%>

	<!--主体内容-->
	<div class="list-content">
		<!--块元素-->
		<div class="block">
			<!--页面有多个表格时，可以用于标识表格-->
			<h2>发表帖子</h2>
			<!--右上角的返回按钮-->
			<a href="javascript:history.back();">
				<button class="button indigo radius-3"
					style="position: absolute; right: 20px; top: 16px;">
					<span class="icon-arrow_back"></span> 返回
				</button>
			</a>

			<!--正文内容-->
			<div class="main">

			<!--文本框-->
					<div class="unit clear">
						<div class="left">
							<span class="required">*</span>
							<p class="subtitle">回复主题</p>
						</div>
						<div class="right">
							<input type="text"  data-type="必填" class="text readonly" readonly
						autocomplete="off" id="replay_topic" name="replay_topic" value="@<%=forRePlayer.puser_name %>"/>
						</div>
					</div>

				<form id="form" action="add_task_action.jsp">
					<!--文本域-->
					<div class="unit clear">
						<div class="left">
							<span class="required">*</span>
							<p class="subtitle">内容</p>
						</div>
						<div class="right">
							<textarea class="desc" placeholder="请填写内容" name="replay_info"
								id="replay_info"></textarea>
							<!--提示说明-->
							<p class="hint">请填写帖子。帖子中不得包含令人反感的信息。</p>
						</div>
					</div>


					<!--提交按钮-->
					<div class="unit clear" style="width: 800px;">
						<div style="text-align: center;">
							<!--表单提交时，必须是input元素，并指定type类型为button，否则ajax提交时，会返回error回调函数-->
							<input type="button" id="save" class="button yes" value="发表帖子" />
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<script>
	javaex.select({
		id : "select",
		isSearch : false
	});
	
	javaex.tag({
		id : "tag"
	});
	
	// 监听点击保存按钮事件
	$("#save").click(function() {
		// 表单验证函数
		if (javaexVerify()) {
			// 返回错误信息时，可以添加自定义异常提示。参数为元素id和提示
			// addErrorMsg("username", "用户名不存在");
			// 提交
			 $("#form").submit();
			alert("验证通过");
		}
	});

	// 监听点击返回按钮事件
	$("#return").click(function() {
		alert("返回");
		// window.location.href = document.referrer;
	});
</script>
</body>
</html>