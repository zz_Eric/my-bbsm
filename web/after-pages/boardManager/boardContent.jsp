<%--
  Created by IntelliJ IDEA.
  User: Eric
  Date: 2020-09-02
  Time: 16:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>板块管理主页面</title>
    <%@include file="/WEB-INF/include/base.jsp"%>
</head>
<body>

    <div class="javaex-main">
        <div class="javaex-breadcrumb">
            <span>快速入口</span>
            <span class="divider">/</span>
            <span class="active">板块管理</span>
        </div>
    </div>

<div class="javaex-main">
        <div class="javaex-block">
            <div class="javaex-list-toolbar clear">
                <span class="fl"><button class="javaex-btn blue" onclick="add()">添加</button></span>
                <span class="fr">
                    <div class="javaex-search-group">
                        <input type="text" class="javaex-text" id="keyword" placeholder="请输入搜索内容" />
                        <button class="javaex-btn blue" onclick="search()">搜索</button>
                    </div>
                </span>
            </div>

            <table id="table" class="javaex-table th-c td-c color2">
            <thead>
            <tr>
                <th class="javaex-table-num-col"></th>
                <th class="javaex-table-filter-col"><input type="checkbox" class="javaex-fill" listen="listen-1"/> </th>
                <th>板块名</th>
                <th>板主</th>
                <th>板块主题总数</th>
                <th>板块当日发帖总数</th>
                <th>操作</th>
                <th>详情</th>
                <th>最后回复</th>
            </tr>
            </thead>
            <tbody>
          <c:forEach items="${requestScope.allBoards}" var="boards">
            <tr>
                <td>${boards.boardId}</td>
                <td><input type="checkbox" class="javaex-fill listen-1-2" /> </td>
                <td>${boards.boardName}</td>
                <td>${boards.user.userUsername}</td>
                <td>${boards.boardTopicNum}</td>
                <td>${boards.boardTodayNum}</td>
                <td>编辑&nbsp删除</td>
                <td><a>详情</a></td>
                <td>${boards.boardLastReply}</td>
            </tr>
          </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="javaex-page" align="center">
        <ul id="page" class="javaex-pagination"></ul>
    </div>
    <script>
        //添加板块
        function add(){
            window.location.href="after-pages/boardManager/add_board.jsp";
        }

        javaex.page({
            id : "page",    // jquery选择器，指定到ul节点
            pageNum : 1,           // 默认选中第几页
            pageSize : 10,         // 每页显示多少条
            totalPages : 12,       // 总页数
            isShowJumpPage : true, // 是否显示跳页
            totalNum : 125,        // 总条数，不填时，不显示
            position : "left",
            callback : function(rtn) {
                console.log("当前选中的页数：" + rtn.pageNum);
                console.log("每页显示条数：" + rtn.pageSize);
            }
        });

    </script>
</div>
</body>
</html>
