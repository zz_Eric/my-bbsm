<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>添加板块页面</title>
	<%@include file="/WEB-INF/include/base.jsp"%>
</head>
<body>

<div class="javaex-main">
	<div class="javaex-breadcrumb">
		<span>板块管理</span>
		<span class="divider">/</span>
		<span class="active">添加板块页面</span>
	</div>


 <!--主体内容-->
	<div class="javaex-main">
	<!--块元素-->
	<div class="javaex-list-toolbar clear">
		<!--右上角的返回按钮-->
		<a href="javascript:history.back();">
			<button class="javaex-btn indigo" style="position: absolute;right: 20px;top: 16px;"><span class="icon-arrow_back"></span> 返回</button>
		</a>
	</div>
		<!--正文内容-->
			<form id="form" action="add_board_action.jsp">
				<!--文本框-->
				<div class="javaex-unit clear">
					<div class="javaex-unit-left"><p class="subtitle required">板块名称</p></div>
					<div class="javaex-unit-right">
						<input type="text" class="javaex-text" data-type="必填" placeholder="请输入板块名称" style="width: 30%;" />
					</div>
				</div>
				
				<!--下拉选择框-->
				<div class="javaex-unit clear">
					<div class="javaex-unit-left"><p class="subtitle">所属板块</p></div>
					<div class="javaex-unit-right">
						<select id="board_of" name="board_of">
							<option value="0">无</option>

							<option value=<%--<%=bo.board_id%>><%=bo.board_name %>--%></option>

						</select>
					</div>
				</div>
				<!--下拉选择框-->
				<div class="javaex-unit clear">
					<div class="javaex-unit-left"><p class="subtitle required">任命版主</p></div>
					<div class="javaex-unit-right">
						<select id="select" data-type="required">
							<option value="">请选择</option>
							<option value="1">root</option>
						</select>
					</div>
				</div>

				<!--文本域-->
				<div class="javaex-unit clear">
					<div class="javaex-unit-left"><p class="subtitle">简介</p></div>
					<div class="javaex-unit-right">
						<textarea class="javaex-desc" placeholder="请填写简介" name="board_info" id="board_info"></textarea>
						<!--提示说明-->
						<p class="javaex-hint">请填写板块简介。简介中不得包含令人反感的信息，且长度应在10到255个字符之间。</p>
					</div>
				</div>

				<!--提交按钮-->
				<div class="javaex-unit clear tc">
						<!--表单提交时，必须是input元素，并指定type类型为button，否则ajax提交时，会返回error回调函数-->
						<input type="button" id="save" class="javaex-btn yes" value="保存" />
				</div>
			</form>
		  </div>
</div>
     
<script>
	javaex.select({
		id : "select",
		isSearch : false
	});
	
	javaex.tag({
		id : "tag"
	});
	
	// 监听点击保存按钮事件
	$("#save").click(function() {
		// 表单验证函数
		if (javaexVerify()) {
			// 返回错误信息时，可以添加自定义异常提示。参数为元素id和提示
			// addErrorMsg("username", "用户名不存在");
			// 提交
			 $("#form").submit();
			alert("验证通过");
		}
	});

	// 监听点击返回按钮事件
	$("#return").click(function() {
		alert("返回");
		// window.location.href = document.referrer;
	});
</script>
</body>
</html>
