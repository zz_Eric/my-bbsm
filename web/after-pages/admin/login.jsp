<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>后台管理员登录界面</title>
	<%@include file="/WEB-INF/include/base.jsp"%>
	<!--登录的css文件引用-->
	<link rel="stylesheet" href="static/assets/css/login.css"/>
</head>

<body >

	<!--登录区域-->
	<div id="login-box">
		<h2>论坛后台登录</h2>
		<form action="AdminServlet?method=login" autocomplete="off" method="post">
		<div class="form">
			<!--用户名-->
			<div class="item">
				<i class="fa fa-user-circle-o" aria-hidden="true"></i>
				<input type="text" placeholder="Admin" name="username">
			</div>
			<!--密码-->
		    <div class="item">
		    	<i class="fa fa-key" aria-hidden="true"></i>
				<input type="password" placeholder="Password" name="password">
		    </div>
		</div>
		<button type="submit">Login</button>
		 </form>
	</div>
     


</body>
</html>
