<%--
  Created by IntelliJ IDEA.
  User: Eric
  Date: 2020-08-30
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>
<!--引入核心包-->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<base href="${pageContext.request.contextPath}/">

<!--使用javaex框架-->
<!--字体图标-->
<link href="static/javaex/pc/css/icomoon.css" rel="stylesheet" />
<!--动画-->
<link href="static/javaex/pc/css/animate.css" rel="stylesheet" />
<!--骨架样式-->
<link href="static/javaex/pc/css/common.css" rel="stylesheet" />
<!--皮肤-->
<link href="static/javaex/pc/css/skin/default.css" rel="stylesheet" />
<!--jquery，不可修改版本-->
<script src="static/javaex/pc/lib/jquery-1.7.2.min.js"></script>
<!--核心组件-->
<script src="static/javaex/pc/js/javaex.min.js"></script>
<!--表单验证-->
<script src="static/javaex/pc/js/javaex-formVerify.js"></script>

<!--引入jQuery-1.7.2.js文件-->
<script src="static/javaex/pc/lib/jquery-1.7.2.min.js"></script>

<!--font-awesome文件资源的引用-->
<link rel="stylesheet" href="static/font-awesome-4.7.0/css/font-awesome.min.css"/>
