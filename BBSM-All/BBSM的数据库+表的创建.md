### BBSM的数据库+表的创建

#### 1.创建数据库

```mysql
create database bbsm;
```

#### 2.创建表
##### 1]用户

###### 数据库表 --用户表

```mysql
#--用户表
create table users(
   -- #1用户注册信息5
    user_id int primary key, #--用户ID
    user_name char(32),#--用户名
    user_password char(32), #--密码
    user_sex char(2), #--性别
    user_regist_time datetime, #--注册时间
   -- #2用户基本信息7
    user_age int , #年龄
    user_birthday datetime, #--生日
    user_QQ char(32), #--QQ号
    user_Email char(32),#--Email
    user_face char(32), #--头像地址
    user_tel char(20), #--手机号
    user_sign text, #--个人签名
    
	-- #3用户系统信息12
    user_mark  int,  #--积分
    user_grade char(20),# --用户等级
    user_topic  int, #--发表话题总数
    user_wealth int, #-- 用户财富
    user_post  int, #--发表帖子总数
    user_delete_num  int, #--被删帖子总数
    
    user_group  char(20), #--所属门派
    user_lastip  char(20), # --最后登录IP
    user_last_time datetime,# --上次访问时间
    user_locked char(1), #--用户是否被锁定，禁登录
    user_admin char(1), # --管理员身份判断
    user_login char(1), # --用户是否登录
    
    -- #4用户找回密码验证信息2
    user_password_a  char(50), #--取回密码答案
    user_password_q  char(50), #--取回密码提问
  
    -- #5用户详细信息7
    user_second_name  char(20),# --用户昵称
    user_true_name  char(20), #--真实姓名
    user_blood  char(10), #--血型
    user_zodiac  char(20),# --生肖
    user_nation  char(20), # --民族
    user_province  char(20), #--省份
    user_city  char(20)#--城市
);
```

###### pojo类  --User

```java
public class User {

    //1.用户注册信息 5
    private Integer userId;
    private String userUsername;
    private String userPassword;
    private String userSex;
    private Date userRegistTime;

    //2.用户基本信息 7
    private Integer age;
    private Date userBirthday;
    private String userQq;
    private String userEmail;
    private String userFace;
    private String userTel;
    private String userSign;

    //3.系统所需用户信息 12
    private String userMark;
    private String userGrade;
    private Integer userTopic;
    private Integer userWealth;
    private Integer userPost;
    private Integer userDeletenum;
    
    private String userGroup;
    private String userLastIp;
    private Date userLastTime;
    private Boolean userLocked;
    private Boolean userLogin;
    private Boolean userAdmin;

    //4.用户找回密码的验证信息 2
    private String userPasswordA;
    private String userPasswordQ;

    //5.用户详细信息 7
    private String userSecondName;
    private String userTrueName;
    private String userBlood;
    private String userZodiac;
    private String userNation;
    private String userProvince;
    private String userCity;
	//共33个数据库字段(5+7+12+2+7=33)
    
    //6.体现实体间关系的属性
    private List<Board> boards;
    private List<Topic> topics;
    private List<Post> posts;
    
    // getter&setter 
}
```


##### 2]管理员

###### 数据库表 --管理员表

```mysql
#--管理员表
create table admins(
 admin_id int  primary key , #--管理员ID
 admin_username char(32), #--管理员名字
 admin_password char(32), #--管理员密码
 admin_user_id int , #--管理员前台用户名
 regist_time datetime  # -- 管理员注册时间
-- foreign key(admin_user_id) references PUser(user_id)
);
```

###### pojo类  --Admin

```java
public class Admin {
    private Integer adminId;
    private String adminUsername;
    private String adminPassword;
    private Integer adminUserId;
    private Date registTime;
    private User user;
    
    //...
}
```

##### 3]板块

###### 数据库表 --板块表

```mysql
 #--板块表
create table Board(
    board_id int primary key, #--板块ID
    board_idMother char(10), #--是否为主版块
   board_bid int, #--所属板块
   board_name char(50), #--板块名称
   board_info text,#--板块说明
   board_master_id int, #--版主ID
   board_img  char(20), #--板块LOGO 地址
   board_post_num int, #--板块帖子数
   board_topic_num int, #--板块主题总数
   board_today_num int,#--板块当日发帖数
   board_lastreply int #-- 板块最新回复
);
```

###### pojo类  --Board

```java
public class Board {
    private Integer boardId;
    private Boolean boardIdMother;
    private Integer boardBid;

    private String boardName;
    private String boardInfo;

    private Integer boardMasterId;
    private String boardImg;

    private Integer boardPostNum;
    private Integer boardTopicNum;
    private Integer boardTodayNum;

    private String boardLastReply;

    //体现实体间关系的属性
    private User user;
    private List<Topic> topics;
    private List<Post> posts;
    
   // ...
}
```

##### 4]话题

###### 数据库表 --话题表

```mysql
#--话题表
create table Topic(
    topic_id int primary key,#--话题ID
    topic_board_id int ,#--所属板块ID
    topic_user_id int,#--发帖者ID
    topic_name char(50),#--话题名称
   topic_create_time datetime,#--话题发表时间
   topic_hits int,#--话题浏览量
   topic_replynum int,#--话题回复量
   topic_lastreply_id int,#--最后回复者
   topic_top char(1),#--是否置顶
   topic_best char(1),#--是否加精
   topic_del char(1),#--是否已被删帖
   topic_hot char(1)  #--是否热门话题
);
```

###### pojo类  --Topic

```java
public class Topic {
    private Integer topicId;
    private Integer topicBoardId;
    private Integer topicUserId;

    private String topicName;
    private Date topicCreateTime;

    private Integer topicHits;
    private Integer topicReplyNum;
    private Integer topicLastReplyId;

    //状态信息
    private Boolean topicTop;
    private Boolean topicBest;
    private Boolean topicDel;
    private Boolean topicHot;

    //体现关系的属性
    private User user;
    private Board board;
    private List<Post> posts;
    
    //...
}
```

##### 5]帖子

###### 数据库表 --帖子表

```mysql
#--帖子表
create table Post(
                     post_id int primary key,#--帖子ID
                     post_board_id int ,#--所属板块ID
                     post_user_id int,#--发帖者ID
                     post_topic_id int,#--所属话题ID
                     post_reply_id int,#--所回复话题ID
                     post_content text,#--帖子内容
                     post_time datetime,#--发表时间
                     post_editime datetime,#--重新编辑时间
                     post_ip char(50)#--发帖者所在IP地址
);
```

###### pojo类  --Post

```java
public class Post {
    private Integer postId;
    private Integer postBoardId;
    private Integer postUserId;
    private Integer postTopicId;
    private Integer postReplyId;

    private String postContent;
    private Date postCreateTime;
    private Date postEditTime;

    private String postIpAddress;

    //体现实体间关系的属性
    private User user;
    private Board board;
    private Topic topic;
    
    //...
}
```

##### 6]广告

###### 数据库表 --广告表

```mysql
#--友情链接表
create table FLink(

  link_id int primary key,#--友情链接ID
  link_name char(50),#--网站名称
  link_url char(50),#--网站URL
  link_info text,#--网站简介
  link_logo char(50),#--LOGO地址
  link_islogo char(10),#--是否有LOGO
  link_ispass char(10) #--是否通过验证

);
```

###### pojo类  --

##### 7]友情链接

###### 数据库表 --友情链接表

###### pojo类  --



3.需要修改的問題

​	1]板块表 添加字段  createTime 创建板块的时间

​	2]话题表添加 **赞**与**踩**  字段，添加最后回复语句的String字段。

​	3]话题表添加内容字段