package jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import FunCtion.Topic;

public class DBOTopic extends DBOPer {


	/***********1.添加话题信息     (传入话题 tp 对象)*******************/	
	public void Insert_Topic_data(Topic tp)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.SQL插入语句
		String sql="INSERT into Topic(topic_id,topic_board_id,topic_user_id,topic_name,topic_time,topic_hits, topic_replynum,topic_lastreply_id,topic_top,topic_best,topic_del,topic_hot,topic_info) "
				  + "values(?,?,?,?,?,?, ?,?,?,?,?,?,?)";
		
		//3.加载驱动    建立连接
		con=getConnection();
		
		try {
		//4.输入SQL插入语句	
		st=con.prepareStatement(sql);
		
		
		st.setInt(1,tp.topic_id);
		st.setInt(2,tp.topic_board_id);
		st.setInt(3,tp.topic_user_id);
		st.setString(4,tp.topic_name);
		st.setTimestamp(5,new java.sql.Timestamp(System.currentTimeMillis()));
		st.setInt(6,0);
		st.setInt(7,0);
		st.setInt(8,-1);
		st.setInt(9,checkis(false));
		st.setInt(10,checkis(false));
		st.setInt(11,checkis(false));
		st.setInt(12,checkis(false));
		st.setString(13, tp.topic_info);
		
 	    //5.执行SQL插入语句
		st.executeUpdate();
		
		}catch(Exception e)
		{
			System.out.println("新话题信息添加异常！"+e.getMessage());
		}
		
		//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
	}
	/***********1.1新建话题信息     对话话题进行初始化(传入话题 tp 对象)*******************/	
	public void new_Topic_data(Topic tp)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.SQL插入语句
		String sql="INSERT into Topic(topic_id,topic_board_id,topic_user_id,topic_name,topic_time,topic_hits, topic_replynum,topic_lastreply_id,topic_top,topic_best,topic_del,topic_hot,topic_info) "
				  + "values(?,?,?,?,?,?, ?,?,?,?,?,?,?)";
		
		//3.加载驱动    建立连接
		con=getConnection();
		
		try {
		//4.输入SQL插入语句	
		st=con.prepareStatement(sql);
		
		//初始化话题信息
		st.setInt(1,tp.topic_id);
		st.setInt(2,tp.topic_board_id);
		st.setInt(3,tp.topic_user_id);
		st.setString(4,tp.topic_name);
		st.setDate(5,(Date) tp.topic_time);
		st.setInt(6,0);
		st.setInt(7,0);
		st.setInt(8,-1);
		st.setInt(9,checkis(false));
		st.setInt(10,checkis(false));
		st.setInt(11,checkis(false));
		st.setInt(12,checkis(false));
		st.setString(13, tp.topic_info);
		
 	    //5.执行SQL插入语句
		st.executeUpdate();
		
		}catch(Exception e)
		{
			System.out.println("新建话题信息添加异常！"+e.getMessage());
		}
		
		//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
	}
	
	/***********2.删除话题信息     (传入话题 tp 对象的 编号ID :topic_id)*******************/	
	public void Delete_Topic_data(int topic_id)   
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL删除语句
				String sql="delete from Topic where topic_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL删除语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1, topic_id);
		 	    
				
		 	    //5.执行SQL删除语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("话题信息删除异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
	}
	
	/***********3.修改(更新)话题信息   (ID,board_id,user_id,name,time,hits,replynum,lastreply_id,top,best,del,hot)  (传入链接FL 对象)*******************/	
	public void Update_Topic_data(Topic tp)
	{
		
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL更新语句
				String sql="update Topic set topic_board_id=?,topic_user_id=?,topic_name=?,topic_time=?,topic_hits=?, topic_replynum=?,topic_lastreply_id=?,topic_top=?,topic_best=?,topic_del=?,topic_hot=? where topic_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL更新语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1,tp.topic_board_id);
				st.setInt(2,tp.topic_user_id);
				st.setString(3,tp.topic_name);
				st.setDate(4,(Date) tp.topic_time);
				st.setInt(5,tp.topic_hits);
				st.setInt(6,tp.topic_replynum);
				st.setInt(7,tp.topic_lastreply_id);
				st.setInt(8, checkis(tp.topic_top));
				st.setInt(9,checkis(tp.topic_best));
				st.setInt(10, checkis(tp.topic_del));
				st.setInt(11, checkis(tp.topic_hot));
				st.setInt(12,tp.topic_id);
				
		 	    //5.执行SQL更新语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("这个话题信息修改异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
	}
	
	/***********4.查询此话题信息     返回ArrayList 类型(无参数)*******************/	
	public ArrayList<Topic> Query_AllFLink()
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.创建ArrayList集合      作用：保存对象信息
		ArrayList<Topic> list=new ArrayList<Topic>();
		
		//3.SQL查询语句
		String sql="select  topic_id=?,topic_board_id=?,topic_user_id=?,topic_name=?,topic_time=?,topic_hits=?, topic_replynum=?,topic_lastreply_id=?,topic_top=?,topic_best=?,topic_del=?,topic_hot=? ,topic_info=? from Topic";
		
		//4.加载驱动 建立连接
		con=getConnection();
		
		try {
		//5.输入SQL查询语句
		st=con.prepareStatement(sql);
		//6.执行查询语句          并存入结果集ResultSet
		rs=st.executeQuery();
		
		//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
		while(rs.next())
		{
			Topic tp=new Topic();
			tp.topic_id=rs.getInt(1);
			tp.topic_board_id=rs.getInt(2);
			tp.topic_user_id=rs.getInt(3);	
			tp.topic_name=rs.getString(4);
			tp.topic_time=rs.getTimestamp(5);
			tp.topic_hits=rs.getInt(6);
			tp.topic_replynum=rs.getInt(7);
			tp.topic_lastreply_id=rs.getInt(8);
			tp.topic_top=checkis(rs.getInt(9));
			tp.topic_best=checkis(rs.getInt(10));
			tp.topic_del=checkis(rs.getInt(11));
			tp.topic_hot=checkis(rs.getInt(12));
			tp.topic_info=rs.getString(13);

			list.add(tp);  //将对象Topic 循环 加入ArrayList 集合中
		}
		
		}catch(Exception e)
		{
			System.out.println("此话题查询异常！"+e.getMessage());
		}
		
		//8.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
		
		//9.将含有查询结果信息对象 的 ArrayList 集合  返回
		return list;
	}
	

	/*方法:查询该板块的话题总数
	 * 通过 板块id进行查询
	 * 
	 */
		public int Sum_Topic(int board_id)
		{

			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
	                //2.SQL查询语句
					String sql1="select count(*)as 'num'  from Topic  where topic_board_id=?";
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					
					try {
						//4.查询数据库	
						st=con.prepareStatement(sql1);
						st.setInt(1, board_id);
						rs=st.executeQuery();
						
						//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
						if(rs.next())
						{
							return rs.getInt("num");
						
						}
					
						}catch(Exception e)
						{
							System.out.println("查询话题总数异常！"+e.getMessage());
						}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
			
			return 0;
		}

		/*方法:查询该板块的话题集合
		 * 通过 板块id进行查询
		 * 
		 */
		public ArrayList<Topic> All_Topic(int board_id)
		{

			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
			//1.2创建ArrayList集合      作用：保存对象信息
					ArrayList<Topic> list=new ArrayList<Topic>();		
					
	       //2.SQL查询语句
					String sql1="select *  from Topic  where topic_board_id=?";
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					
					try {
						//4.查询数据库	
						st=con.prepareStatement(sql1);
						st.setInt(1, board_id);
						rs=st.executeQuery();
						//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
						while(rs.next())
						{
							Topic tp=new Topic();
							tp.topic_id=rs.getInt(1);
							tp.topic_board_id=rs.getInt(2);
							tp.topic_user_id=rs.getInt(3);	
							tp.topic_name=rs.getString(4);
							tp.topic_time=rs.getTimestamp(5);
							tp.topic_hits=rs.getInt(6);
							tp.topic_replynum=rs.getInt(7);
							tp.topic_lastreply_id=rs.getInt(8);
							tp.topic_top=checkis(rs.getInt(9));
							tp.topic_best=checkis(rs.getInt(10));
							tp.topic_del=checkis(rs.getInt(11));
							tp.topic_hot=checkis(rs.getInt(12));
							tp.topic_info=rs.getString(13);

							list.add(tp);  //将对象Topic 循环 加入ArrayList 集合中
						}
						
					
						}catch(Exception e)
						{
							System.out.println("查询话题总数异常！"+e.getMessage());
						}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
			
			return list;
		}
		
		/*方法:用ID号查询该话题
		 * 通过 话题id进行查询
		 * 
		 */
		public Topic some_Topic(int topic_id)
		{

			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
			//1.2创建ArrayList集合      作用：保存对象信息
					ArrayList<Topic> list=new ArrayList<Topic>();		
					
	       //2.SQL查询语句
					String sql1="select *  from Topic  where topic_id=?";
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					
					try {
						//4.查询数据库	
						st=con.prepareStatement(sql1);
						st.setInt(1, topic_id);
						rs=st.executeQuery();
						//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
						while(rs.next())
						{
							Topic tp=new Topic();
							tp.topic_id=rs.getInt(1);
							tp.topic_board_id=rs.getInt(2);
							tp.topic_user_id=rs.getInt(3);	
							tp.topic_name=rs.getString(4);
							tp.topic_time=rs.getTimestamp(5);
							tp.topic_hits=rs.getInt(6);
							tp.topic_replynum=rs.getInt(7);
							tp.topic_lastreply_id=rs.getInt(8);
							tp.topic_top=checkis(rs.getInt(9));
							tp.topic_best=checkis(rs.getInt(10));
							tp.topic_del=checkis(rs.getInt(11));
							tp.topic_hot=checkis(rs.getInt(12));
							tp.topic_info=rs.getString(13);

							list.add(tp);  //将对象Topic 循环 加入ArrayList 集合中
						}
						
					
						}catch(Exception e)
						{
							System.out.println("查询话题总数异常！"+e.getMessage());
						}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
			
			return (Topic)list.get(0);
		}
		
		/***********4.4查询最大的ID号*******************/	
	public int QueryID()
	{

		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
                //2.SQL查询语句
				String sql1="select * from Topic order by topic_id desc ";
				
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				
				try {
					//4.查询数据库	
					st=con.prepareStatement(sql1);

					rs=st.executeQuery();
					
					//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
					if(rs.next())
					{
						return rs.getInt("topic_id");
					
					}
				
					}catch(Exception e)
					{
						System.out.println("查询编号异常！"+e.getMessage());
					}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
				
		return 0;
	}
		
}
