package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import FunCtion.Advertisement;

public class DBOAd extends DBOPer {

	/***********1.添加广告信息     (传入广告 ad 对象)*******************/	
	public void Insert_Ad_data(Advertisement ad)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.SQL插入语句
		String sql="INSERT into Advertisement(ad_id,ad_url,ad_image,ad_title) "
				  + "values(?,?,?,?)";
		
		//3.加载驱动    建立连接
		con=getConnection();
		
		try {
		//4.输入SQL插入语句	
		st=con.prepareStatement(sql);
		
		
		st.setInt(1,ad.ad_id);
		st.setString(2,ad.ad_url);
		st.setString(3,ad.ad_image);
		st.setString(4,ad.ad_title);
		
		
 	    //5.执行SQL插入语句
		st.executeUpdate();
		
		}catch(Exception e)
		{
			System.out.println("新广告信息添加异常！"+e.getMessage());
		}
		
		//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
	}
	
	/***********2.删除广告信息     (传入广告 ad 对象的 编号ID :ad_id)*******************/	
	public void Delete_ad_data(int ad_id)   
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL删除语句
				String sql="delete from Advertisement where ad_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL删除语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1, ad_id);
		 	    
				
		 	    //5.执行SQL删除语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("广告信息删除异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
	}
	
	/***********3.修改(更新)广告信息   (ID,url,image,title)  (传入广告ad 对象)*******************/	
	public void Update_ad_data(Advertisement ad)
	{
		
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL更新语句
				String sql="update Advertisement set ad_id=?,ad_url=?,ad_image=?,ad_title=?where ad_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL更新语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1,ad.ad_id );
				st.setString(2,ad.ad_url);
				st.setString(3,ad.ad_image);
				st.setString(4, ad.ad_title);
				st.setInt(5,ad.ad_id );
				
		 	    //5.执行SQL更新语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("这个广告信息修改异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
				
		
	}
	
	/***********4.查询此广告信息     返回ArrayList 类型(无参数)*******************/	
	public ArrayList<Advertisement> Query_AllAdvertisement()
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.创建ArrayList集合      作用：保存对象信息
		ArrayList<Advertisement> list=new ArrayList<Advertisement>();
		
		//3.SQL查询语句
		String sql="select  ad_id, ad_url,ad_image,ad_title from Advertisement";
		
		//4.加载驱动 建立连接
		con=getConnection();
		
		try {
		//5.输入SQL查询语句
		st=con.prepareStatement(sql);
		//6.执行查询语句          并存入结果集ResultSet
		rs=st.executeQuery();
		
		//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
		while(rs.next())
		{
			Advertisement ad=new Advertisement();
			ad.ad_id=rs.getInt(1);
			ad.ad_url=rs.getString(2);
			ad.ad_image=rs.getString(3);	
			ad.ad_title=rs.getString(4);	
			
			list.add(ad);  //将对象Advertisement 循环 加入ArrayList 集合中
		}
		
		}catch(Exception e)
		{
			System.out.println("此广告信息查询异常！"+e.getMessage());
		}
		
		//8.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
		
		//9.将含有查询结果信息对象 的 ArrayList 集合  返回
		return list;
	}
	

	
	
	
	
	
}
