package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;

import FunCtion.Post;
public class DBOPost extends DBOPer {

	

	/***********1.添加帖子信息     (传入帖子 ost 对象)*******************/	
	public void Insert_Post_data(Post ost)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.SQL插入语句
		String sql="INSERT into Post(post_id,post_board_id,post_user_id,post_topic_id,post_reply_id,post_content,post_time,post_ip) "
				  + "values(?,?,?,?,?, ?,?,?,?)";
		
		//3.加载驱动    建立连接
		con=getConnection();
		
		try {
		//4.输入SQL插入语句	
		st=con.prepareStatement(sql);
		
		int i=0;
		st.setInt(i++,ost.post_id);
		st.setInt(i++,ost.post_board_id);
		st.setInt(i++,ost.post_user_id);
		st.setInt(i++,ost.post_topic_id);
		st.setInt(i++,ost.post_reply_id);
		st.setString(i++,ost.post_content);

		st.setTimestamp(i++,new java.sql.Timestamp(System.currentTimeMillis()));
		st.setString(i++,"no function.");
		
 	    //5.执行SQL插入语句
		st.executeUpdate();
		
		}catch(Exception e)
		{
			System.out.println("新帖子信息添加异常！"+e.getMessage());
		}
		
		//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
	}
	
	/***********1.1新建帖子信息     对初始化(传入帖子 ost 对象)*******************/	
	public void new_Post_data(Post ost)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.SQL插入语句
		String sql="INSERT into Post(post_id,post_board_id,post_user_id,post_topic_id,post_reply_id,post_content,post_time,post_editime,post_ip) "
				  + "values(?,?,?,?,?, ?,?,?,?)";
		
		//3.加载驱动    建立连接
		con=getConnection();
		
		try {
		//4.输入SQL插入语句	
		st=con.prepareStatement(sql);
		
		int i=0;
		st.setInt(i++,ost.post_id);
		st.setInt(i++,ost.post_board_id);
		st.setInt(i++,ost.post_user_id);
		st.setInt(i++,ost.post_topic_id);
		st.setInt(i++,ost.post_reply_id);
		st.setString(i++,ost.post_content);
		st.setTimestamp(i++,new java.sql.Timestamp(System.currentTimeMillis()));
		st.setString(i++,null);
		st.setString(i++,ost.post_ip);
		
 	    //5.执行SQL插入语句
		st.executeUpdate();
		
		}catch(Exception e)
		{
			System.out.println("新建帖子信息添加异常！"+e.getMessage());
		}
		
		//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
	}
	
	/***********2.删除帖子信息     (传入帖子ost 对象的 编号ID :post_id)*******************/	
	public void Delete_Post_data(int post_id)   
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL删除语句
				String sql="delete from Post where post_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL删除语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1, post_id);
		 	    
				
		 	    //5.执行SQL删除语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("帖子信息删除异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
	}
	
	/***********3.修改(更新)帖子信息   ()  (传入帖子ost 对象)*******************/	
	public void Update_Post_data(Post ost)
	{
		
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL更新语句
				String sql="update Post set post_id=?,post_board_id=?,post_user_id=?,post_topic_id=?,post_reply_id=?,post_content=?,post_time=?,post_editime=?,post_ip=?where post_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL更新语句	
				st=con.prepareStatement(sql);
				
				int i=0;
				st.setInt(i++,ost.post_id);
				st.setInt(i++,ost.post_board_id);
				st.setInt(i++,ost.post_user_id);
				st.setInt(i++,ost.post_topic_id);
				st.setInt(i++,ost.post_reply_id);
				st.setString(i++,ost.post_content);
				st.setTimestamp(i++,new Timestamp(ost.post_time.getTime()));  //util的Date转化成sqlDate
				st.setTimestamp(i++,new java.sql.Timestamp(System.currentTimeMillis()));
				st.setString(i++,ost.post_ip);
				st.setInt(i++,ost.post_id);
				
		 	    //5.执行SQL更新语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("这个帖子信息修改异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
	}
	
	
	/***********4.查询此帖子信息     返回ArrayList 类型(无参数)*******************/	
	public ArrayList<Post> Query_AllPost()
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.创建ArrayList集合      作用：保存对象信息
		ArrayList<Post> list=new ArrayList<Post>();
		
		//3.SQL查询语句
		String sql="select * from Post";
		
		//4.加载驱动 建立连接
		con=getConnection();
		
		try {
		//5.输入SQL查询语句
		st=con.prepareStatement(sql);
		//6.执行查询语句          并存入结果集ResultSet
		rs=st.executeQuery();
		
		//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
		while(rs.next())
		{
			Post ost=new Post();

			ost.post_id=rs.getInt(1);
			ost.post_board_id=rs.getInt(2);
			ost.post_user_id=rs.getInt(3);
			ost.post_topic_id=rs.getInt(4);
			ost.post_reply_id=rs.getInt(5);
			ost.post_content=rs.getString(6);
			ost.post_time=rs.getTimestamp(7);	
			list.add(ost);  //将对象FLink 循环 加入ArrayList 集合中
		}
		
		}catch(Exception e)
		{
			System.out.println("此帖子查询异常！"+e.getMessage());
		}
		
		//8.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
		
		//9.将含有查询结果信息对象 的 ArrayList 集合  返回
		return list;
	}
	
	
	/*方法:查询该板块的帖子总数
	 * 通过 板块id进行查询
	 * 
	 */
		public int Sum_Post(int board_id)
		{

			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
	                //2.SQL查询语句
					String sql1="select count(*)as 'num'  from Post  where post_board_id=?";
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					
					try {
						//4.查询数据库	
						st=con.prepareStatement(sql1);
						st.setInt(1, board_id);
						rs=st.executeQuery();
						
						//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
						if(rs.next())
						{
							return rs.getInt("num");
						
						}
					
						}catch(Exception e)
						{
							System.out.println("查询该子板块的帖子总数异常！"+e.getMessage());
						}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
			
			return 0;
		}
	
		/*方法:查询该主题的帖子总数
		 * 通过 主题id进行查询
		 * 
		 */
			public int Sum_tPost(int Topic_id)
			{

				//1.创建       Connection    PreparedStatement    ResultSet 
						Connection con=null;
						PreparedStatement st=null;
						ResultSet rs=null;
						
		                //2.SQL查询语句
						String sql1="select count(*)as 'num'  from Post  where post_topic_id=?";
						
						//3.加载驱动    建立连接
						con=getConnection();
						
						
						try {
							//4.查询数据库	
							st=con.prepareStatement(sql1);
							st.setInt(1, Topic_id);
							rs=st.executeQuery();
							
							//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
							if(rs.next())
							{
								return rs.getInt("num");
							
							}
						
							}catch(Exception e)
							{
								System.out.println("查询该话题的帖子总数异常！"+e.getMessage());
							}
						
						//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
						closeAll(con, st, rs);
				
				return 0;
			}
			
			/*方法:查询该主题的帖子
			 * 通过 主题id进行查询
			 * 
			 */
			public ArrayList<Post> All_tPost(int topic_id)
			{

				//1.创建       Connection    PreparedStatement    ResultSet 
						Connection con=null;
						PreparedStatement st=null;
						ResultSet rs=null;
						
		                //2.SQL查询语句
						String sql1="select *  from Post  where post_topic_id=?";
						
						//2.创建ArrayList集合      作用：保存对象信息
						ArrayList<Post> list=new ArrayList<Post>();
						
						//3.加载驱动    建立连接
						con=getConnection();
						
						
						try {
							//4.查询数据库	
							st=con.prepareStatement(sql1);
							st.setInt(1, topic_id);
							rs=st.executeQuery();
							
							//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
							while(rs.next())
							{
								Post ost=new Post();
								int i=0;
								ost.post_id=rs.getInt(i++);
								ost.post_board_id=rs.getInt(i++);
								ost.post_user_id=rs.getInt(i++);
								ost.post_topic_id=rs.getInt(i++);
								ost.post_reply_id=rs.getInt(i++);
								ost.post_content=rs.getString(i++);
								ost.post_time=rs.getTimestamp(i++);	
								list.add(ost);  //将对象FLink 循环 加入ArrayList 集合中
							}
							
						
							}catch(Exception e)
							{
								System.out.println("查询该话题的帖子异常！11"+e.getMessage());
							}
						
						//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
						closeAll(con, st, rs);
				
				return list;
			}
			
			/*方法:查询某ID的帖子
			 * 通过 帖子id进行查询
			 * 
			 */
			public Post some_Post(int post_id)
			{

				//1.创建       Connection    PreparedStatement    ResultSet 
						Connection con=null;
						PreparedStatement st=null;
						ResultSet rs=null;
						
		                //2.SQL查询语句
						String sql1="select *  from Post  where post_id=?";
						
						//2.创建ArrayList集合      作用：保存对象信息
						ArrayList<Post> list=new ArrayList<Post>();
						
						//3.加载驱动    建立连接
						con=getConnection();
						
						
						try {
							//4.查询数据库	
							st=con.prepareStatement(sql1);
							st.setInt(1, post_id);
							rs=st.executeQuery();
							
							//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
							while(rs.next())
							{
								Post ost=new Post();
								int i=0;
								ost.post_id=rs.getInt(i++);
								ost.post_board_id=rs.getInt(i++);
								ost.post_user_id=rs.getInt(i++);
								ost.post_topic_id=rs.getInt(i++);
								ost.post_reply_id=rs.getInt(i++);
								ost.post_content=rs.getString(i++);
								ost.post_time=rs.getTimestamp(i++);	
								ost.post_editime=rs.getTime(i++);
								ost.post_ip=rs.getString(i++);
								
								list.add(ost);  //将对象FLink 循环 加入ArrayList 集合中
							}
							
						
							}catch(Exception e)
							{
								System.out.println("查询该话题的帖子异常！"+e.getMessage());
							}
						
						//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
						closeAll(con, st, rs);
				
				return (Post)list.get(0);
			}
			
			
}
