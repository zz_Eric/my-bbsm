package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import HumanM.Admin;


public class DBOAdmin extends DBOPer {

	/***********1.添加管理员信息     (传入管理员 admin 对象)*******************/	
	public void Insert_admin_data(Admin admin)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.SQL插入语句
		String sql="INSERT into Admin(admin_id,admin_name,admin_password,admin_user_id) "
				  + "values(?,?,?,?)";
		
		//3.加载驱动    建立连接
		con=getConnection();
		
		try {
		//4.输入SQL插入语句	
		st=con.prepareStatement(sql);
		
		
		st.setInt(1,admin.admin_id);
		st.setString(2,admin.admin_name);
		st.setString(3,admin.admin_password);
		st.setInt(4, admin.admin_user_id);
		
 	    //5.执行SQL插入语句
		st.executeUpdate();
		
		}catch(Exception e)
		{
			System.out.println("新管理员信息添加异常！"+e.getMessage());
		}
		
		//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
	}
	
	/***********2.删除管理员信息     (传入管理员 admin 对象的 编号ID :admin_id)*******************/	
	public void Delete_admin_data(int admin_id)   
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL删除语句
				String sql="delete from Admin where admin_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL删除语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1, admin_id);
		 	    
				
		 	    //5.执行SQL删除语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("管理员信息删除异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
	}
	
	/***********3.修改(更新)管理员信息   修改密码 (管理员,密码,用户名称)  (传入管理员admin 对象)*******************/	
	public void Update_admin_data(Admin admin,String admin_password)
	{
		
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL更新语句
				String sql="update Admin set admin_id=?,admin_name=?,admin_password=?,admin_user_id=? where admin_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL更新语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1,admin.admin_id );
				st.setString(2,admin.admin_name);
				st.setString(3,admin_password);
				st.setInt(4, admin.admin_user_id);
				st.setInt(5,admin.admin_id );
				
		 	    //5.执行SQL更新语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("这个管理员的密码修改异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
				
		
	}
	
	/***********4.查询此管理员信息     返回ArrayList 类型(无参数)*******************/	
	public ArrayList<Admin> Query_AllAdmindata()
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.创建ArrayList集合      作用：保存对象信息
		ArrayList<Admin> list=new ArrayList<Admin>();
		
		//3.SQL查询语句
		String sql="select  admin_id, admin_name,admin_password,admin_user_id from Admin";
		
		//4.加载驱动 建立连接
		con=getConnection();
		
		try {
		//5.输入SQL查询语句
		st=con.prepareStatement(sql);
		//6.执行查询语句          并存入结果集ResultSet
		rs=st.executeQuery();
		
		//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
		while(rs.next())
		{
			Admin admin=new Admin();
			admin.admin_id=rs.getInt(1);
			admin.admin_name=rs.getString(2);
			admin.admin_password=rs.getString(3);	
			admin.admin_user_id=rs.getInt(4);	
			
			list.add(admin);  //将对象Admin 循环 加入ArrayList 集合中
		}
		
		}catch(Exception e)
		{
			System.out.println("此管理员查询异常！"+e.getMessage());
		}
		
		//8.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
		System.out.println("管理员查询成功！");
		//9.将含有查询结果信息对象 的 ArrayList 集合  返回
		return list;
	}
	
	/***********4.1查询此管理员信息   根据管理员名     返回Admin 类型(无参数)*******************/	
	public Admin Query_Admindata(String name)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.创建ArrayList集合      作用：保存对象信息
		ArrayList<Admin> list=new ArrayList<Admin>();
		
		//3.SQL查询语句
		String sql="select  admin_id, admin_name,admin_password,admin_user_id from Admin where  admin_name=?";
		
		//4.加载驱动 建立连接
		con=getConnection();
		
		try {
		//5.输入SQL查询语句
		st=con.prepareStatement(sql);
		
		st.setString(1, name);//给查询条件赋值
		//6.执行查询语句          并存入结果集ResultSet
		rs=st.executeQuery();
		
		
		
		//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
		while(rs.next())
		{
			Admin admin=new Admin();
			admin.admin_id=rs.getInt(1);
			admin.admin_name=rs.getString(2);
			admin.admin_password=rs.getString(3);	
			admin.admin_user_id=rs.getInt(4);	
			
			list.add(admin);  //将对象Admin 循环 加入ArrayList 集合中
		}
		
		}catch(Exception e)
		{
			System.out.println("此管理员查询异常！"+e.getMessage());
		}
		
		//8.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
		System.out.println("管理员查询成功！");
		//9.将含有查询结果信息对象 的 ArrayList 集合  返回
		return (Admin)list.get(0);
	}
	
	/***********4.2查询此管理员是否存在     返回boolean类型(无参数)*******************/	
	public boolean Query_Admin_is(String name)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.创建ArrayList集合      作用：保存对象信息
		//ArrayList<Admin> list=new ArrayList<Admin>();
		
		//3.SQL查询语句
		String sql="select  admin_id, admin_name,admin_password,admin_user_id from Admin where  admin_name=?";
		
		//4.加载驱动 建立连接
		con=getConnection();
		
		try {
		//5.输入SQL查询语句
		st=con.prepareStatement(sql);
		
		st.setString(1, name);//给查询条件赋值
		//6.执行查询语句          并存入结果集ResultSet
		rs=st.executeQuery();
		
		
		
		//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
		if(rs.next())
		{
			System.out.println("该管理员存在！");
			return true;
		}
		
		}catch(Exception e)
		{
			System.out.println("此管理员查询异常！"+e.getMessage());
		}
		
		//8.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
		return false;
	}
}
