package jdbc;
import java.sql.*;
public class DBOPer {
	  public String url="jdbc:sqlserver://localhost:1433;DatabaseName=BBSM";
	  public String UserName="sa";
	  public String PassWord="sa";	  
	  /*加载驱动Class.forName("")   建立连接  Driver.getConnection(url,UserName,PassWord);*/ 
	  public Connection getConnection()
	  {
		  Connection con=null;
		  try {
		  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		  con=DriverManager.getConnection(url,UserName,PassWord);
		  }catch(Exception e) {
			  
			  System.out.println("建立连接失败!"+e.getMessage());
		  }
		  return con;
	  }
	  //关闭所有链接   Connection    PreparedStatement    ResultSet 
	  public void closeAll(Connection con,PreparedStatement st,ResultSet rs)
	  {
		  
		  try {
			  if(con!=null)con.close();
			  if(st!=null)st.close();
			  if(rs!=null)rs.close();
		  }catch(Exception e)
		  {
			  System.out.println("数据关闭异常！");
		  }
	  }
	  //判断状态
	  public int checkis(boolean bool)
		{
			if(bool)
				return 1;
			else
				return 0;
		}
		
	  public boolean checkis(int str)
		{	
			if(str == 1) 
				return true;
			else if(str == 0)
				return false;
		return false;
		}
	  
}
