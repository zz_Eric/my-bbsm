package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import FunCtion.Board;;
public class DBOBoard extends DBOPer {
	

	/***********1.添加板块信息     (传入板块 bo 对象)*******************/	
	public void Insert_Board_data(Board bo)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.SQL插入语句
		String sql="INSERT into Board(board_id,board_idMother,board_bid,board_name ,board_info ,board_master ,board_img,board_postnum ,board_topicnum ,board_todaynum ,board_lastreply ) "
				  + "values(?,?,?,?,?, ?,?,?,?,?, ?)";
		
		//3.加载驱动    建立连接
		con=getConnection();
		
		try {
		//4.输入SQL插入语句	
		st=con.prepareStatement(sql);
		
		
		st.setInt(1,bo.board_id);
		st.setInt(2,checkis(bo.board_idMother));
		st.setInt(3,bo.board_bid);
		st.setString(4,bo.board_name);
		st.setString(5,bo.board_info);
		st.setString(6,bo.board_master);
		st.setString(7,bo.board_img);
		st.setInt(8,bo.board_postnum);
		st.setInt(9,bo.board_topicnum);
		st.setInt(10,bo.board_todaynum);
		st.setInt(11,bo.board_lastreply);
		
 	    //5.执行SQL插入语句
		st.executeUpdate();
		System.out.println("板块信息添加成功！");
		}catch(Exception e)
		{
			System.out.println("新板块信息添加异常！"+e.getMessage());
		}
		
		//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
	}
	
	/***********2.删除板块信息     (传入板块 bo 对象的 编号ID :board_id)*******************/	
	public void Delete_Board_data(int board_id)   
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL删除语句
				String sql="delete from Board where board_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL删除语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1, board_id);
		 	    
				
		 	    //5.执行SQL删除语句
				st.executeUpdate();
				System.out.println("板块信息删除成功！");
				}catch(Exception e)
				{
					System.out.println("板块信息删除异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
	}
	
	/***********3.修改(更新)板块信息   ()  (传入链接FL 对象)*******************/	
	public void Update_Board_data(Board bo)
	{
		
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL更新语句
				String sql="update Board set board_id=?,board_idMother=?,board_bid=?,board_name=? ,board_info=? ,board_master=? ,board_img=?,board_postnum=? ,board_topicnum=? ,board_todaynum=? ,board_lastreply=? where board_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL更新语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1,bo.board_id);
				st.setInt(2,checkis(bo.board_idMother));
				st.setInt(3,bo.board_bid);
				st.setString(4,bo.board_name);
				st.setString(5,bo.board_info);
				st.setString(6,bo.board_master);
				st.setString(7,bo.board_img);
				st.setInt(8,bo.board_postnum);
				st.setInt(9,bo.board_topicnum);
				st.setInt(10,bo.board_todaynum);
				st.setInt(11,bo.board_lastreply);
				st.setInt(12,bo.board_id);
				
		 	    //5.执行SQL更新语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("这个板块信息修改异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
				
		
	}
	
	/***********4.查询所有板块信息     返回ArrayList 类型(无参数)*******************/	
	public ArrayList<Board> Query_AllBoard()
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.创建ArrayList集合      作用：保存对象信息
		ArrayList<Board> list=new ArrayList<Board>();
		
		//3.SQL查询语句
		String sql="select  *  from Board";
		
		//4.加载驱动 建立连接
		con=getConnection();
		
		try {
		//5.输入SQL查询语句
		st=con.prepareStatement(sql);
		//6.执行查询语句          并存入结果集ResultSet
		rs=st.executeQuery();
		
		//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
		while(rs.next())
		{
			Board bo=new Board();
			bo.board_id=rs.getInt(1);
			bo.board_idMother=checkis(rs.getInt(2));
			bo.board_bid=rs.getInt(3);
			bo.board_name=rs.getString(4);	
			bo.board_info=rs.getString(5);
			bo.board_master=rs.getString(6);
			bo.board_img=rs.getString(7);
			bo.board_postnum=rs.getInt(8);
			bo.board_topicnum=rs.getInt(9);
			bo.board_todaynum=rs.getInt(10);
			bo.board_lastreply=rs.getInt(11);
			
			list.add(bo);  //将对象FLink 循环 加入ArrayList 集合中
		}
		
		}catch(Exception e)
		{
			System.out.println("此板块查询异常！"+e.getMessage());
		}
		
		//8.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
		
		//9.将含有查询结果信息对象 的 ArrayList 集合  返回
		return list;
	}
	
	/***4.1查询最大ID号***/
	public int QueryID()
	{

		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
                //2.SQL查询语句
				String sql1="select * from Board order by board_id desc ";
				
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				
				try {
					//4.查询数据库	
					st=con.prepareStatement(sql1);
					
					rs=st.executeQuery();
					
					//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
					if(rs.next())
					{
						return rs.getInt("board_id");
					
					}
				
					}catch(Exception e)
					{
						System.out.println("查询编号异常！"+e.getMessage());
					}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
				
		return 0;
	}
	
	/*方法:查询该板块的所有信息
	 * 通过 板块id进行查询
	 * 
	 */
	public Board Query_isBoard(int board_id)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.创建ArrayList集合      作用：保存对象信息
		//ArrayList<Board> list=new ArrayList<Board>();
		
		//3.SQL查询语句
		String sql="select  *  from Board where board_id=?";
		
		//4.加载驱动 建立连接
		con=getConnection();
		
		Board bo=new Board();
		try {
		//5.输入SQL查询语句
		st=con.prepareStatement(sql);
		st.setInt(1, board_id);
		//6.执行查询语句          并存入结果集ResultSet
		rs=st.executeQuery();
		
		//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
		
		if(rs.next())
		{
			
			bo.board_id=rs.getInt(1);
			bo.board_idMother=checkis(rs.getInt(2));
			bo.board_bid=rs.getInt(3);
			bo.board_name=rs.getString(4);	
			bo.board_info=rs.getString(5);
			bo.board_master=rs.getString(6);
			bo.board_img=rs.getString(7);
			bo.board_postnum=rs.getInt(8);
			bo.board_topicnum=rs.getInt(9);
			bo.board_todaynum=rs.getInt(10);
			bo.board_lastreply=rs.getInt(11);
			
			//list.add(bo);  //将对象FLink 循环 加入ArrayList 集合中
		}
		
		}catch(Exception e)
		{
			System.out.println("此板块查询异常！"+e.getMessage());
		}
		
		//8.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
		
		//9.将含有查询结果信息对象 的 ArrayList 集合  返回
		return bo;
	}

	/*方法:查询主板块的子版块总数
	 * 通过 主板块id进行查询
	 * 
	 */
		public int Sum_sonBoard(int mother_board_id)
		{

			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
	                //2.SQL查询语句
					String sql1="select count(*)as 'num'  from Board  where board_bid=?";
					
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					
					try {
						//4.查询数据库	
						st=con.prepareStatement(sql1);
						st.setInt(1, mother_board_id);
						rs=st.executeQuery();
						
						//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
						if(rs.next())
						{
							return rs.getInt("num");
						
						}
					
						}catch(Exception e)
						{
							System.out.println("查询子版块总数异常！"+e.getMessage());
						}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
			
			return 0;
		}
	
		/*方法:通过板块名称查询板块ID
		 * 通过 板块名进行查询
		 * 
		 */public int Q_BoardID(String Board_name)
			{

				//1.创建       Connection    PreparedStatement    ResultSet 
						Connection con=null;
						PreparedStatement st=null;
						ResultSet rs=null;
						
		                //2.SQL查询语句
						String sql1="select *  from Board  where board_name=?";
						
						
						//3.加载驱动    建立连接
						con=getConnection();
						
						
						try {
							//4.查询数据库	
							st=con.prepareStatement(sql1);
							st.setString(1, Board_name);
							rs=st.executeQuery();
							
							//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
							if(rs.next())
							{
								return rs.getInt("board_id");
							
							}
						
							}catch(Exception e)
							{
								System.out.println("查询子版块总数异常！"+e.getMessage());
							}
						
						//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
						closeAll(con, st, rs);
				
				return 0;
			}
}
