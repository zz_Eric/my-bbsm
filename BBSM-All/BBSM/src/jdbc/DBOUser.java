package jdbc;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import HumanM.PUser;

public class DBOUser extends DBOPer{

	
	
	
		/***********1.添加用户信息     (传入用户user 对象)*******************/	
		public boolean Insert_user_data(PUser user)
		{
			//1.创建       Connection    PreparedStatement    ResultSet 
			Connection con=null;
			PreparedStatement st=null;
			ResultSet rs=null;
			
			//2.SQL插入语句
			String sql="INSERT into PUser(puser_id,puser_name,puser_password, puser_sex,puser_birthday," + 
					"puser_QQ,puser_Email,puser_tel,puser_face,puser_sign," + 
					"puser_grade,puser_mark, puser_topic, puser_wealth, puser_post," + 
					"puser_group,puser_lastip, puser_delnum,puser_friends,puser_regtime," + 
					"puser_locked, puser_admin, puser_password_a," + 
					"puser_password_q, puser_age, puser_secondname,puser_truename,puser_blood," + 
					"puser_zodiac, puser_nation, puser_province,puser_city) "
					  + "values(?,?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?,?, ?,?)";
			
			//3.加载驱动    建立连接
			con=getConnection();
			
			try {
			//4.输入SQL插入语句	
			st=con.prepareStatement(sql);
			
			//SimpleDateFormat HMFromat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				
			st.setInt(1,user.puser_id);
			st.setString(2,user.puser_name);
			st.setString(3,user.puser_password);
			st.setString(4, user.puser_sex);
			st.setTimestamp(5,new Timestamp(user.puser_birthday.getTime()));//util.Date转sql.Date
	      
			st.setString(6, user.puser_QQ);
			st.setString(7, user.puser_Email);
			st.setString(8, user.puser_tel);
			st.setString(9, user.puser_face);
			st.setString(10, user.puser_sign);
			
			st.setInt(11, user.puser_grade);
			st.setInt(12, user.puser_mark);
			st.setInt(13, user.puser_topic);
			st.setInt(14, user.puser_wealth);
			st.setInt(15, user.puser_post);
			
			st.setString(16, user.puser_group);
			st.setString(17, user.puser_lastip);
			st.setInt(18, user.puser_delnum);
			st.setString(19, user.puser_friends);
			st.setTimestamp(20,new java.sql.Timestamp(System.currentTimeMillis()));//puser_regtime
			
			
			//st.setString(21,HMFromat.format(new Date()));//puser_lasttime
			st.setInt(21, checkis(user.puser_locked));
			st.setInt(22, checkis(user.puser_admin));
			st.setString(23, user.puser_password_a);
			st.setString(24, user.puser_password_q);
			st.setInt(25, user.puser_age);
			
			st.setString(26, user.puser_secondname);
			st.setString(27, user.puser_truename);
			st.setString(28, user.puser_blood);
			st.setString(29, user.puser_zodiac);
			
			st.setString(30, user.puser_nation);
			st.setString(31, user.puser_province);
			st.setString(32, user.puser_city);
			
	 	    //5.执行SQL插入语句
			st.executeUpdate();
			
			}catch(Exception e)
			{
				System.out.println("新用户信息添加异常！"+e.getMessage());
				return false;
			}
			//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
			closeAll(con, st, rs);
		  return true;
		}

		/***********1.1注册用户信息     (传入用户user 对象)*******************/	
		public boolean SignUp_user(PUser user)
		{
			//1.创建       Connection    PreparedStatement    ResultSet 
			Connection con=null;
			PreparedStatement st=null;
			ResultSet rs=null;
			
			//2.SQL插入语句
			String sql="INSERT into PUser(puser_id,puser_name,puser_password, puser_sex,puser_Email,puser_tel,puser_regtime, puser_password_a,puser_password_q,puser_birthday) "
					  + "values(?,?,?,?,?,?,?,?,?,?)";
			
			//3.加载驱动    建立连接
			con=getConnection();
			
			try {
			//4.输入SQL插入语句	
			st=con.prepareStatement(sql);
			
			//SimpleDateFormat HMFromat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			int i=0;
			st.setInt(++i,this.QueryID()+1);
			st.setString(++i,user.puser_name);
			st.setString(++i,user.puser_password);
			st.setString(++i, user.puser_sex);
			st.setString(++i, user.puser_Email);
			st.setString(++i, user.puser_tel);
			st.setTimestamp(++i,new java.sql.Timestamp(System.currentTimeMillis()));//puser_regtime
			st.setString(++i, user.puser_password_a);
			st.setString(++i, user.puser_password_q);
			st.setTimestamp(++i,new Timestamp(user.puser_birthday.getTime()));
			
	 	    //5.执行SQL插入语句
			st.executeUpdate();
			
			}catch(Exception e)
			{
				System.out.println("新用户注册信息异常！"+e.getMessage());
				return false;
			}
			//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
			closeAll(con, st, rs);
		  return true;
		}
			
		/***********2.删除用户信息     (传入用户 User 对象的 编号ID :user_id)*******************/	
		public void Delete_user_data(int user_id)   
		{

			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					//判断该用户是否为管理员
					DBOUser user=new DBOUser();
					
					if(!user.is_user_admin(user_id))
					{
						
					//2.SQL删除语句
					String sql="delete from PUser where puser_id=?";
					
					//3.加载驱动    建立连接
					con=getConnection();	
					
						try {
							//4.输入SQL删除语句	
							st=con.prepareStatement(sql);
							
							st.setInt(1, user_id);
					 	    
							
					 	    //5.执行SQL删除语句
							st.executeUpdate();
							
							}catch(Exception e)
							{
								System.out.println("用户信息删除异常！"+e.getMessage());
							}
					
					}		
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
		}
		
		/***********3.修改(更新)用户信息   修改密码 (用户名,密码,其他)  (传入用户user 对象)*******************/	
		public void Update_user_password(PUser user,String user_password)
		{
			
			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
					//2.SQL更新语句
					String sql="update PUser set puser_password=? where puser_id=?";
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					try {
					//4.输入SQL更新语句	
					st=con.prepareStatement(sql);
					
					st.setString(1,user_password);
					st.setInt(2,user.puser_id);
					
					
			 	    //5.执行SQL更新语句
					st.executeUpdate();
					
					}catch(Exception e)
					{
						System.out.println("这个用户的密码修改异常！"+e.getMessage());
					}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
		}
		
		/***********3.0修改(更新)用户信息   修改头像地址 (传入用户user 对象)*******************/	
		public void Update_user_photo(int user_id,String user_face)
		{
			
			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
					//2.SQL更新语句
					String sql="update PUser set puser_face=? where puser_id=?";
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					try {
					//4.输入SQL更新语句	
					st=con.prepareStatement(sql);
					
					st.setString(1,user_face);
					st.setInt(2,user_id);
					
					
			 	    //5.执行SQL更新语句
					st.executeUpdate();
					
					}catch(Exception e)
					{
						System.out.println("这个用户的头像地址修改异常！"+e.getMessage());
					}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
		}
			
		/***********3.1修改(更新)用户信息    (用户名,密码,其他)  (传入用户user 对象)*******************/	
		public void Update_user_data(PUser user,String sex,Date birthday,String QQ,String Email,String tel,String face_url,String password_a,
				                     String password_q,String secondname,String truename,String blood,String zodiac,String nation,String province,
				                     String city)
		{
			
			//1.创建       Connection    PreparedStatement    ResultSet 
			Connection con=null;
			PreparedStatement st=null;
			ResultSet rs=null;
			
			//2.SQL插入语句
			String sql="update PUser set puser_sex=?,puser_birthday=?, puser_QQ=?,puser_Email=?,puser_tel=?,puser_face=?,"+
					"puser_password_a=?,puser_password_q=?,puser_secondname=?,puser_truename=?,puser_blood=?,puser_zodiac=?,"+
					"puser_nation=?, puser_province=?,puser_city=? where puser_id=?";
			
			//3.加载驱动    建立连接
			con=getConnection();
			
			try {
			//4.输入SQL插入语句	
			st=con.prepareStatement(sql);
			
			st.setString(1, user.puser_sex);
			st.setTimestamp(2,new Timestamp(birthday.getTime()));
	      
			st.setString(3,QQ);
			st.setString(4, Email);
			st.setString(5, tel);
			st.setString(6, face_url);
		
			st.setString(7, password_a);
			st.setString(8, password_q);
			
			st.setString(9, secondname);
			st.setString(10, truename);
			st.setString(11, blood);
			st.setString(12, zodiac);
			
			st.setString(13, nation);
			st.setString(14, province);
			st.setString(15, city);
			st.setInt(16,user.puser_id);
			
			
	 	    //5.执行SQL语句
			st.executeUpdate();
			
			}catch(Exception e)
			{
				System.out.println("新用户信息添加异常！"+e.getMessage());
			}			
			//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
			closeAll(con, st, rs);
		}
		
		/***********3.2修改(更新)用户状态  (禁用/启用)  (传入用户user 对象)*******************/	
		public void Update_isLocked(int id,Boolean is)
		{
			
			//1.创建       Connection    PreparedStatement    ResultSet 
			Connection con=null;
			PreparedStatement st=null;
			ResultSet rs=null;
			
			//2.SQL插入语句
			String sql="update PUser set puser_locked=? where puser_id=?";
			
			//3.加载驱动    建立连接
			con=getConnection();
			
			try {
			//4.输入SQL插入语句	
			st=con.prepareStatement(sql);
			
			if(is==true)
			{st.setInt(1, 1);}
			if(is==false)
			{st.setInt(1, 0);}
			
			st.setInt(2, id);
			
	 	    //5.执行SQL语句
			st.executeUpdate();
			System.out.println("新用户状态更改成功！");
			}catch(Exception e)
			{
				System.out.println("新用户状态更改异常！"+e.getMessage());
			}			
			//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
			closeAll(con, st, rs);
		}
		
		/***********3.2修改(标注)用户注销时间  LastTime  (传入用户user 对象)*******************/	
				public void Insert_user_lasttime(PUser user)
				{
					//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
					//2.SQL插入语句
					String sql="update PUser set puser_lasttime=? where puser_id=?";
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					try {
					//4.输入SQL插入语句	
					st=con.prepareStatement(sql);
					
					SimpleDateFormat HMFromat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

					st.setString(1,HMFromat.format(new Date()));//puser_lasttime
					st.setInt(2,user.puser_id);
					
			 	    //5.执行SQL插入语句
					st.executeUpdate();
					System.out.println("用户注销时间标注成功！");
					}catch(Exception e)
					{
						System.out.println("用户注销时间标注异常！"+e.getMessage());
					}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
					
				}
				/***********3.2修改()用户密码  password  (传入用户user_id)*******************/	
				public void change_user_password(int id,String new_password)
				{
					//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
					//2.SQL插入语句
					String sql="update PUser set puser_password=? where puser_id=?";
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					try {
					//4.输入SQL插入语句	
					st=con.prepareStatement(sql);

					st.setString(1,new_password);
					st.setInt(2,id);
					
			 	    //5.执行SQL插入语句
					st.executeUpdate();
					System.out.println("用户密码修改成功！");
					}catch(Exception e)
					{
						System.out.println("用户密码修改异常！"+e.getMessage());
					}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
					
				}
					
		/***********4.查询所有用户信息     返回ArrayList 类型(无参数)*******************/	
		public ArrayList<PUser> Query_AllPuserdata()
		{
			//1.创建       Connection    PreparedStatement    ResultSet 
			Connection con=null;
			PreparedStatement st=null;
			ResultSet rs=null;
			
			//2.创建ArrayList集合      作用：保存对象信息
			ArrayList<PUser> list=new ArrayList<PUser>();
			
			//3.SQL查询语句
			String sql="select puser_id,puser_name,puser_password, puser_sex,puser_birthday,"
					+ "puser_QQ,puser_Email,puser_tel,puser_face,puser_sign,"
					+ "puser_grade,puser_mark, puser_topic, puser_wealth, puser_post,"
					+ "puser_group,puser_lastip, puser_delnum,puser_friends,puser_regtime,puser_lasttime,"
					+ "puser_locked, puser_admin, puser_password_a,puser_password_q, "
					+ "puser_age, puser_secondname,puser_truename,puser_blood,puser_zodiac, puser_nation, puser_province,puser_city from PUser";
			
			//4.加载驱动 建立连接
			con=getConnection();
			
			try {
			//5.输入SQL查询语句
			st=con.prepareStatement(sql);
			//6.执行查询语句          并存入结果集ResultSet
			rs=st.executeQuery();
			
			//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
			while(rs.next())
			{
				PUser user=new PUser();
				user.puser_id=rs.getInt(1);
				user.puser_name=rs.getString(2);
				user.puser_password=rs.getString(3);
				user.puser_sex=rs.getString(4);
				user.puser_birthday=rs.getTimestamp(5);
				user.puser_QQ=rs.getString(6);
				user.puser_Email=rs.getString(7);
				user.puser_tel=rs.getString(8);
				user.puser_face=rs.getString(9);
				user.puser_sign=rs.getString(10);
				user.puser_grade=rs.getInt(11);
				user.puser_mark=rs.getInt(12);
				user.puser_topic=rs.getInt(13);
				user.puser_wealth=rs.getInt(14);
				user.puser_post=rs.getInt(15);
				user.puser_group=rs.getString(16);
				user.puser_lastip=rs.getString(17);
				user.puser_delnum=rs.getInt(18);
				user.puser_friends=rs.getString(19);
				user.puser_regtime=rs.getTimestamp(20);
				user.puser_lasttime=rs.getTimestamp(21);
				user.puser_locked=checkis(rs.getInt(22));
				user.puser_admin=checkis(rs.getInt(23));
				user.puser_password_a=rs.getString(24);
				user.puser_password_q=rs.getString(25);
				user.puser_age=rs.getInt(26);
				user.puser_secondname=rs.getString(27);
				user.puser_truename=rs.getString(28);
				user.puser_blood=rs.getString(29);
				user.puser_zodiac=rs.getString(30);
				user.puser_nation=rs.getString(31);  
				user.puser_province=rs.getString(32); 
				user.puser_city=rs.getString(33);
				
				list.add(user);  //将对象PUser 循环 加入ArrayList 集合中
			}
			
			}catch(Exception e)
			{
				System.out.println("此用户查询异常！"+e.getMessage());
			}
			
			//8.关闭所有链接   Connection    PreparedStatement    ResultSet 
			closeAll(con, st, rs);
			
			
			//9.将含有查询结果信息对象 的 ArrayList 集合  返回
			return list;
		}
		
		/***********4.1该用户是否为管理员    (传入用户 User 对象的 编号ID :user_id)*******************/	
		public boolean is_user_admin(int user_id)   
		{
			
			
			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
	                //2.SQL查询语句
					String sql1="select  admin_id from Admin where admin_user_id=?";
					
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					
					try {
						//4.查询数据库	
						st=con.prepareStatement(sql1);
						
						st.setInt(1, user_id);
						
						rs=st.executeQuery();
						
						//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
						if(rs.next())
						{
							System.out.println("此用户为管理员！");
							return true;
						}
					
						}catch(Exception e)
						{
							System.out.println("查询用户是否为管理员异常！"+e.getMessage());
						}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
					return false;
		}
	
		/***********4.2该用户名与密码是否正确    (传入用户 User 对象的 编号ID :user_id)*******************/	
		public boolean is_user_true(PUser user)   
		{
			
			
			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
	                //2.SQL查询语句
					String sql1="select * from Puser where puser_name=? and puser_password=?";
					
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					
					try {
						//4.查询数据库	
						st=con.prepareStatement(sql1);
						
						st.setString(1, user.puser_name);
						st.setString(2, user.puser_password);
						
						rs=st.executeQuery();
						
						//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
						if(rs.next())
						{
							System.out.println("用户名密码均正确！");
							return true;
						}
					
						}catch(Exception e)
						{
							System.out.println("用户名或密码错误！"+e.getMessage());
						}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
					return false;
		}
		
		/***********4.3该用户是否存在    (传入用户 User 对象的 编号ID :user_id)*******************/	
		public boolean is_user_exist(String user_name)   
		{
			
			
			//1.创建       Connection    PreparedStatement    ResultSet 
					Connection con=null;
					PreparedStatement st=null;
					ResultSet rs=null;
					
	                //2.SQL查询语句
					String sql1="select * from Puser where puser_name=? ";
					
					
					//3.加载驱动    建立连接
					con=getConnection();
					
					
					try {
						//4.查询数据库	
						st=con.prepareStatement(sql1);
						
						st.setString(1, user_name);
						
						
						rs=st.executeQuery();
						
						//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
						if(rs.next())
						{
							System.out.println("该用户存在！");
							return true;
						}
					
						}catch(Exception e)
						{
							System.out.println("该用户不存在！"+e.getMessage());
						}
					
					//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
					closeAll(con, st, rs);
					return false;
		}
		/***********4.4查询最大的ID号*******************/	
	public int QueryID()
	{

		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
                //2.SQL查询语句
				String sql1="select * from Puser order by puser_id desc ";
				
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				
				try {
					//4.查询数据库	
					st=con.prepareStatement(sql1);

					rs=st.executeQuery();
					
					//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
					if(rs.next())
					{
						return rs.getInt("puser_id");
					
					}
				
					}catch(Exception e)
					{
						System.out.println("查询编号异常！"+e.getMessage());
					}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
				
		return 0;
	}
	
	/***********4.5查询用户信息    (传入用户 User 对象的 user_name)*******************/	
	public PUser Q_user_info(String user_name)   
	{
		
		
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
                //2.SQL查询语句
				String sql1="select * from Puser where puser_name=? ";
				//2.创建ArrayList集合      作用：保存对象信息
				ArrayList<PUser> list=new ArrayList<PUser>();
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				
				try {
					//4.查询数据库	
					st=con.prepareStatement(sql1);
					
					st.setString(1, user_name);
					
					
					rs=st.executeQuery();
					
					//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
					while(rs.next())
					{
						PUser user=new PUser();
						user.puser_id=rs.getInt(1);
						user.puser_name=rs.getString(2);
						user.puser_password=rs.getString(3);
						user.puser_sex=rs.getString(4);
						user.puser_birthday=rs.getDate(5);
						user.puser_QQ=rs.getString(6);
						user.puser_Email=rs.getString(7);
						user.puser_tel=rs.getString(8);
						user.puser_face=rs.getString(9);
						user.puser_sign=rs.getString(10);
						user.puser_grade=rs.getInt(11);
						user.puser_mark=rs.getInt(12);
						user.puser_topic=rs.getInt(13);
						user.puser_wealth=rs.getInt(14);
						user.puser_post=rs.getInt(15);
						user.puser_group=rs.getString(16);
						user.puser_lastip=rs.getString(17);
						user.puser_delnum=rs.getInt(18);
						user.puser_friends=rs.getString(19);
						user.puser_regtime=rs.getDate(20);
						user.puser_lasttime=rs.getDate(21);
						user.puser_locked=checkis(rs.getInt(22));
						user.puser_admin=checkis(rs.getInt(23));
						user.puser_password_a=rs.getString(24);
						user.puser_password_q=rs.getString(25);
						user.puser_age=rs.getInt(26);
						user.puser_secondname=rs.getString(27);
						user.puser_truename=rs.getString(28);
						user.puser_blood=rs.getString(29);
						user.puser_zodiac=rs.getString(30);
						user.puser_nation=rs.getString(31);  
						user.puser_province=rs.getString(32); 
						user.puser_city=rs.getString(33);
						
						list.add(user);  //将对象PUser 循环 加入ArrayList 集合中
					}
					}catch(Exception e)
					{
						System.out.println("该用户不存在！"+e.getMessage());
					}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
				System.out.println("查询成功！");
				return (PUser)list.get(0);
	}
	
	/***********4.5根据ID查询用户姓名    (传入用户 User 对象的 编号ID :user_id)*******************/	
	public String Q_user_info_ofid(int user_id)   
{
		
		
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
                //2.SQL查询语句
				String sql1="select puser_name from Puser where puser_id=? ";
				//2.创建ArrayList集合      作用：保存对象信息
				ArrayList<PUser> list=new ArrayList<PUser>();
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				
				try {
					//4.查询数据库	
					st=con.prepareStatement(sql1);
					
					st.setInt(1, user_id);
					
					
					rs=st.executeQuery();
					
					//5.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
					while(rs.next())
					{
						PUser user=new PUser();
						user.puser_name=rs.getString("puser_name");
						list.add(user);  //将对象PUser 循环 加入ArrayList 集合中
					}
					}catch(Exception e)
					{
						System.out.println("该用户不存在！"+e.getMessage());
					}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
				return (String)list.get(0).puser_name;
	}
	
	
}
