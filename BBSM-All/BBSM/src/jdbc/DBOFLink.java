package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import FunCtion.FLink;

public class DBOFLink extends DBOPer {


	/***********1.添加链接信息     (传入链接 fl 对象)*******************/	
	public void Insert_FL_data(FLink fl)
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.SQL插入语句
		String sql="INSERT into FLink(link_id,link_name,link_url,link_info,link_logo,link_islogo,link_ispass) "
				  + "values(?,?,?,?,?,?)";
		
		//3.加载驱动    建立连接
		con=getConnection();
		
		try {
		//4.输入SQL插入语句	
		st=con.prepareStatement(sql);
		
		
		st.setInt(1,fl.link_id);
		st.setString(2,fl.link_name);
		st.setString(3,fl.link_url);
		st.setString(4,fl.link_info);
		st.setString(5,fl.link_logo);
		st.setInt(6,checkis(fl.link_islogo));
		st.setInt(7,checkis(fl.link_ispass));
		
 	    //5.执行SQL插入语句
		st.executeUpdate();
		
		}catch(Exception e)
		{
			System.out.println("新链接信息添加异常！"+e.getMessage());
		}
		
		//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
	}
	
	/***********2.删除链接信息     (传入链接 fl 对象的 编号ID :link_id)*******************/	
	public void Delete_FL_data(int link_id)   
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL删除语句
				String sql="delete from FLink where link_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL删除语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1, link_id);
		 	    
				
		 	    //5.执行SQL删除语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("链接信息删除异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
	}
	
	/***********3.修改(更新)链接信息   (ID,name，URL,info,Logo,islogo,ispass)  (传入链接FL 对象)*******************/	
	public void Update_FL_data(FLink fl)
	{
		
		//1.创建       Connection    PreparedStatement    ResultSet 
				Connection con=null;
				PreparedStatement st=null;
				ResultSet rs=null;
				
				//2.SQL更新语句
				String sql="update FLink set link_id=?,link_name=?,link_url=?,link_info=?,link_logo=?,islogo=?,ispass=?where link_id=?";
				
				//3.加载驱动    建立连接
				con=getConnection();
				
				try {
				//4.输入SQL更新语句	
				st=con.prepareStatement(sql);
				
				st.setInt(1,fl.link_id);
				st.setString(2,fl.link_name);
				st.setString(3,fl.link_url);
				st.setString(4, fl.link_info);
				st.setString(5, fl.link_logo);
				st.setInt(6,checkis(fl.link_islogo));
				st.setInt(7, checkis(fl.link_ispass));
				st.setInt(8,fl.link_id);
				
		 	    //5.执行SQL更新语句
				st.executeUpdate();
				
				}catch(Exception e)
				{
					System.out.println("这个链接信息修改异常！"+e.getMessage());
				}
				
				//6.关闭所有链接   Connection    PreparedStatement    ResultSet 
				closeAll(con, st, rs);
				
		
	}
	
	/***********4.查询此链接信息     返回ArrayList 类型(无参数)*******************/	
	public ArrayList<FLink> Query_AllFLink()
	{
		//1.创建       Connection    PreparedStatement    ResultSet 
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		
		//2.创建ArrayList集合      作用：保存对象信息
		ArrayList<FLink> list=new ArrayList<FLink>();
		
		//3.SQL查询语句
		String sql="select  link_id=?,link_name=?,link_url=?,link_info=?,link_logo=?,islogo=?,ispass=? from FLink";
		
		//4.加载驱动 建立连接
		con=getConnection();
		
		try {
		//5.输入SQL查询语句
		st=con.prepareStatement(sql);
		//6.执行查询语句          并存入结果集ResultSet
		rs=st.executeQuery();
		
		//7.从结果集中 取出信息  存入对象中    并将对象加入  ArrayList 集合中
		while(rs.next())
		{
			FLink fl=new FLink();
			fl.link_id=rs.getInt(1);
			fl.link_name=rs.getString(2);
			fl.link_url=rs.getString(3);	
			fl.link_info=rs.getString(4);
			fl.link_logo=rs.getString(5);
			fl.link_islogo=checkis(rs.getInt(6));
			fl.link_ispass=checkis(rs.getInt(7));
			
			list.add(fl);  //将对象FLink 循环 加入ArrayList 集合中
		}
		
		}catch(Exception e)
		{
			System.out.println("此链接查询异常！"+e.getMessage());
		}
		
		//8.关闭所有链接   Connection    PreparedStatement    ResultSet 
		closeAll(con, st, rs);
		
		
		//9.将含有查询结果信息对象 的 ArrayList 集合  返回
		return list;
	}
	

	
	
}
