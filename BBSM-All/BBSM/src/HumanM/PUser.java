package HumanM;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PUser {
	//用户信息表
	public	int puser_id;// 01 用户ID号
	public	String puser_name;// 02 用户名
	public	String puser_password;// 03 密码
	public	String puser_sex;// 04 性别
	public	Date puser_birthday;// 05 生日
	
	public	String puser_QQ;// 06 QQ号
	public	String puser_Email;// 07 Email
	public	String puser_tel;// 08 电话或手机
	public	String puser_face;// 09 头像地址
	public	String puser_sign;// 10 个人签名
	
	public	int puser_grade=1;// 11 用户等级 //数据库中为char类型
	public	int puser_mark=0;// 12 积分
	public	int puser_topic=0;// 13 发表话题总数
	public	int puser_wealth=0;// 14 用户财富
	public	int puser_post=0;// 15 发表帖子总数
	
	public	String puser_group;// 16 所属门派
	public	String puser_lastip;// 17 最后登录IP
	public	int puser_delnum=0;// 18 被删帖子总数
	public	String puser_friends="";// 19 好友名单
	public	Date puser_regtime;// 20 注册时间
	
	public	Date puser_lasttime;// 21 上次访问时间
	public	boolean puser_locked;// 22 状态判断
	public	boolean puser_admin;// 23 管理员身份判断
	public	String puser_password_a="";// 24 取回密码答案
	public	String puser_password_q="";// 25 取回密码提问
	
	public	int puser_age;// 26 年龄
	public	String puser_secondname;// 27 用户昵称
	public	String puser_truename;// 28 真实姓名
	public	String puser_blood="";// 29 血型
	public	String puser_zodiac="";// 30 生肖
	
	public	String puser_nation="";// 31 民族
	public	String puser_province="";// 32 省份
	public	String puser_city=""; // 33 城市
	
	
	
	public PUser() {}

	public PUser(String puser_name, String puser_password, String puser_sex, Date puser_birthday,
			 String puser_tel, String puser_email,String puser_password_a,String puser_password_q) {
	
		super();
		
		
		DateFormat df = new SimpleDateFormat("yyyy");
        String birthday = df.format(puser_birthday);
        String currenttime=df.format(new Date());
        int a=Integer.valueOf(birthday);
        int b=Integer.valueOf(currenttime);
        int age=b-a;
        
		this.puser_name = puser_name;
		this.puser_password = puser_password;
		this.puser_sex = puser_sex;
		this.puser_birthday = puser_birthday;
		this.puser_QQ = " ";
		this.puser_Email = puser_email;
		this.puser_tel = puser_tel;
		this.puser_face =  " ";
		this.puser_sign =  " ";
		this.puser_grade = 1;
		this.puser_mark = 0;
		this.puser_topic = 0;
		this.puser_wealth = 0;
		this.puser_post = 0;
		this.puser_group = "";
		this.puser_lastip = "";
		this.puser_delnum = 0;
		this.puser_friends = "";
		this.puser_locked = false;
		this.puser_admin =  false ;
		this.puser_password_a = puser_password_a;
		this.puser_password_q = puser_password_q;
		this.puser_age = age;
		this.puser_secondname =  " ";
		this.puser_truename =  " ";
		this.puser_blood =  " ";
		this.puser_zodiac =  " ";
		this.puser_nation =  " ";
		this.puser_province =  " ";
		this.puser_city =  " ";
	}
	
	
	
	
	/**
	 * @param puser_id
	 * @param puser_name
	 * @param puser_password
	 * @param puser_sex
	 * @param puser_birthday
	 * @param puser_QQ
	 * @param puser_Email
	 * @param puser_tel
	 * @param puser_face
	 * @param puser_sign
	 * @param puser_grade
	 * @param puser_mark
	 * @param puser_topic
	 * @param puser_wealth
	 * @param puser_post
	 * @param puser_group
	 * @param puser_lastip
	 * @param puser_delnum
	 * @param puser_friends
	 * @param puser_regtime
	 * @param puser_lasttime
	 * @param puser_locked
	 * @param puser_admin
	 * @param puser_password_a
	 * @param puser_password_q
	 * @param puser_age
	 * @param puser_secondname
	 * @param puser_truename
	 * @param puser_blood
	 * @param puser_zodiac
	 * @param puser_nation
	 * @param puser_province
	 * @param puser_city
	 */
	public PUser(int puser_id, String puser_name, String puser_password, String puser_sex, Date puser_birthday,
			String puser_QQ, String puser_Email, String puser_tel, String puser_face, String puser_sign,
			int puser_grade, int puser_mark, int puser_topic, int puser_wealth, int puser_post, String puser_group,
			String puser_lastip, int puser_delnum, String puser_friends, Date puser_regtime, Date puser_lasttime,
			boolean puser_locked, boolean puser_admin, String puser_password_a, String puser_password_q, int puser_age,
			String puser_secondname, String puser_truename, String puser_blood, String puser_zodiac,
			String puser_nation, String puser_province, String puser_city) {
		this.puser_id = puser_id;
		this.puser_name = puser_name;
		this.puser_password = puser_password;
		this.puser_sex = puser_sex;
		this.puser_birthday = puser_birthday;
		this.puser_QQ = puser_QQ;
		this.puser_Email = puser_Email;
		this.puser_tel = puser_tel;
		this.puser_face = puser_face;
		this.puser_sign = puser_sign;
		this.puser_grade = puser_grade;
		this.puser_mark = puser_mark;
		this.puser_topic = puser_topic;
		this.puser_wealth = puser_wealth;
		this.puser_post = puser_post;
		this.puser_group = puser_group;
		this.puser_lastip = puser_lastip;
		this.puser_delnum = puser_delnum;
		this.puser_friends = puser_friends;
		this.puser_regtime = puser_regtime;
		this.puser_lasttime = puser_lasttime;
		this.puser_locked = puser_locked;
		this.puser_admin = puser_admin;
		this.puser_password_a = puser_password_a;
		this.puser_password_q = puser_password_q;
		this.puser_age = puser_age;
		this.puser_secondname = puser_secondname;
		this.puser_truename = puser_truename;
		this.puser_blood = puser_blood;
		this.puser_zodiac = puser_zodiac;
		this.puser_nation = puser_nation;
		this.puser_province = puser_province;
		this.puser_city = puser_city;
	}

	public int getPuser_id() {
		return puser_id;
	}

	public void setPuser_id(int puser_id) {
		this.puser_id = puser_id;
	}

	public String getPuser_name() {
		return puser_name;
	}

	public void setPuser_name(String puser_name) {
		this.puser_name = puser_name;
	}

	public String getPuser_password() {
		return puser_password;
	}

	public void setPuser_password(String puser_password) {
		this.puser_password = puser_password;
	}

	public String getPuser_sex() {
		return puser_sex;
	}

	public void setPuser_sex(String puser_sex) {
		this.puser_sex = puser_sex;
	}

	public Date getPuser_birthday() {
		return puser_birthday;
	}

	public void setPuser_birthday(Date puser_birthday) {
		this.puser_birthday = puser_birthday;
	}

	public String getPuser_QQ() {
		return puser_QQ;
	}

	public void setPuser_QQ(String puser_QQ) {
		this.puser_QQ = puser_QQ;
	}

	public String getPuser_Email() {
		return puser_Email;
	}

	public void setPuser_Email(String puser_Email) {
		this.puser_Email = puser_Email;
	}

	public String getPuser_tel() {
		return puser_tel;
	}

	public void setPuser_tel(String puser_tel) {
		this.puser_tel = puser_tel;
	}

	public String getPuser_face() {
		return puser_face;
	}

	public void setPuser_face(String puser_face) {
		this.puser_face = puser_face;
	}

	public String getPuser_sign() {
		return puser_sign;
	}

	public void setPuser_sign(String puser_sign) {
		this.puser_sign = puser_sign;
	}

	public int getPuser_grade() {
		return puser_grade;
	}

	public void setPuser_grade(int puser_grade) {
		this.puser_grade = puser_grade;
	}

	public int getPuser_mark() {
		return puser_mark;
	}

	public void setPuser_mark(int puser_mark) {
		this.puser_mark = puser_mark;
	}

	public int getPuser_topic() {
		return puser_topic;
	}

	public void setPuser_topic(int puser_topic) {
		this.puser_topic = puser_topic;
	}

	public int getPuser_wealth() {
		return puser_wealth;
	}

	public void setPuser_wealth(int puser_wealth) {
		this.puser_wealth = puser_wealth;
	}

	public int getPuser_post() {
		return puser_post;
	}

	public void setPuser_post(int puser_post) {
		this.puser_post = puser_post;
	}

	public String getPuser_group() {
		return puser_group;
	}

	public void setPuser_group(String puser_group) {
		this.puser_group = puser_group;
	}

	public String getPuser_lastip() {
		return puser_lastip;
	}

	public void setPuser_lastip(String puser_lastip) {
		this.puser_lastip = puser_lastip;
	}

	public int getPuser_delnum() {
		return puser_delnum;
	}

	public void setPuser_delnum(int puser_delnum) {
		this.puser_delnum = puser_delnum;
	}

	public String getPuser_friends() {
		return puser_friends;
	}

	public void setPuser_friends(String puser_friends) {
		this.puser_friends = puser_friends;
	}

	public Date getPuser_regtime() {
		return puser_regtime;
	}

	public void setPuser_regtime(Date puser_regtime) {
		this.puser_regtime = puser_regtime;
	}

	public Date getPuser_lasttime() {
		return puser_lasttime;
	}

	public void setPuser_lasttime(Date puser_lasttime) {
		this.puser_lasttime = puser_lasttime;
	}

	public boolean isPuser_locked() {
		return puser_locked;
	}

	public void setPuser_locked(boolean puser_locked) {
		this.puser_locked = puser_locked;
	}

	public boolean isPuser_admin() {
		return puser_admin;
	}

	public void setPuser_admin(boolean puser_admin) {
		this.puser_admin = puser_admin;
	}

	public String getPuser_password_a() {
		return puser_password_a;
	}

	public void setPuser_password_a(String puser_password_a) {
		this.puser_password_a = puser_password_a;
	}

	public String getPuser_password_q() {
		return puser_password_q;
	}

	public void setPuser_password_q(String puser_password_q) {
		this.puser_password_q = puser_password_q;
	}

	public String getPuser_secondname() {
		return puser_secondname;
	}

	public void setPuser_secondname(String puser_secondname) {
		this.puser_secondname = puser_secondname;
	}

	public String getPuser_truename() {
		return puser_truename;
	}

	public void setPuser_truename(String puser_truename) {
		this.puser_truename = puser_truename;
	}

	public String getPuser_blood() {
		return puser_blood;
	}

	public void setPuser_blood(String puser_blood) {
		this.puser_blood = puser_blood;
	}

	public String getPuser_zodiac() {
		return puser_zodiac;
	}

	public void setPuser_zodiac(String puser_zodiac) {
		this.puser_zodiac = puser_zodiac;
	}

	public String getPuser_nation() {
		return puser_nation;
	}

	public void setPuser_nation(String puser_nation) {
		this.puser_nation = puser_nation;
	}

	public String getPuser_province() {
		return puser_province;
	}

	public void setPuser_province(String puser_province) {
		this.puser_province = puser_province;
	}

	public String getPuser_city() {
		return puser_city;
	}

	public void setPuser_city(String puser_city) {
		this.puser_city = puser_city;
	}

	
	
	//输出Admin  表
    public void out(ArrayList<?> list)
    {
  	  for(int i=0;i<list.size();i++)
        {
       	PUser user=new PUser();
       	 user=(PUser)list.get(i);
       	 
       	System.out.println(user.puser_id+"\t"+user.puser_name+"\t"+user.puser_password+"\t"+user.puser_birthday.toString());
        }
    }
	
	
	
	

}
