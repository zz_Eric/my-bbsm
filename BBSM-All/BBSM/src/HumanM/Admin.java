package HumanM;

import java.util.ArrayList;



public class Admin {
	  //管理员信息表
      public int admin_id;//管理员ID
      public String admin_name;//管理员名字
      public String admin_password;//管理员密码
      public int admin_user_id;//管理员前台用户名
      
      public Admin() {}
      
      public Admin(int admin_id,String admin_name,String admin_password,int admin_user_id)
      {
    	  
    	  this.admin_id=admin_id;
    	  this.admin_name=admin_name;
    	  this.admin_password=admin_password;
    	  this.admin_user_id=admin_user_id;
    		
      }
      
      
      
      
      
    /**
	 * @return the admin_id
	 */
	public int getAdmin_id() {
		return admin_id;
	}

	/**
	 * @param admin_id the admin_id to set
	 */
	public void setAdmin_id(int admin_id) {
		this.admin_id = admin_id;
	}

	/**
	 * @return the admin_name
	 */
	public String getAdmin_name() {
		return admin_name;
	}

	/**
	 * @param admin_name the admin_name to set
	 */
	public void setAdmin_name(String admin_name) {
		this.admin_name = admin_name;
	}

	/**
	 * @return the admin_password
	 */
	public String getAdmin_password() {
		return admin_password;
	}

	/**
	 * @param admin_password the admin_password to set
	 */
	public void setAdmin_password(String admin_password) {
		this.admin_password = admin_password;
	}

	/**
	 * @return the admin_user
	 */
	public int getAdmin_user() {
		return admin_user_id;
	}

	/**
	 * @param admin_user the admin_user to set
	 */
	public void setAdmin_user(int admin_user) {
		this.admin_user_id = admin_user;
	}

	//输出Admin  表
      public void out(ArrayList<?> list)
      {
    	  for(int i=0;i<list.size();i++)
          {
         	 Admin admin=new Admin();
         	 admin=(Admin)list.get(i);
         	 
         	System.out.println(admin.admin_id+" "+admin.admin_name+""+admin.admin_password+""+admin.admin_user_id);
          }
      }
      
      
}
