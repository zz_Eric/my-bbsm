package FunCtion;

public class Advertisement {
   
	public int ad_id;//01 广告ID,代表广告的不同位置
	public String ad_url;//02 广告链接URL
	public String ad_image;//03 广告图片URL
	public String ad_title;// 04 广告语
	
	
	
	
	/**
	 * 
	 */
	public Advertisement() {
	}


	/**
	 * @param ad_id
	 * @param ad_url
	 * @param ad_image
	 * @param ad_title
	 */
	public Advertisement(int ad_id, String ad_url, String ad_image, String ad_title) {
		super();
		this.ad_id = ad_id;
		this.ad_url = ad_url;
		this.ad_image = ad_image;
		this.ad_title = ad_title;
	}


	/**
	 * @return the ad_id
	 */
	public int getAd_id() {
		return ad_id;
	}


	/**
	 * @param ad_id the ad_id to set
	 */
	public void setAd_id(int ad_id) {
		this.ad_id = ad_id;
	}


	/**
	 * @return the ad_url
	 */
	public String getAd_url() {
		return ad_url;
	}


	/**
	 * @param ad_url the ad_url to set
	 */
	public void setAd_url(String ad_url) {
		this.ad_url = ad_url;
	}


	/**
	 * @return the ad_image
	 */
	public String getAd_image() {
		return ad_image;
	}


	/**
	 * @param ad_image the ad_image to set
	 */
	public void setAd_image(String ad_image) {
		this.ad_image = ad_image;
	}


	/**
	 * @return the ad_title
	 */
	public String getAd_title() {
		return ad_title;
	}


	/**
	 * @param ad_title the ad_title to set
	 */
	public void setAd_title(String ad_title) {
		this.ad_title = ad_title;
	}
	
	
	
	
	
	
	
	
}
