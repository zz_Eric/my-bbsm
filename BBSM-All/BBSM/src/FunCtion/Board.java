package FunCtion;

public class Board {
	
	public int board_id;//01 ���ID
	public boolean board_idMother;//02 �Ƿ�Ϊ�����
	public int board_bid;//03 �������
	public String board_name;//04 �������
	public String board_info;//05 ���˵��
	public String board_master;//06 ����
	public String board_img;//07 ���LOGO ��ַ
	public int board_postnum;//08 ���������
	public int board_topicnum;//09 �����������
	public int board_todaynum;//10 ��鵱�շ�����
	public int board_lastreply;//11 ������»ظ�
	

	/**
	 * 
	 */
	public Board() {
	}



	public Board(int board_id, boolean board_idMother, int board_bid, String board_name, String board_info,
			String board_master) {
		super();
		this.board_id = board_id;
		this.board_idMother = board_idMother;
		this.board_bid = board_bid;
		this.board_name = board_name;
		this.board_info = board_info;
		this.board_master = board_master;
		this.board_img=null;
		this.board_postnum = 0;
		this.board_topicnum = 0;
		this.board_todaynum = 0;
		this.board_lastreply=-1;
	}



	/**
	 * @return the board_id
	 */
	public int getBoard_id() {
		return board_id;
	}



	/**
	 * @param board_id the board_id to set
	 */
	public void setBoard_id(int board_id) {
		this.board_id = board_id;
	}



	/**
	 * @return the board_idMother
	 */
	public boolean isBoard_idMother() {
		return board_idMother;
	}



	/**
	 * @param board_idMother the board_idMother to set
	 */
	public void setBoard_idMother(boolean board_idMother) {
		this.board_idMother = board_idMother;
	}



	/**
	 * @return the board_bid
	 */
	public int getBoard_bid() {
		return board_bid;
	}



	/**
	 * @param board_bid the board_bid to set
	 */
	public void setBoard_bid(int board_bid) {
		this.board_bid = board_bid;
	}



	/**
	 * @return the board_name
	 */
	public String getBoard_name() {
		return board_name;
	}



	/**
	 * @param board_name the board_name to set
	 */
	public void setBoard_name(String board_name) {
		this.board_name = board_name;
	}



	/**
	 * @return the board_info
	 */
	public String getBoard_info() {
		return board_info;
	}



	/**
	 * @param board_info the board_info to set
	 */
	public void setBoard_info(String board_info) {
		this.board_info = board_info;
	}



	/**
	 * @return the board_master
	 */
	public String getBoard_master() {
		return board_master;
	}



	/**
	 * @param board_master the board_master to set
	 */
	public void setBoard_master(String board_master) {
		this.board_master = board_master;
	}



	/**
	 * @return the board_img
	 */
	public String getBoard_img() {
		return board_img;
	}



	/**
	 * @param board_img the board_img to set
	 */
	public void setBoard_img(String board_img) {
		this.board_img = board_img;
	}



	/**
	 * @return the board_postnum
	 */
	public int getBoard_postnum() {
		return board_postnum;
	}



	/**
	 * @param board_postnum the board_postnum to set
	 */
	public void setBoard_postnum(int board_postnum) {
		this.board_postnum = board_postnum;
	}



	/**
	 * @return the board_topicnum
	 */
	public int getBoard_topicnum() {
		return board_topicnum;
	}



	/**
	 * @param board_topicnum the board_topicnum to set
	 */
	public void setBoard_topicnum(int board_topicnum) {
		this.board_topicnum = board_topicnum;
	}



	/**
	 * @return the board_todaynum
	 */
	public int getBoard_todaynum() {
		return board_todaynum;
	}



	/**
	 * @param board_todaynum the board_todaynum to set
	 */
	public void setBoard_todaynum(int board_todaynum) {
		this.board_todaynum = board_todaynum;
	}



	/**
	 * @return the board_lastreply
	 */
	public int getBoard_lastreply() {
		return board_lastreply;
	}



	/**
	 * @param board_lastreply the board_lastreply to set
	 */
	public void setBoard_lastreply(int board_lastreply) {
		this.board_lastreply = board_lastreply;
	}

	
	
}
