package FunCtion;

import java.util.Date;

public class Topic {
	
	public int topic_id;// 01 话题ID
	public int topic_board_id;// 02 所属板块ID
	public int topic_user_id;// 03 发帖者 
	public String topic_name;//04 话题名称
	public Date topic_time;//05 话题发表时间
	
	public int topic_hits;//06 话题浏览量
	public int topic_replynum;//07 话题回复量
	public int topic_lastreply_id;//08 最后回复者帖子ID
	public boolean topic_top;//09 是否置顶
	public boolean topic_best;//10 是否加精
	
	public boolean topic_del;//11 是否已被删帖
	public boolean topic_hot;//12 是否热门话题
	public String topic_info;//04 话题说明
	
	
	/**
	 * @return the topic_info
	 */
	public String getTopic_info() {
		return topic_info;
	}



	/**
	 * @param topic_info the topic_info to set
	 */
	public void setTopic_info(String topic_info) {
		this.topic_info = topic_info;
	}



	/**
	 * 
	 */
	public Topic() {
	}



	public Topic(int topic_id, int topic_board_id, int topic_user_id, String topic_name, Date topic_time,
			int topic_hits, int topic_replynum, int topic_lastreply_id, boolean topic_top, boolean topic_best,
			boolean topic_del, boolean topic_hot) {

		this.topic_id = topic_id;
		this.topic_board_id = topic_board_id;
		this.topic_user_id = topic_user_id;
		this.topic_name = topic_name;
		this.topic_time = topic_time;
		this.topic_hits = topic_hits;
		this.topic_replynum = topic_replynum;
		this.topic_lastreply_id = topic_lastreply_id;
		this.topic_top = topic_top;
		this.topic_best = topic_best;
		this.topic_del = topic_del;
		this.topic_hot = topic_hot;
	}



	/**
	 * @return the topic_id
	 */
	public int getTopic_id() {
		return topic_id;
	}



	/**
	 * @param topic_id the topic_id to set
	 */
	public void setTopic_id(int topic_id) {
		this.topic_id = topic_id;
	}



	/**
	 * @return the topic_board_id
	 */
	public int getTopic_board_id() {
		return topic_board_id;
	}



	/**
	 * @param topic_board_id the topic_board_id to set
	 */
	public void setTopic_board_id(int topic_board_id) {
		this.topic_board_id = topic_board_id;
	}



	/**
	 * @return the topic_user_id
	 */
	public int getTopic_user_id() {
		return topic_user_id;
	}



	/**
	 * @param topic_user_id the topic_user_id to set
	 */
	public void setTopic_user_id(int topic_user_id) {
		this.topic_user_id = topic_user_id;
	}



	/**
	 * @return the topic_name
	 */
	public String getTopic_name() {
		return topic_name;
	}



	/**
	 * @param topic_name the topic_name to set
	 */
	public void setTopic_name(String topic_name) {
		this.topic_name = topic_name;
	}



	/**
	 * @return the topic_time
	 */
	public Date getTopic_time() {
		return topic_time;
	}



	/**
	 * @param topic_time the topic_time to set
	 */
	public void setTopic_time(Date topic_time) {
		this.topic_time = topic_time;
	}



	/**
	 * @return the topic_hits
	 */
	public int getTopic_hits() {
		return topic_hits;
	}



	/**
	 * @param topic_hits the topic_hits to set
	 */
	public void setTopic_hits(int topic_hits) {
		this.topic_hits = topic_hits;
	}



	/**
	 * @return the topic_replynum
	 */
	public int getTopic_replynum() {
		return topic_replynum;
	}



	/**
	 * @param topic_replynum the topic_replynum to set
	 */
	public void setTopic_replynum(int topic_replynum) {
		this.topic_replynum = topic_replynum;
	}



	/**
	 * @return the topic_lastreply_id
	 */
	public int getTopic_lastreply_id() {
		return topic_lastreply_id;
	}



	/**
	 * @param topic_lastreply_id the topic_lastreply_id to set
	 */
	public void setTopic_lastreply_id(int topic_lastreply_id) {
		this.topic_lastreply_id = topic_lastreply_id;
	}



	/**
	 * @return the topic_top
	 */
	public boolean isTopic_top() {
		return topic_top;
	}



	/**
	 * @param topic_top the topic_top to set
	 */
	public void setTopic_top(boolean topic_top) {
		this.topic_top = topic_top;
	}



	/**
	 * @return the topic_best
	 */
	public boolean isTopic_best() {
		return topic_best;
	}



	/**
	 * @param topic_best the topic_best to set
	 */
	public void setTopic_best(boolean topic_best) {
		this.topic_best = topic_best;
	}



	/**
	 * @return the topic_del
	 */
	public boolean isTopic_del() {
		return topic_del;
	}



	/**
	 * @param topic_del the topic_del to set
	 */
	public void setTopic_del(boolean topic_del) {
		this.topic_del = topic_del;
	}



	/**
	 * @return the topic_hot
	 */
	public boolean isTopic_hot() {
		return topic_hot;
	}



	/**
	 * @param topic_hot the topic_hot to set
	 */
	public void setTopic_hot(boolean topic_hot) {
		this.topic_hot = topic_hot;
	}
	
	
	

}
