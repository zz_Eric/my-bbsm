package FunCtion;

//友情链接对象
public class FLink {

	public int link_id;// 01 友情链接ID
	public String link_name;// 02 网站名称
	public String link_url;// 03 网站URL
	public String link_info;// 04 网站简介
	public String link_logo;// 05 LOGO地址
	public boolean link_islogo;// 06 是否有LOGO
	public boolean link_ispass;// 07 是否通过验证
	
	public FLink() {}
	/**
	 * @param link_id 友情链接ID
	 * @param link_name 网站名称
	 * @param link_url 网站URL
	 * @param link_info 网站简介
	 * @param link_logo LOGO地址
	 * @param link_islogo 是否有LOGO
	 * @param link_ispass 是否通过验证
	 */
	public FLink(int link_id, String link_name, String link_url, String link_info, String link_logo) {
		
		this.link_id = link_id;
		this.link_name = link_name;
		this.link_url = link_url;
		this.link_info = link_info;
		this.link_logo = link_logo;
		this.link_islogo = false;
		this.link_ispass = false;
	}
	/**
	 * @return the link_id
	 */
	public int getLink_id() {
		return link_id;
	}
	/**
	 * @param link_id the link_id to set
	 */
	public void setLink_id(int link_id) {
		this.link_id = link_id;
	}
	/**
	 * @return the link_name
	 */
	public String getLink_name() {
		return link_name;
	}
	/**
	 * @param link_name the link_name to set
	 */
	public void setLink_name(String link_name) {
		this.link_name = link_name;
	}
	/**
	 * @return the link_url
	 */
	public String getLink_url() {
		return link_url;
	}
	/**
	 * @param link_url the link_url to set
	 */
	public void setLink_url(String link_url) {
		this.link_url = link_url;
	}
	/**
	 * @return the link_info
	 */
	public String getLink_info() {
		return link_info;
	}
	/**
	 * @param link_info the link_info to set
	 */
	public void setLink_info(String link_info) {
		this.link_info = link_info;
	}
	/**
	 * @return the link_logo
	 */
	public String getLink_logo() {
		return link_logo;
	}
	/**
	 * @param link_logo the link_logo to set
	 */
	public void setLink_logo(String link_logo) {
		this.link_logo = link_logo;
	}
	/**
	 * @return the link_islogo
	 */
	public boolean isLink_islogo() {
		return link_islogo;
	}
	/**
	 * @param link_islogo the link_islogo to set
	 */
	public void setLink_islogo(boolean link_islogo) {
		this.link_islogo = link_islogo;
	}
	/**
	 * @return the link_ispass
	 */
	public boolean isLink_ispass() {
		return link_ispass;
	}
	/**
	 * @param link_ispass the link_ispass to set
	 */
	public void setLink_ispass(boolean link_ispass) {
		this.link_ispass = link_ispass;
	}
	
	
	
}
