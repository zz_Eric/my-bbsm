package FunCtion;

import java.sql.Timestamp;
import java.util.Date;

public class Post {

	/*
	 * @param post_id 帖子ID
	 * @param post_board_id 所属板块ID
	 * @param post_user_id 发帖者ID
	 * @param post_topic_id 所属话题ID
	 * @param post_reply_id 回复用户的ID
	 * @param post_content 帖子内容
	 * @param post_time 发表时间
	 * @param post_editime 重新编辑时间
	 * @param post_ip 发帖者所在IP地址
	 */
	public int post_id;
	public int post_board_id;
	public int post_user_id;
	public int post_topic_id;
	public int post_reply_id;
	public String post_content;
	public Date post_time;
	public Date post_editime;
	public String post_ip;
	
	public Post() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param post_id
	 * @param post_board_id
	 * @param post_user_id
	 * @param post_topic_id
	 * @param post_reply_id
	 * @param post_content
	 * @param post_time
	 * @param post_editime
	 * @param post_ip
	 */
	public Post(int post_id, int post_board_id, int post_user_id, int post_topic_id, int post_reply_id,
			String post_content, Timestamp post_time, Date post_editime, String post_ip) {
		this.post_id = post_id;
		this.post_board_id = post_board_id;
		this.post_user_id = post_user_id;
		this.post_topic_id = post_topic_id;
		this.post_reply_id = post_reply_id;
		this.post_content = post_content;
		this.post_time = post_time;
		this.post_editime = post_editime;
		this.post_ip = post_ip;
	}

	/**
	 * @return the post_id
	 */
	public int getPost_id() {
		return post_id;
	}

	/**
	 * @param post_id the post_id to set
	 */
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}

	/**
	 * @return the post_board_id
	 */
	public int getPost_board_id() {
		return post_board_id;
	}

	/**
	 * @param post_board_id the post_board_id to set
	 */
	public void setPost_board_id(int post_board_id) {
		this.post_board_id = post_board_id;
	}

	/**
	 * @return the post_user_id
	 */
	public int getPost_user_id() {
		return post_user_id;
	}

	/**
	 * @param post_user_id the post_user_id to set
	 */
	public void setPost_user_id(int post_user_id) {
		this.post_user_id = post_user_id;
	}

	/**
	 * @return the post_topic_id
	 */
	public int getPost_topic_id() {
		return post_topic_id;
	}

	/**
	 * @param post_topic_id the post_topic_id to set
	 */
	public void setPost_topic_id(int post_topic_id) {
		this.post_topic_id = post_topic_id;
	}

	/**
	 * @return the post_reply_id
	 */
	public int getPost_reply_id() {
		return post_reply_id;
	}

	/**
	 * @param post_reply_id the post_reply_id to set
	 */
	public void setPost_reply_id(int post_reply_id) {
		this.post_reply_id = post_reply_id;
	}

	/**
	 * @return the post_content
	 */
	public String getPost_content() {
		return post_content;
	}

	/**
	 * @param post_content the post_content to set
	 */
	public void setPost_content(String post_content) {
		this.post_content = post_content;
	}

	/**
	 * @return the post_time
	 */
	public Date getPost_time() {
		return post_time;
	}

	/**
	 * @param post_time the post_time to set
	 */
	public void setPost_time(Date post_time) {
		this.post_time = post_time;
	}

	/**
	 * @return the post_editime
	 */
	public Date getPost_editime() {
		return post_editime;
	}

	/**
	 * @param post_editime the post_editime to set
	 */
	public void setPost_editime(Date post_editime) {
		this.post_editime = post_editime;
	}

	/**
	 * @return the post_ip
	 */
	public String getPost_ip() {
		return post_ip;
	}

	/**
	 * @param post_ip the post_ip to set
	 */
	public void setPost_ip(String post_ip) {
		this.post_ip = post_ip;
	}

	
	
	
	
	
}
