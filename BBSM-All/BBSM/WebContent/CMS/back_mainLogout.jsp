<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>后台注销页面</title>
</head>
<body>
<%
    request.getSession();
   session.removeAttribute("admin");//释放session中的管理员信息
   response.sendRedirect("../main.jsp");
%>
</body>
</html>