<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>后台管理员登录界面</title>
	<link rel="stylesheet" href="../assets/css/login.css"/>
	<link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min.css"/>
</head>

<body >


	<div id="login-box">
		<h2>后台登录</h2>
		<form action="back_main_Login_action.jsp" autocomplete="off">
		<div class="form">
			<div class="item">
				<i class="fa fa-user-circle-o" aria-hidden="true"></i>
				<input type="text" placeholder="Admin" name="LoginName">
			</div>
		    <div class="item">
		    	<i class="fa fa-key" aria-hidden="true"></i>
				<input type="password" placeholder="Password" name="PassWord">
		    </div>
		</div>
		<button type="submit">Login</button>
		 </form>
	</div>
     


</body>
</html>