<%@page import="java.util.ArrayList"%>
<%@page import="jdbc.DBOBoard"%>
<%@page import="FunCtion.Board"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>板块管理页面</title>

<!--字体图标-->
<link href="../javaex/pc/css/icomoon.css" rel="stylesheet" />
<!--动画-->
<link href="../javaex/pc/css/animate.css" rel="stylesheet" />
<!--骨架样式-->
<link href="../javaex/pc/css/common.css" rel="stylesheet" />
<!--皮肤（缇娜）-->
<link href="../javaex/pc/css/skin/tina.css" rel="stylesheet" />
<!--jquery，不可修改版本-->
<script src="../javaex/pc/lib/jquery-1.7.2.min.js"></script>
<!--全局动态修改-->
<script src="../javaex/pc/js/common.js"></script>
<!--核心组件-->
<script src="../javaex/pc/js/javaex.min.js"></script>
<!--表单验证-->
<script src="../javaex/pc/js/javaex-formVerify.js"></script>

</head>
<body>

  <%
   		DBOBoard dbbo=new DBOBoard(); 
        
       ArrayList<Board> list=dbbo.Query_AllBoard();
   %>
   
   <!--主体内容-->
<div class="list-content">
	<!--块元素-->
	<div class="block">
		<!--页面有多个表格时，可以用于标识表格-->
		<h2>板块管理</h2>
		<!--右上角的返回按钮
		<a href="javascript:history.back();">
			<button class="button indigo radius-3" style="position: absolute;right: 20px;top: 16px;"><span class="icon-arrow_back"></span> 返回</button>
		</a>
		-->
		<!--正文内容-->
		<div class="main">
			<!--表格上方的搜索操作-->
			<div class="admin-search">
				<div class="input-group">
					<input type="text" class="text" placeholder="提示信息" />
					<button class="button blue">搜索</button>
				</div>
			</div>
			
			<!--表格上方的操作元素，添加、删除等-->
			<div class="operation-wrap">
				<div class="buttons-wrap">
					<a href="Board_M/add_board.jsp"><button class="button blue radius-3"><span class="icon-plus"></span> 添加</button></a>
					<a href="Board_M/delete_board.jsp?"><button class="button red radius-3"><span class="icon-close2"></span> 删除</button></a>
				</div>
			</div>
			<table id="table" class="table color2">
				<thead>
					<tr>
						<th class="checkbox"><input type="checkbox" class="fill listen-1" /> </th>
						<th>板块ID</th>
     					<th>板块名</th>
     					<th>所属板块</th>
     					<th>是否为主板</th>
     					<th>板块主题总数</th>
     					<th>板块当日发帖数</th>
     					<th>操作</th>
					</tr>
				</thead>
				<tbody>
				
					<%int tot=0;
     		  for(int i=0;i<list.size();i++)
     		  {
     			  Board bo= (Board)list.get(i);
     		 %>	  
     		 	<tr>
     		 		<td class="checkbox"><input type="checkbox" class="fill listen-1-2" /> </td>
     		 	 	<td><%=bo.board_id %></td>
     		 		<td><%=bo.board_name %></td>
     		 		<td><%=bo.board_bid %></td>
     		 		<td><%=bo.board_idMother %></td>
     		 		<td><%=bo.board_topicnum %></td>
     		 		<td><%=bo.board_todaynum%></td>
     		 		<td>
     		 		<a href="--locked_user.jsp?id=<%=bo.board_id%>" >	
					<button class="button indigo radius-3"><span class="icon-close"></span>详情</button>
					</a>
					
					<a href="--locked_user.jsp?id=<%=bo.board_id%>" >	
					<button class="button yellow radius-3"><span class="icon-build"></span>编辑</button>
					</a>
     		 		</td>
     		 	</tr>
  
     		<%	  
     		 tot=i; }
     		%>
     		
				</tbody>
			</table>
			<div class="page">
	               <ul id="page" class="pagination">
	               <%
	                 
	               %>
	               
	               </ul>
					</div>
		</div>	
		</div>

					
<script>
  var tot='<%out.print(tot);%>';
  var totpage=(tot/5)+1;
	javaex.page({
		id : "page",
		pageCount : totpage,	// 总页数
		currentPage : 1,// 默认选中第几页
		perPageCount : 5,	// 每页显示多少条，不填时，不显示该条数选择控件
		isShowJumpPage : true,	// 是否显示跳页
		totalNum : tot,		// 总条数，不填时，不显示
		position : "left",
		callback : function(rtn) {
			console.log("当前选中的页数：" + rtn.pageNum);
			console.log("每页显示条数：" + rtn.perPageCount);
		}
	});
	
	
</script>
	</div>




</body>
</html>