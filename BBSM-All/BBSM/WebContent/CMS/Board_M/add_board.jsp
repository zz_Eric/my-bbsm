<%@page import="jdbc.DBOUser"%>
<%@page import="FunCtion.*"%>
<%@page import="HumanM.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jdbc.DBOBoard"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>添加板块页面</title>

<%@ include file="../../javaex_folder/javaex_EX3.jsp" %>

</head>
<body>

<%
    DBOBoard dbob=new DBOBoard();
    ArrayList<Board> list_board=dbob.Query_AllBoard();
    
    DBOUser dbu=new DBOUser();
    ArrayList<PUser> list_user=dbu.Query_AllPuserdata();
    
%>

 <!--主体内容-->
	<div class="list-content">
	<!--块元素-->
	<div class="block">
		<!--页面有多个表格时，可以用于标识表格-->
		<h2>添加板块</h2>
		<!--右上角的返回按钮-->
		<a href="javascript:history.back();">
			<button class="button indigo radius-3" style="position: absolute;right: 20px;top: 16px;"><span class="icon-arrow_back"></span> 返回</button>
		</a>
		
		<!--正文内容-->
		<div class="main">
			
			<form id="form" action="add_board_action.jsp">
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required">*</span>
					<p class="subtitle">板块名称</p>
					</div>
			   <div class="right">
						<input type="text" class="text" data-type="必填"  placeholder="请输入板块名称"id="board_name" name="board_name"/>
					</div>
				</div>
				
				<!--下拉选择框-->
				<div class="unit clear">
					<div class="left"><p class="subtitle">所属板块</p></div>
					<div class="right">
						<select id="board_of" name="board_of">
							<option value="0">无</option>
							<%for(int i=0,j=1;i<list_board.size();i++) {
							Board bo=(Board) list_board.get(i);
							if(bo.board_idMother){%>
							<option value=<%=bo.board_id%>><%=bo.board_name %></option>
							<%}
							}%>
						</select>
					</div>
				</div>
				
				<!--下拉选择框-->
				<div class="unit clear">
					<div class="left"><p class="subtitle">任命版主</p></div>
					<div class="right">
						<select id="board_boarder" name="board_boarder">
							<option value="0">root</option>
							<%for(int i=0;i<list_user.size();i++) {
							PUser user=(PUser) list_user.get(i);
							%>
							<option value=<%=user.puser_id%>><%=user.puser_name %></option>
							<%
							}%>
						</select>
					</div>
				</div>
				
				
				
				<!--文本域-->
				<div class="unit clear">
					<div class="left"><p class="subtitle">简介</p></div>
					<div class="right">
						<textarea class="desc" placeholder="请填写简介" name="board_info" id="board_info"></textarea>
						<!--提示说明-->
						<p class="hint">请填写板块简介。简介中不得包含令人反感的信息，且长度应在10到255个字符之间。</p>
					</div>
				</div>

				<!--提交按钮-->
				<div class="unit clear" style="width: 800px;">
					<div style="text-align: center;">
						<!--表单提交时，必须是input元素，并指定type类型为button，否则ajax提交时，会返回error回调函数-->
						<input type="button" id="save" class="button yes" value="保存" />
					</div>
				</div>
			</form>
		  </div>	
		</div>
     </div>
     
<script>
	javaex.select({
		id : "select",
		isSearch : false
	});
	
	javaex.tag({
		id : "tag"
	});
	
	// 监听点击保存按钮事件
	$("#save").click(function() {
		// 表单验证函数
		if (javaexVerify()) {
			// 返回错误信息时，可以添加自定义异常提示。参数为元素id和提示
			// addErrorMsg("username", "用户名不存在");
			// 提交
			 $("#form").submit();
			alert("验证通过");
		}
	});

	// 监听点击返回按钮事件
	$("#return").click(function() {
		alert("返回");
		// window.location.href = document.referrer;
	});
</script>
</body>
</html>