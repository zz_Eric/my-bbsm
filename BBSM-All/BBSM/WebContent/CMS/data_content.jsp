<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>统计数据</title>

<!--字体图标-->
<link href="../javaex/pc/css/icomoon.css" rel="stylesheet" />
<!--动画-->
<link href="../javaex/pc/css/animate.css" rel="stylesheet" />
<!--骨架样式-->
<link href="../javaex/pc/css/common.css" rel="stylesheet" />
<!--皮肤（缇娜）-->
<link href="../javaex/pc/css/skin/tina.css" rel="stylesheet" />
<!--jquery，不可修改版本-->
<script src="../javaex/pc/lib/jquery-1.7.2.min.js"></script>
<!--全局动态修改-->
<script src="../javaex/pc/js/common.js"></script>
<!--核心组件-->
<script src="../javaex/pc/js/javaex.min.js"></script>
<!--表单验证-->
<script src="../javaex/pc/js/javaex-formVerify.js"></script>


</head>
<body>
<!--主体内容-->
	<div class="list-content">
	<!--块元素-->
	<div class="block">
		<!--页面有多个表格时，可以用于标识表格-->
		<h2>数据统计结果</h2>
		
		<!--正文内容-->
		<div class="main">
			
			<form id="form" action="change_password_action.jsp">
			<ul class="equal-3 clear">
			<li>
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required"></span><p class="subtitle">管理员数</p></div>
					<div class="right">
						<input type="text"  data-type="必填" class="text readonly" readonly
						autocomplete="off" id="task_exp" name="task_exp" value="1 "/>
					</div>
				</div>
			</li>
			<li>	
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required"></span><p class="subtitle">用户总数</p></div>
					<div class="right">
						<input type="text"  data-type="必填" class="text readonly" readonly
						autocomplete="off" id="task_exp" name="task_exp" value="6 "/>
					</div>
				</div>
			</li>
			<li>	
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required"></span><p class="subtitle">版主总数</p></div>
					<div class="right">
						<input type="text"  data-type="必填" class="text readonly" readonly
						autocomplete="off" id="task_exp" name="task_exp" value="3 "/>
					</div>
				</div>
			</li>
			<li>	
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required"></span><p class="subtitle">话题总数</p></div>
					<div class="right">
						<input type="text"  data-type="必填" class="text readonly" readonly
						autocomplete="off" id="task_exp" name="task_exp" value="1 "/>
					</div>
				</div>
			</li>
			<li>
			<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required"></span><p class="subtitle">帖子总数</p></div>
					<div class="right">
						<input type="text"  data-type="必填" class="text readonly" readonly
						autocomplete="off" id="task_exp" name="task_exp" value="2"/>
					</div>
				</div>
			</li>
            </ul>
			</form>
		  </div>	
		</div>
     </div>
     
</body>
</html>