<%@page import="HumanM.Admin"%>
<%@page import="jdbc.DBOAdmin"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>论坛后台管理</title>

<%@ include file="../../javaex_folder/javaex_EX2.jsp" %>

<style>
</style>
</head>
<body>
	<%
	Admin admin=(Admin)session.getAttribute("admin");
	%>
	<!--顶部导航-->
	<div class="admin-navbar">
		<div class="admin-container-fluid clear">
			<!--logo名称-->
			<div class="admin-logo">论坛后台</div>
			
			<!--导航-->
			<ul class="admin-navbar-nav fl">
				<li class="active"><a href="/admin/index">总览</a></li>
				<li><a href="../main.jsp"  target="_blank">论坛首页</a></li>
				<li><a href="/admin/nav2">导航2</a></li>
			</ul>
			
			<!--右侧-->
			<ul class="admin-navbar-nav fr">
				<% if(admin!=null)	
				{%><li>
				    
					<a href="javascript:;">
					欢迎您，管理员<%=admin.admin_name %>
					</a>
					<ul class="dropdown-menu" style="right: 10px;">
						<li><a href="back_mainLogout.jsp">退出当前账号</a></li>
					</ul>
					</li>
				 <%}%>
				
			</ul>
		</div>
	</div>
	
	<!--主题内容-->
	<div class="admin-mian">
		<!--左侧菜单-->
		<div class="admin-aside admin-aside-fixed">
			<h1><span class="admin-nav-name">总览</span></h1>
			<div id="admin-toc" class="admin-toc">
				<div class="menu-box">
					<div id="menu" class="menu">
						<ul>
							<li class="menu-item hover">
								<a href="javascript:page('Board_M/add_board.jsp');">后台管理</a>
							</li>
							<li class="menu-item">
								<a href="javascript:;">快速入口<i class="icon-keyboard_arrow_left"></i></a>
								<ul>
									<li><a href="javascript:page('test.jsp');">管理员账号</a></li>
									<li><a href="javascript:page('content_board.jsp');">板块管理</a></li>
									<li><a href="javascript:page('content_user.jsp');">用户管理</a></li>
									<li><a href="javascript:page('data_content.jsp');">数据统计</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<!--iframe载入内容-->
		<div class="admin-markdown">
			<iframe id="page" src="content_user.jsp"></iframe>
		</div>
	</div>
</body>
<script>
	var hightUrl = "xxxx";
	javaex.menu({
		id : "menu",
		isAutoSelected : true,
		key : "",
		url : hightUrl
	});
	
	$(function() {
		// 设置左侧菜单高度
		setMenuHeight();
	});
	
	/**
	 * 设置左侧菜单高度
	 */
	function setMenuHeight() {
		var height = document.documentElement.clientHeight - $("#admin-toc").offset().top;
		height = height - 10;
		$("#admin-toc").css("height", height+"px");
	}
	
	// 控制页面载入
	function page(url) {
		$("#page").attr("src", url);
	}
</script>
</html>