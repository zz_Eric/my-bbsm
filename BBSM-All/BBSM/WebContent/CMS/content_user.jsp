<%@page import="HumanM.PUser"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jdbc.DBOUser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>用户管理界面</title>
<!--字体图标-->
<link href="../javaex/pc/css/icomoon.css" rel="stylesheet" />
<!--动画-->
<link href="../javaex/pc/css/animate.css" rel="stylesheet" />
<!--骨架样式-->
<link href="../javaex/pc/css/common.css" rel="stylesheet" />
<!--皮肤（缇娜）-->
<link href="../javaex/pc/css/skin/tina.css" rel="stylesheet" />
<!--jquery，不可修改版本-->
<script src="../javaex/pc/lib/jquery-1.7.2.min.js"></script>
<!--全局动态修改-->
<script src="../javaex/pc/js/common.js"></script>
<!--核心组件-->
<script src="../javaex/pc/js/javaex.min.js"></script>
<!--表单验证-->
<script src="../javaex/pc/js/javaex-formVerify.js"></script>
</head>
<body>

   <%
   		DBOUser dbuser=new DBOUser();
       ArrayList<PUser> list=dbuser.Query_AllPuserdata();
   %>
   
   <!--主体内容-->
<div class="list-content">
	<!--块元素-->
	<div class="block">
		<!--页面有多个表格时，可以用于标识表格-->
		<h2>用户管理</h2>
		<!--右上角的返回按钮
		<a href="javascript:history.back();">
			<button class="button indigo radius-3" style="position: absolute;right: 20px;top: 16px;"><span class="icon-arrow_back"></span> 返回</button>
		</a>
		-->
		<!--正文内容-->
		<div class="main">
			<!--表格上方的搜索操作-->
			<div class="admin-search">
				<div class="input-group">
					<input type="text" class="text" placeholder="提示信息" />
					<button class="button blue">搜索</button>
				</div>
			</div>
			
			<!--表格上方的操作元素，添加、删除等-->
			<div class="operation-wrap">
				<div class="buttons-wrap">
					<button class="button blue radius-3"><span class="icon-plus"></span> 添加</button>
					<button class="button red radius-3"><span class="icon-close2"></span> 删除</button>
				</div>
			</div>
			<table id="table" class="table color2">
				<thead>
					<tr>
						<th class="checkbox"><input type="checkbox" class="fill listen-1" /> </th>
						<th>用户ID</th>
     					<th>用户名</th>
     					<th>邮箱</th>
     					<th>TEL</th>
     					<th>注册时间</th>
     					<th>上次注销时间</th>
     					<th>状态</th>
     					<th>是否为管理员</th>
     					<th>是否为版主</th>
     					<th>操作</th>
					</tr>
				</thead>
				<tbody>
				
					<%
     		  for(int i=0;i<list.size();i++)
     		  {
     			  PUser user= (PUser)list.get(i);
     		 %>	  
     		 	<tr>
     		 		<td class="checkbox"><input type="checkbox" class="fill listen-1-2" /> </td>
     		 	 	<td><%=user.puser_id %></td>
     		 		<td><%=user.puser_name %></td>
     		 		<td><%=user.puser_Email %></td>
     		 		<td><%=user.puser_tel %></td>
     		 		<td><%=user.puser_regtime %></td>
     		 		<td><%=user.puser_lasttime %></td>
     		 		<td><%=user.puser_locked %></td>
     		 		<td><%=user.puser_admin %></td>
     		 		<td><%=000%></td>
     		 		<td>
     		 		<% if(user.puser_locked==false){%>
     		 		<a href="user_M/locked_user.jsp?id=<%=user.puser_id%>" >	
					<button class="button gray radius-3"><span class="icon-collect"></span>禁用</button>
					</a>
					<%}else{%>
						<a href="user_M/unlocked_user.jsp?id=<%=user.puser_id%>">	
						<button class="button blue radius-3"><span class="icon-collect"></span>启用</button>
						</a>	
					<%}%>
     		 		</td>
     		 	</tr>
  
     		<%	  
     		  }
     		%>
     		
				</tbody>
			</table>
		</div>	
		</div>

					
<script>
	javaex.page({
		id : "page",
		pageCount : 12,	// 总页数
		currentPage : 1,// 默认选中第几页
		perPageCount : 10,	// 每页显示多少条，不填时，不显示该条数选择控件
		isShowJumpPage : true,	// 是否显示跳页
		totalNum : 125,		// 总条数，不填时，不显示
		position : "left",
		callback : function(rtn) {
			console.log("当前选中的页数：" + rtn.pageNum);
			console.log("每页显示条数：" + rtn.perPageCount);
		}
	});
</script>
	</div>

</body>
</html>