<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>找回密码界面</title>
	<link rel="stylesheet" href="assets/css/forget.css"/>
	<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css"/>
</head>

<body >

	<div id="login-box">
		<h2>找回密码</h2>
		<form action="user_reg_action.jsp">
		<div class="form">
			<div class="item">
				<i class="fa fa-user-circle-o" aria-hidden="true"></i>
				<input type="text" placeholder="Username" name="username" value="">
				<a href=""><i class="fa fa-eercast" aria-hidden="true"></i></a>
			</div>
			<div class="lock">
		    	<i class="fa fa-key" aria-hidden="true"></i>
		    	<input type="text" class="text readonly" value=" " readonly autocomplete="off" />
		    </div>
		    <div class="item">
		    	<i class="fa fa-unlock" aria-hidden="true"></i>
				<input type="text" placeholder="验证回答" name="reply"value="">
		    </div>
		    <div class="item">
		    	<i class="fa fa-key" aria-hidden="true"></i>
				<input type="password" placeholder="新的密码" name="passwordq"value="">
		    </div>
		    <div class="item">
		    	<i class="fa fa-unlock" aria-hidden="true"></i>
				<input type="password" placeholder="确认密码" name="password2"value="">
		    </div>
		   
			<span><a href="user_reg.jsp">返回登录</a></span>
		</div>
		<button type="submit">提交</button>
		</form>
	</div>
     


</body>
</html>