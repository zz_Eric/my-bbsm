<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>用户注册界面</title>
	<link rel="stylesheet" href="../assets/css/reg.css"/>
	<link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min.css"/>
</head>

<body >

	<div id="login-box">
		<h2><a href="User_login.jsp">Login</a>/Sign Up</h2>
		<form action="user_reg_action.jsp" autocomplete="off">
		<div class="form">
			<div class="item">
				<i class="fa fa-user-circle-o" aria-hidden="true"></i>
				<input type="text" placeholder="Username" name="username" value="">
			</div>
		    <div class="item">
		    	<i class="fa fa-key" aria-hidden="true"></i>
				<input type="password" placeholder="Password" name="password1"value="">
		    </div>
		    <div class="item">
		    	<i class="fa fa-unlock" aria-hidden="true"></i>
				<input type="password" placeholder="确认密码" name="password2"value="">
		    </div>
		    <div class="item">
				<i class="fa fa-envelope-o" aria-hidden="true"></i>
				<input type="email" placeholder="Email" name="email"value="">
			</div>
			 <div class="item">
				<i class="fa fa-phone" aria-hidden="true"></i>
				<input type="tel" placeholder="电话" name="tel"value="">
			</div>
			<div class="item">
				<i class="fa fa-birthday-cake" aria-hidden="true"></i>
				<input type="date" placeholder="1990-01-01" name="birthday"value="">
			</div>
		    <div class="item">
		    	<i class="fa fa-key" aria-hidden="true"></i>
				<input type="text" placeholder="验证问题" name="question"value="">
		    </div>
		    <div class="item">
		    	<i class="fa fa-unlock" aria-hidden="true"></i>
				<input type="text" placeholder="验证回答" name="reply"value="">
		    </div>
		    <div class="radio" >
		         <i class="fa fa-mars" aria-hidden="true"></i><input type="radio"  name="sex" value="男">
				 <i class="fa fa-venus" aria-hidden="true"></i><input type="radio"  name="sex"value="女">
			</div>
		</div>
		<button type="submit">Sign</button>
		</form>
	</div>
     


</body>
</html>