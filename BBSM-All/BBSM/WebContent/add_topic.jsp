<%@page import="jdbc.DBOBoard"%>
<%@page import="FunCtion.Board"%>
<%@page import="HumanM.PUser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>发布主题页面</title>

<%@ include file="javaex_folder/javaex_EX1.jsp" %>

</head>
<body>

<%
     //板块的信息，发布人的信息, 传入话题数据库
     int board_id = Integer.parseInt(request.getParameter("board_id"));//获得板块的ID号
     PUser user=(PUser)session.getAttribute("user");
     DBOBoard dbbo=new DBOBoard();
     Board boa=dbbo.Query_isBoard(board_id);
%>


	<!--主体内容-->
	<div class="list-content">
		<!--块元素-->
		<div class="block">
			<!--页面有多个表格时，可以用于标识表格-->
			<h2>发布主题</h2>
			<!--右上角的返回按钮-->
			<a href="javascript:history.back();">
				<button class="button indigo radius-3"
					style="position: absolute; right: 20px; top: 16px;">
					<span class="icon-arrow_back"></span> 返回
				</button>
			</a>

			<!--正文内容-->
			<div class="main">
	    <form id="form" action="add_topic_action.jsp">
	   
	     <!--文本框-->
					<div class="unit clear">
						<div class="left">
							<span class="required">*</span>
							<p class="subtitle">楼主</p>
						</div>
						<div class="right">
							<input type="text"  data-type="必填" class="text readonly" readonly
						autocomplete="off" id="user_name" name="user_name" value="@<%=user.puser_name%>"/>
						</div>
					</div>
		<!--文本框-->
					<div class="unit clear">
						<div class="left">
							<span class="required">*</span>
							<p class="subtitle">所属板块</p>
						</div>
						<div class="right">
							<input type="text"  data-type="必填" class="text readonly" readonly
						autocomplete="off" id="board_name" name="board_name" value="<%=boa.board_name%>"/>
						</div>
					</div>
			<!--文本框-->
					<div class="unit clear">
						<div class="left">
							<span class="required">*</span>
							<p class="subtitle">主题名称</p>
						</div>
						<div class="right">
							<input type="text"  data-type="必填" class="text" id="topic_name" name="topic_name" value="测试主题1 "/>
						</div>
					</div>

		
					<!--文本域-->
					<div class="unit clear">
						<div class="left">
							<span class="required">*</span>
							<p class="subtitle">内容</p>
						</div>
						<div class="right">
							<textarea class="desc" placeholder="请填写内容" name="topic_info"
								id="topic_info"></textarea>
							<!--提示说明-->
							<p class="hint">请填写主题内容。内容中不得包含令人反感的信息。</p>
						</div>
					</div>
					
					<!--提交按钮-->
					<div class="unit clear" style="width: 800px;">
						<div style="text-align: center;">
							<!--表单提交时，必须是input元素，并指定type类型为button，否则ajax提交时，会返回error回调函数-->
							<input type="button" id="save" class="button yes" value="发表话题" />
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<script>
	javaex.select({
		id : "select",
		isSearch : false
	});
	
	javaex.tag({
		id : "tag"
	});
	
	// 监听点击保存按钮事件
	$("#save").click(function() {
		// 表单验证函数
		if (javaexVerify()) {
			// 返回错误信息时，可以添加自定义异常提示。参数为元素id和提示
			// addErrorMsg("username", "用户名不存在");
			// 提交
			 $("#form").submit();
			alert("验证通过");
		}
	});

	// 监听点击返回按钮事件
	$("#return").click(function() {
		alert("返回");
		// window.location.href = document.referrer;
	});
</script>



</body>
</html>