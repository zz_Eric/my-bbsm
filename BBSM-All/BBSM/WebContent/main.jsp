<%@page import="HumanM.PUser"%>
<%@page import="jdbc.DBOUser"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>论坛</title>

<%@ include file="javaex_folder/javaex_EX1.jsp" %>

</head>
<body>


	<%
	//获取用户对象
	PUser user=(PUser)session.getAttribute("user");
	%>
	<!--顶部导航-->
	<div class="admin-navbar">
		<div class="admin-container-fluid clear">
			<!--logo名称-->
			<div class="admin-logo">论坛</div>
			
			<!--导航-->
			<ul class="admin-navbar-nav fl">
				<li class="active"><a href="javascript:page('main_content.jsp');">总览</a></li>
				<li><a href="javascript:page('topic_content.jsp?id=11');">首页</a></li>
				<li><a href="javascript:page('topic_content.jsp?id=2');">文学</a></li>
			</ul>
			
			<!--右侧-->
			<ul class="admin-navbar-nav fr">
				<% if(user!=null)	
				{%><li>
				    
					<a href="javascript:;">
					欢迎您，<%=user.puser_name %>
					</a>
					<ul class="dropdown-menu" style="right: 10px;">
						<li><a href="Login/mainLogout.jsp">退出当前账号</a></li>
					</ul>
					</li>
				 <%}else{%>
				 <li><a href="Login/User_login.jsp"  style="color: white;">登录</a></li>
				 <li>/</li>
				 <li><a href="Login/user_reg.jsp"  style="color: white;">注册</a></li>
				
				<%}%>
				
			</ul>
		</div>
	</div>
	
	<!--主题内容-->
	<div class="admin-mian">
		<!--左侧菜单-->
		<div class="admin-aside admin-aside-fixed">
			<h1><span class="admin-nav-name">总览</span></h1>
			<div id="admin-toc" class="admin-toc">
				<div class="menu-box">
					<div id="menu" class="menu">
					<% if(user!=null){%>
						<ul>
							<li class="menu-item hover">					
								<a href="CMS/back_main_Login.jsp"  target="_blank">后台系统</a>
								
							</li>
							<li class="menu-item">
								<a href="javascript:;">个人中心<i class="icon-keyboard_arrow_left"></i></a>
								<ul>
									<li><a href="javascript:page('Front-end/user/user_info_check.jsp');">个人主页</a></li>
									<li><a href="javascript:page('Front-end/user/change_password.jsp');">修改密码</a></li>
									<li><a href="javascript:page('Front-end/user/user_edit.jsp');">修改个人信息</a></li>											
								</ul>
							</li>
							<li class="menu-item">
								<a href="javascript:;">我的板块<i class="icon-keyboard_arrow_left"></i></a>
								<ul>
									<li><a href="javascript:page('test.jsp');">申请版主</a></li>
									<li><a href="javascript:page('test.jsp');">板块管理</a></li>											
								</ul>
							</li>
						</ul>
						
						<%}else{%>
						<ul>
							<li class="menu-item hover">					
									<a href="javascript:page('main_content.jsp');">后台系统</a>
								
							</li>
							<li class="menu-item">
								<a href="javascript:;">个人中心<i class="icon-keyboard_arrow_left"></i></a>
								<ul><!--user_info_check.jsp 检测用户是否登录  -->
									<li><a href="javascript:page('Front-end/user/user_info_check.jsp');">个人主页</a></li>
									<li><a href="javascript:page('Front-end/user/user_info_check.jsp');">修改密码</a></li>
									<li><a href="javascript:page('Front-end/user/user_info_check.jsp');">编辑个人信息</a></li>											
								</ul>
							</li>
						</ul>
							
								<%}%>
					</div>
				</div>
			</div>
		</div>
		
		<!--iframe载入内容-->
		<div class="admin-markdown">
			<iframe id="page" src="main_content.jsp"></iframe>
		</div>
	</div>
</body>
<script>
	var hightUrl = "xxxx";
	javaex.menu({
		id : "menu",
		isAutoSelected : true,
		key : "",
		url : hightUrl
	});
	
	$(function() {
		// 设置左侧菜单高度
		setMenuHeight();
	});
	
	/**
	 * 设置左侧菜单高度
	 */
	function setMenuHeight() {
		var height = document.documentElement.clientHeight - $("#admin-toc").offset().top;
		height = height - 10;
		$("#admin-toc").css("height", height+"px");
	}
	
	// 控制页面载入
	function page(url) {
		$("#page").attr("src", url);
	}
</script>

<script>
	$("#tanc").click(function() {
		javaex.dialog({
			type : "dialog",	// 弹出层类型
			width : "400",
			height : "300",
			url : "Front-end/back_main_Login.jsp"
		});
	});
</script>

</html>