<%@page import="jdbc.DBOUser"%>
<%@page import="jdbc.DBOPer"%>
<%@page import="HumanM.PUser"%>
<%@page import="FunCtion.Topic"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jdbc.DBOPost"%>
<%@page import="jdbc.DBOTopic"%>
<%@page import="FunCtion.Board"%>
<%@page import="jdbc.DBOBoard"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>话题</title>
<style type="text/css">
#topic {
	margin: 3px;
	padding: 10px;
	height: 150px;
	width: 1100px;
/*  border: 1px solid #666;*/
	font-family: "MS Serif", "New York", serif;
	background-color: #FFF;
}

#photo {
	margin: 1px;
	padding: 1px;
	height: 100px;
	width: 100px;
	border-radius: 50%;
	position: relative;
	left: auto;
	top: 3px;
	right: auto;
	bottom: auto;
	border: 1px solid #CCC;
	float: left;
	clear: none;
}
#user_name {
	position: relative;
	height: 25px;
	width: 100px;
	margin: 1px;
	padding: 1px;
	top: 3px;
	font-size: 13px;
	color: #666;
	font-family: "仿宋";
	text-align: center;
}

#content_topic {
	margin: 1px;
	padding: 1px;
	float: left;
	height: 120px;
	width: 700px;
	position: relative;
	left: 12px;
/*  border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: ridge;
	border-right-style: ridge;
	border-bottom-style: ridge;
	border-left-style: ridge;
*/
	bottom: 30px;
	font-family: "MS Serif", "New York", serif;
	font-size: 12px;
}
#topic #content_topic .topic_name {
	font-size: 28px;
	font-family: "MS Serif", "New York", serif;
	position: relative;
	left: 30px;
	color: #000000;
}
#topic #content_topic .other {
	font-size: 16px;
	position: relative;
	top: 35px;
	left: 50px;
}
#topic #content_topic .best {
	font-size: 20px;
	font-family: "华文彩云";
	position: relative;
	top: 75px;
	left: 220px;
}
#topic #content_topic .bad {
	position: relative;
	left: 250px;
	top: 75px;
	font-family: "华文彩云";
	font-size: 20px;
}
#last_replay {
	margin: 3px;
	padding: 3px;
	height: 120px;
	width: 200px;
	position: relative;
	float: left;
	left: 45px;
	bottom: 30px;
}
#topic #last_replay .user {
	float: left;
	position: relative;
	top: 20px;
	right: 40px;
}
#topic #last_replay .blank {
	float: left;
	position: relative;
}
#topic #last_replay .time {
	float: right;
	top: 45px;
	position: relative;
}
.topic_nav_content {
	list-style-type: none;
	background-color: #F1F2F3;
	margin: 1px;
	padding: 1px;
	color: #0078D7;
}
.main {
	background-color: #F1F2F3;
}
#topic_nav {
	height: 150px;
	width: 1100px;
	position: relative;
	top: 0px;
/*	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
*/
	left: 10px;
	background-color: #FFF;
	font-size: 18px;
}
.main #topic_nav .border_name {
	font-size: 24px;
	text-align: center;
	font-weight: bold;
	color: #000;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	position: relative;
	margin: 5px;
	padding: 5px;
	top: 30px;
	letter-spacing: 2em;
}
.main #topic_nav .border {
	position: relative;
	top: 88px;
	float: left;
	left: 22px;
}
.main #topic_nav .border_time {
	float: left;
	position: relative;
	right: 1px;
	top: 120px;
}
#function {
	margin: 1px;
	padding: 1px;
	height: 100px;
	width: 200px;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	float: right;
	position: relative;
	top: 0px;
	font-size: 24px;
}
.main #topic_nav #function ul li {
	float: left;
	margin: 7px;
	padding: 1px;
	position: relative;
	top: 0px;
	right: 50px;
	list-style-type: none;
	width: 100;
	height: 30;
	bottom: 20px;
}
.main #topic_nav #function ul li .topic_number {
	font-size: 20px;
	float:right
	margin-left: 6px;
	position: relative;
}
.main #topic_nav #function ul li .post_number {
	font-size: 20px;
	float: right;
	position: relative;
}
.main #topic_nav #function ul {
	background-color: #F1F2F3;
}

body{
zoom: 1;
transform-origin: 0px 0px;//在使用transform方法进行文字或图像的变形时，是以元素的中心点为基准点进行的。使用transform-origin属性，可以改变变形的基准点,用法：transform-origin: 10px 10px;
transform: scale(0.9, 0.9);//第一个参数:指定水平方向的缩放倍率，第二个参数:指定垂直方向的缩放倍率
overflow:-Scroll;
overflow-x:hidden
}

a{
text-decoration: none;
}
</style>
</head>

<body class="main">

<%
		int board_id = Integer.parseInt(request.getParameter("id"));//获得板块的ID号
		DBOBoard dbb=new DBOBoard();
		DBOTopic dbt=new DBOTopic();
		DBOPost dbp=new DBOPost();
		Board bo=dbb.Query_isBoard(board_id);

		int sum_topic_number= dbt.Sum_Topic(board_id);
		int sum_post_number= dbp.Sum_Post(board_id);
		
		ArrayList<Topic> list=dbt.All_Topic(board_id);
		PUser puser=new PUser();
		DBOUser dbu=new DBOUser();
		
%>


<div class="topic_nav" id="topic_nav">
<span class="border_name"><%=bo.board_name %>板块</span>
<span class="border">版主:<%=bo.board_master %></span>
<span class="border_time">创建时间：2020-04-25 19:37</span>
<div class="function" id="function">
<ul>
<li>主题:<span class="topic_number"><%=sum_topic_number %></span></li>
<li>帖子:<span class="post_number"><%=sum_post_number %></span></li>
</ul>
<a href="add_topic.jsp?board_id=<%=board_id%>"><font size="2">发布主题</font></a>
</div>

</div>


<ul class="topic_nav_content">
 <%for(int i=0;i<list.size();i++)
 { 
	 Topic tp=list.get(i);
  %>
	<li>
  <div class="topic" id="topic">
  <hr>
  <div class="photo" id="photo"> <img alt=" " width="99" height="99" src="<%=dbu.Q_user_info(dbu.Q_user_info_ofid(tp.topic_user_id)).puser_face %>" style="border-radius: 50%;overflow:hidden;"/>    </div>
  <div class="user_name" id="user_name"> <span> <%=dbu.Q_user_info_ofid(tp.topic_user_id)%></span>    </div>
  <div class="content_topic" id="content_topic"> <span class="topic_name"><a href="replays.jsp?topic_id=<%=tp.topic_id%>"><%=tp.topic_name%></a>&nbsp;&nbsp;&nbsp;<a href="topic_edit.jsp?topic_id=<%=tp.topic_id%>"><font size="2">管理</font></a></span>
    <span class="other">In@<%=bo.board_name %> <%=tp.topic_time%></span>
    <span class="best">赞: 0</span>
    <span class="bad">踩: 0</span>    </div>
    
  <div class="last_replay" id="last_replay"> 
  <span class="blank"> 最新回复:</span>
 <%if(tp.topic_lastreply_id==-1) {%>
	 <span class="user">NULL</span>
	    <span class="time"></span>    
	     <%}else{%>
    <span class="user"><%=dbu.Q_user_info_ofid(dbp.some_Post(tp.topic_id).post_reply_id)%></span>
    <span class="time"> @<%=dbp.some_Post(tp.topic_lastreply_id).post_time%></span>  
    <%}%>
      </div>
      
	</div>
  </li>
<%
 } %>

</ul>
</body>
</html>

