<%@page import="jdbc.DBOPost"%>
<%@page import="jdbc.DBOTopic"%>
<%@page import="java.util.ArrayList"%>
<%@page import="jdbc.DBOBoard"%>
<%@page import="FunCtion.Board"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>前台内容</title>

<%@ include file="javaex_folder/javaex_EX1.jsp" %>
<link rel="stylesheet" href="assets/css/main_content.css"/>


</head>
<body>

<%
    DBOBoard dbbo=new DBOBoard();
	ArrayList<Board> list= dbbo.Query_AllBoard();
	DBOTopic dbt=new DBOTopic();
	DBOPost dbp= new DBOPost();

	
%>

 <!--主体内容-->
<div class="list-content">
	<!--块元素-->
	<%
	for(int i=0;i<list.size();i++){
		if(list.get(i).board_idMother)
		{
	%>
	<div class="block">
		<!--页面有多个表格时，可以用于标识表格-->
		
		<div class="block">
		
		<div class="board_box" align="left">
		<img alt="<%=list.get(i).board_name %>" src="assets/img/message_board.png" width="60px" height="60px"/>
		<h3><font color="#337AB7"><B><%=list.get(i).board_name %></B></font></h3>
		</div>
		
		<div class="board_data">
		    <ul> <% //计算该主版块中所有子版块的话题总数
		        int sum_topic=0;
		    	int sum_post=0;
		    for(int m=0;m<list.size();m++){
						if(!list.get(m).board_idMother&&list.get(m).board_bid ==list.get(i).board_id)
					{
							sum_topic +=dbt.Sum_Topic(list.get(m).board_id);
							sum_post  +=dbp.Sum_Post(list.get(m).board_id);
							
					}}%>

		    	<li><a><b>主题:  <span><%=sum_topic %></span></b> </a></li>
		    	<li><a><b>帖子:  <span><%=sum_post %></span></b> </a></li>
		    </ul>
		</div>
		
		</div>
				
		<!--正文内容-->
		<div class="main">
				<!--等分系统-->
					<!--下列li将会分成4等分-->
					<ul class="equal-4 clear">
					<% for(int j=0;j<list.size();j++){
						if(!list.get(j).board_idMother && list.get(j).board_bid == list.get(i).board_id)
					{%>
						<li>
						<a href="topic_content.jsp?id=<%=list.get(j).board_id%> ">
						<img alt="<%=list.get(j).board_name %>" src="assets/img/message.png" width="60px" height="30px" style="margin-top: 10"/>
						<b><font face="KaiTi_GB2312" color="#777777" size="2"><%=list.get(j).board_name %></font></b>
						</a>
						</li>
						<%}} %>
					</ul>
			</div>	
		</div>	
		<% }} %>
</div>


</body>
</html>