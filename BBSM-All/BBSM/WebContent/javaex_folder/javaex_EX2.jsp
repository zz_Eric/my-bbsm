<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 笔记：引用时，以引用的位置为准。如何解决这个重用问题。main.jsp引用，就以main.jsp的位置找。因此不用../ -->    
<!--字体图标-->
<link href="../javaex/pc/css/icomoon.css" rel="stylesheet" />
<!--动画-->
<link href="../javaex/pc/css/animate.css" rel="stylesheet" />
<!--骨架样式-->
<link href="../javaex/pc/css/common.css" rel="stylesheet" />
<!--皮肤（缇娜）-->
<link href="../javaex/pc/css/skin/tina.css" rel="stylesheet" />
<!--jquery，不可修改版本-->
<script src="../javaex/pc/lib/jquery-1.7.2.min.js"></script>
<!--全局动态修改-->
<script src="../javaex/pc/js/common.js"></script>
<!--核心组件-->
<script src="../javaex/pc/js/javaex.min.js"></script>
<!--表单验证-->
<script src="../javaex/pc/js/javaex-formVerify.js"></script>