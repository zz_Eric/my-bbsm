<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>编辑用户信息</title>
<%@ include file="../../javaex_folder/javaex_EX3.jsp" %>
</head>
<body>
<!--主体内容-->
	<div class="list-content">
	<!--块元素-->
	<div class="block">
		<!--页面有多个表格时，可以用于标识表格-->
		<h2>编辑用户信息</h2>
		<!--右上角的返回按钮-->
		<a href="javascript:history.back();">
			<button class="button indigo radius-3" style="position: absolute;right: 20px;top: 16px;"><span class="icon-arrow_back"></span> 返回</button>
		</a>
		
		<!--正文内容-->
		<div class="main">
			
			<form id="form" action="change_password_action.jsp">
			<ul >
			<li>
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required">*</span><p class="subtitle">姓名</p></div>
					<div class="right">
						<input type="text" class="text" data-type="必填" value="zjh767965313"id="password" name="password"/>
					</div>
				</div>
			</li>
			<li>	
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required">*</span><p class="subtitle">出生日期</p></div>
					<div class="right">
						<input type="text" class="text" data-type="必填"  value="2017-10-06" id="npassword" name="npassword"/>
					</div>
				</div>
			</li>
			<li>	
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required">*</span><p class="subtitle">电话</p></div>
					<div class="right">
						<input type="text" class="text" data-type="必填"  value="15093312648" id="npassword" name="npassword"/>
					</div>
				</div>
			</li>
			<li>	
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required">*</span><p class="subtitle">邮箱</p></div>
					<div class="right">
						<input type="text" class="text" data-type="必填"  value="767965313@qq.com" id="npassword" name="npassword"/>
					</div>
				</div>
			</li>
			<li>
			<!--下拉选择框-->
				<div class="unit clear">
					<div class="left"><p class="subtitle">民族</p></div>
					<div class="right">
						<select id="board_of" name="board_of">
							<option value="0">汉族</option>
							<option value="1">其他</option>
						</select>
					</div>
				</div>
			</li>
			<li>
			<!--下拉选择框-->
				<div class="unit clear">
					<div class="left"><p class="subtitle">城市</p></div>
					<div class="right">
						<select id="board_of" name="board_of">
							<option value="0">河南省</option>
							<option value="1">安徽省</option>
							<option value="2">湖北省</option>
							<option value="3">山东省</option>
							<option value="4">山西省</option>
							<option value="5">陕西省</option>
							<option value="5">河北省</option>
							
						</select>
					</div>
				</div>
			</li>
			<li>	
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required">*</span><p class="subtitle">个人签名</p></div>
					<div class="right">
						<textarea style="margin: 0px;width:333px;height:83px;"id="npassword" name="npassword">这个人很懒。</textarea>
					</div>
				</div>
			</li>
            </ul>
				<!--提交按钮-->
				<div class="unit clear" style="width: 800px;">
					<div style="text-align: center;">
						<!--表单提交时，必须是input元素，并指定type类型为button，否则ajax提交时，会返回error回调函数-->
						<input type="button" id="save" class="button yes" value="保存" />
					</div>
				</div>
			</form>
		  </div>	
		</div>
     </div>
     
<script>
	javaex.select({
		id : "select",
		isSearch : false
	});
	
	javaex.tag({
		id : "tag"
	});
	
	// 监听点击保存按钮事件
	$("#save").click(function() {
		// 表单验证函数
		if (javaexVerify()) {
			// 返回错误信息时，可以添加自定义异常提示。参数为元素id和提示
			// addErrorMsg("username", "用户名不存在");
			// 提交
			 $("#form").submit();
			alert("验证通过");
		}
	});

	// 监听点击返回按钮事件
	$("#return").click(function() {
		alert("返回");
		// window.location.href = document.referrer;
	});
</script>
</body>
</html>