<%@page import="jdbc.DBOUser"%>
<%@page import="HumanM.PUser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>个人主页</title>
<style type="text/css">
.content {
	font-family: Verdana, Geneva, sans-serif;
	height: 690px;
	width: 678px;
	background-color: #FFF;
	float: none;
	padding: 1px;
	color: #000;
	margin: 1px;
}
body,td,th {
	font-size: 14px;
	color: #FFF;
}
body {
	background-color: #FFF;
}
#photo {
	float: left;
	height: 190px;
	width: 190px;
	border: 2px dotted #000;
}
#user_base {
	padding: 5px;
	height: 340px;
	width: 640px;
	background-color: #FFFFFF;
	margin: 0px;
	position: absolute;
	visibility: visible;
	top: 20px;
	right: 0px;
	left: 20px;
	clip: rect(auto,auto,auto,auto);
	border-top-style: dotted;
	border-right-style: dotted;
	border-bottom-style: dotted;
	border-left-style: dotted;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
}
#photo {
	height: 199px;
	width: 199px;
	border: 1px solid #666;
	padding: 5px;
	margin: 30px;
}
#info {
	margin: 10px;
	height: 291px;
	width: 355px;
	float: left;
	position: absolute;
	top: 18px;
	left: 265px;
	right: auto;
	bottom: auto;
	clear: none;
}
#dt_info {
	height: 300px;
	width: 640px;
	background-color: #FFFFFF;
	top: 355px;
	position: absolute;
	left: 0px;
	padding: 5px;
	margin: 20px;
	border: 1px none #999;
}
.content #user_base #info ul {
	font-size: 18px;
	color: #FFF;
	text-align: left;
	white-space: normal;
	display: block;
}
.content #user_base #info ul li {
	border: 1px none #FFF;
	margin: 3px;
	padding: 1px;
	color: #000;
	list-style-type: none;
}
#icon {
	margin: 10px;
	padding: 10px;
	height: 200px;
	width: 700px;
	position: relative;
	top: 10px;
	background-color: #FF0;
}
#icon {
	margin: 10px;
	padding: 5px;
	height: 100px;
	width: 600px;
	position: absolute;
	left: 10px;
	top: 270px;
	background-color: #FFFFFF;
	clip: rect(auto,auto,auto,auto);
}
#info_nav {
	padding: 10px;
	float: left;
	height: 30px;
	width: 621px;
	position: absolute;
	background-color: #FFFFFF;
	margin-top: 10px;
	margin-right: 10px;
	margin-bottom: 10px;
	margin-left: 0px;
	color: #000;
}
.content #dt_info #info_nav span {
	border: 2pt none #999;
	position: absolute;
	top: auto;
	bottom: 1px;
	left: auto;
	right: auto;
	color: #999;
	clip: rect(auto,auto,20px,auto);
	font-size: 18px;
	font-style: normal;
	font-weight: bold;
	font-variant: normal;
	line-height: normal;
}
#content_info_post {
	margin: 1px;
	padding: 1px;
	height: 225px;
	width: 638px;
	position: relative;
	top: 15px;
	background-color: #FFFFFF;
	left: auto;
	right: 45px;
	bottom: auto;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
}
#post_photo {
	margin: 15px;
	padding: 1px;
	height: 97px;
	width: 97px;
	position: absolute;
	background-color: #FFFFFF;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
}
#post_info {
	background-color: #FFFFFF;
	height: 100px;
	width: 490px;
	position: absolute;
	left: 132px;
	top: 15px;
}
#post_content {
	background-color: #FFFFFF;
	height: 80px;
	width: 610px;
	position: absolute;
	top: 135px;
	left: 35px;
	color: #000;
}
.content #user_base #info ul li span {
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: none;
	border-top-width: 3px;
	border-right-width: 3px;
	border-bottom-width: 3px;
	border-left-width: 3px;
}
#user_tag {
	position: absolute;
	left: 10px;
	top: 227px;
	width: 333px;
	height: 83px;
	border: 1px dotted #CCC;
}
#button {
	height: 30px;
	width: 70px;
	top: 250px;
	left: 110px;
	background-color: #CCC;
	position: absolute;
}
.content #dt_info ul li #content_info_post #post_info .name {
	top: 30px;
	position: absolute;
}
.content #dt_info ul li #content_info_post #post_info .wit {
	top: 30px;
	position: absolute;
	left: 35px;
}
.content #dt_info ul li #content_info_post #post_info .title {
	font-weight: bold;
	position: absolute;
	top: 50px;
	left: 30px;
	font-size: 18px;
}
.content #dt_info ul li #content_info_post #post_info .time {
	position: absolute;
	top: 50px;
	right: 10px;
}


</style>
</head>

<body>

<%
		//获取用户对象
	PUser user=(PUser)session.getAttribute("user");
	DBOUser dbu=new DBOUser();
	user = dbu.Q_user_info(user.puser_name);
  
%>


<div class="content">
  <div class="user_base" id="user_base"> 
    <span class="dt_info">基本信息</span>
    <div class="photo" id="photo"><img src="../../<%=user.puser_face%>" width="202" height="199"/></div>
    
    <div class="button" id="button"><input name="UpPhoto" type="button" value="更改头像">
    </div> 
    
    
  <div class="info" id="info">
  <ul>
  		<li>姓名:<span><%=user.puser_name %></span> </li>
        <li>性别:<span><%=user.puser_sex %></span>  </li>
        <li>年龄:<span><%=user.puser_age %></span>  </li>
        <li>用户等级:<span>Lv.<%=user.puser_grade%></span>  </li>
        <li>积分:<span><%=user.puser_wealth %></span>  </li>
        <li>注册时间:<span><%=user.getPuser_regtime() %></span></li>
        <li>个人签名:
          <div class="user_tag" id="user_tag"><textarea style="margin: 0px;width:333px;height:83px;">这个人很懒。</textarea></div>
        </li>
  </ul>
  
  </div>
 </div>
  <div class="dt_info" id="dt_info">
    <div class="info_nav" id="info_nav">
    <span>最新动态</span>
    </div>
    <hr>
    <ul>
     
    <li><div class="content_info_post" id="content_info_post">
    <hr>
      <div class="post_photo" id="post_photo"><img src="../../<%=user.puser_face%>" width="97" height="97"/></div>
      
      <div class="post_info" id="post_info">
      <span class="name"><%=user.puser_name %>  发表了帖子:</span>
      <span class="title">栈和队列的关系</span>
      <span class="time">2020-04-23</span>
      </div>
      <div class="post_content" id="post_content">队列：先进先出。栈：后进先出。</div>
      
   </div></li>
   
   <li><div class="content_info_post" id="content_info_post">
    <hr>
      <div class="post_photo" id="post_photo"><img src="../../<%=user.puser_face%>" width="97" height="97"/></div>
      
      <div class="post_info" id="post_info">
      <span class="name"><%=user.puser_name %>  发表了帖子:</span>
      <span class="title">栈和队列的关系</span>
      <span class="time">2020-04-23</span>
      </div>
      <div class="post_content" id="post_content">队列：先进先出。栈：后进先出。</div>
      
   </div></li>
   
    </ul>
  </div>
</div>
</body>
</html>
