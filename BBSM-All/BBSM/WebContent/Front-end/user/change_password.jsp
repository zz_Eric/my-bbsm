<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>修改密码页面</title>
<%@ include file="../../javaex_folder/javaex_EX3.jsp" %>
</head>
<body>
<!--主体内容-->
	<div class="list-content">
	<!--块元素-->
	<div class="block">
		<!--页面有多个表格时，可以用于标识表格-->
		<h2>修改密码</h2>
		<!--右上角的返回按钮-->
		<a href="javascript:history.back();">
			<button class="button indigo radius-3" style="position: absolute;right: 20px;top: 16px;"><span class="icon-arrow_back"></span> 返回</button>
		</a>
		
		<!--正文内容-->
		<div class="main">
			
			<form id="form" action="change_password_action.jsp">
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required">*</span><p class="subtitle">原密码</p></div>
					<div class="right">
						<input type="password" class="text" data-type="必填"  placeholder="请输入原来的密码"id="password" name="password"/>
					</div>
				</div>
				
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required">*</span><p class="subtitle">新密码</p></div>
					<div class="right">
						<input type="password" class="text" data-type="必填"  placeholder="请输入新的密码"id="npassword" name="npassword"/>
					</div>
				</div>
				
				<!--文本框-->
				<div class="unit clear">
					<div class="left"><span class="required">*</span><p class="subtitle">确认密码</p></div>
					<div class="right">
						<input type="password" class="text" data-type="必填"  placeholder="确认新密码"id="npassword1" name="npassword1"/>
					</div>
				</div>

				<!--提交按钮-->
				<div class="unit clear" style="width: 800px;">
					<div style="text-align: center;">
						<!--表单提交时，必须是input元素，并指定type类型为button，否则ajax提交时，会返回error回调函数-->
						<input type="button" id="save" class="button yes" value="保存" />
					</div>
				</div>
			</form>
		  </div>	
		</div>
     </div>
     
<script>
	javaex.select({
		id : "select",
		isSearch : false
	});
	
	javaex.tag({
		id : "tag"
	});
	
	// 监听点击保存按钮事件
	$("#save").click(function() {
		// 表单验证函数
		if (javaexVerify()) {
			// 返回错误信息时，可以添加自定义异常提示。参数为元素id和提示
			// addErrorMsg("username", "用户名不存在");
			// 提交
			 $("#form").submit();
			alert("验证通过");
		}
	});

	// 监听点击返回按钮事件
	$("#return").click(function() {
		alert("返回");
		// window.location.href = document.referrer;
	});
</script>
</body>
</html>