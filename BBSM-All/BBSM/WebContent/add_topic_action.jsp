<%@page import="jdbc.DBOBoard"%>
<%@page import="FunCtion.Topic"%>
<%@page import="jdbc.DBOTopic"%>
<%@page import="HumanM.PUser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>添加话题逻辑处理页面</title>
</head>
<body>

<%
		//板块的信息，发布人的信息, 传入话题数据库
		DBOBoard dbob=new DBOBoard();
		String board_name=request.getParameter("board_name");
		int board_id = dbob.Q_BoardID(board_name);//获得板块的ID号
		PUser user=(PUser)session.getAttribute("user");
		String tp_name=request.getParameter("topic_name");
		String topic_info=request.getParameter("topic_info");

		DBOTopic dbto=new DBOTopic();
		
		//topic_id,topic_board_id,topic_user_id,topic_name,topic_time,topic_hits, 
		//topic_replynum,topic_lastreply_id,topic_top,topic_best,topic_del,topic_hot,topic_info
		Topic tp=new Topic();
        tp.setTopic_id(dbto.QueryID()+1); //ID号
        tp.topic_board_id=board_id;//所属板块ID号
        tp.topic_user_id=user.puser_id;//发布该话题的用户ID
        tp.topic_name=tp_name;
        tp.setTopic_info(topic_info);
        
        dbto.Insert_Topic_data(tp);
        
        out.print("<script>alert('发布成功！'); </script>");
        String url="topic_content.jsp?id="+board_id;
        response.sendRedirect(url);
        
%>


</body>
</html>