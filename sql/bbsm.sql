-- MySQL dump 10.13  Distrib 5.5.27, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: bbsm
-- ------------------------------------------------------
-- Server version	5.5.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL,
  `admin_username` char(32) DEFAULT NULL,
  `admin_password` char(32) DEFAULT NULL,
  `admin_user_id` int(11) DEFAULT NULL,
  `regist_time` datetime DEFAULT NULL,
  PRIMARY KEY (`admin_id`),
  KEY `admin_user_id` (`admin_user_id`),
  CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`admin_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (0,'root','root',0,'2020-10-24 16:15:31');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `board`
--

DROP TABLE IF EXISTS `board`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `board` (
  `board_id` int(11) NOT NULL,
  `board_idMother` char(10) DEFAULT NULL,
  `board_bid` int(11) DEFAULT NULL,
  `board_name` char(50) DEFAULT NULL,
  `board_info` text,
  `board_master_id` int(11) DEFAULT NULL,
  `board_img` char(20) DEFAULT NULL,
  `board_post_num` int(11) DEFAULT NULL,
  `board_topic_num` int(11) DEFAULT NULL,
  `board_today_num` int(11) DEFAULT NULL,
  `board_last_reply` int(11) DEFAULT NULL,
  PRIMARY KEY (`board_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `board`
--

LOCK TABLES `board` WRITE;
/*!40000 ALTER TABLE `board` DISABLE KEYS */;
/*!40000 ALTER TABLE `board` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topic`
--

DROP TABLE IF EXISTS `topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topic` (
  `topic_id` int(11) NOT NULL,
  `topic_board_id` int(11) DEFAULT NULL,
  `topic_user_id` int(11) DEFAULT NULL,
  `topic_name` char(50) DEFAULT NULL,
  `topic_create_time` datetime DEFAULT NULL,
  `topic_hits` int(11) DEFAULT NULL,
  `topic_replynum` int(11) DEFAULT NULL,
  `topic_lastreply_id` int(11) DEFAULT NULL,
  `topic_top` char(1) DEFAULT NULL,
  `topic_best` char(1) DEFAULT NULL,
  `topic_del` char(1) DEFAULT NULL,
  `topic_hot` char(1) DEFAULT NULL,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topic`
--

LOCK TABLES `topic` WRITE;
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` char(32) DEFAULT NULL,
  `user_password` char(32) DEFAULT NULL,
  `user_sex` char(2) DEFAULT NULL,
  `user_regist_time` datetime DEFAULT NULL,
  `user_age` int(11) DEFAULT NULL,
  `user_birthday` datetime DEFAULT NULL,
  `user_QQ` char(32) DEFAULT NULL,
  `user_Email` char(32) DEFAULT NULL,
  `user_face` char(32) DEFAULT NULL,
  `user_tel` char(20) DEFAULT NULL,
  `user_sign` text,
  `user_mark` int(11) DEFAULT NULL,
  `user_grade` char(20) DEFAULT NULL,
  `user_topic` int(11) DEFAULT NULL,
  `user_wealth` int(11) DEFAULT NULL,
  `user_post` int(11) DEFAULT NULL,
  `user_delete_num` int(11) DEFAULT NULL,
  `user_group` char(20) DEFAULT NULL,
  `user_last_ip` char(20) DEFAULT NULL,
  `user_last_time` datetime DEFAULT NULL,
  `user_locked` char(1) DEFAULT NULL,
  `user_admin` char(1) DEFAULT NULL,
  `user_login` char(1) DEFAULT NULL,
  `user_password_a` char(50) DEFAULT NULL,
  `user_password_q` char(50) DEFAULT NULL,
  `user_second_name` char(20) DEFAULT NULL,
  `user_true_name` char(20) DEFAULT NULL,
  `user_blood` char(10) DEFAULT NULL,
  `user_zodiac` char(20) DEFAULT NULL,
  `user_nation` char(20) DEFAULT NULL,
  `user_province` char(20) DEFAULT NULL,
  `user_city` char(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (0,'Eric','root','男','2020-10-24 16:13:35',23,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'50',NULL,30000,NULL,NULL,NULL,NULL,NULL,'0','1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-24 20:51:34
